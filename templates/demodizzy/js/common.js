//alert('open');
$(document).ready(function() {
	
	
	function getRandomInt(min, max, array) { 
		new_kod = Math.floor(Math.random() * (max - min + 1)) + min;
		if($.inArray(new_kod, array) == '-1'){
			  return new_kod; 
		}
		else{
			getRandomInt(min, max, array);
		}
	} 
	
	if($('#kod_dlya_prilozheniya').length){
		
		kod_values = [];
		
			$.ajax({
						url: '/usel://kod/.json',
						type: 'json',
						dataType: 'json',
						success: function(data){
							
								for(var i in data.item) {
									var item = data.item[i];
									
									for(var e in item.extended) {
										var extended = item.extended[e];
										
										if(extended.property[0]){
											for(var p in extended.property) {
												var property = extended.property[p];
												//console.log(property.value.value);
												kod_values.push(property.value.value);
											}
										}
									
										
									}
								}
								
							new_val = getRandomInt(1000, 9999, kod_values);
							$('#kod_dlya_prilozheniya').val(new_val);
							
						},
						error: function(msg){
							console.log(msg.responseText);
						}
						
					}); 
	
	}
	
	// Placeholder
	$('input.styler, textarea.styler').placeholder();
	
	// Checkbox, Radio, Select
	$('.selectlang').styler();
	/*$('input.radio, input.checkbox, .selectbox').styler();*/

	// Navmenu in mobile
	
	$('.footer').after('<div class="navmenu_overlay"></div>');
	$('.showmenu').on('click', function() {
		$('.navmenu_ul').addClass('open');
		$('.navmenu_overlay').show();
		return false;
	});
	$('.navmenu_overlay').on('click touchstart', function() {
		$('.navmenu_ul').removeClass('open');
		$('.navmenu_overlay').hide();
	});
	
	
	// Fancybox
	$('.album_gallery .grid3 a').fancybox({
		fitToView	: false,
		autoSize	: false,
		width		: '98%',
		height		: '98%',
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		padding		: 0,
		modal		: false
	});
	
	
	
	

		
	// Ontop button
	$(window).scroll(function() {
		var to_top = $(document).scrollTop();
		if (to_top < 300) {
			$('#ontop').fadeOut(200);
		} else {
			$('#ontop').fadeIn(200);
		}
	});

	$("#ontop").click(function () {
		$("body, html").animate({
			scrollTop: 0
		}, 500);
		return false;
	});
	
	
	// Tabs
	$('#tabsbox').tabs({
		collapsible: false,
		hide: { effect: "fade", duration: 100 },
		show: { effect: "fade", duration: 100 }
	});
	

	// Rating
	$('.rating').append('<span></span><span></span><span></span><span></span><span></span>');
	/*$('a.open_forget').click(function(event) {
		event.preventDefault();
		$('.forget').toggle();
	});	*/
	/*$('.participant').hide();
	$('.government-departments').hide();
	*/
	
	$('input[name="poltica"]').on('change', function() {
			if($(this).prop('checked') == false){
				$(this).parents('form').find('.btn').addClass('noact');
			}
			else{
				$(this).parents('form').find('.btn').removeClass('noact');
			}
			
		});
	/*$('select[name*="[participant-category]"').on('change', function() {
			if($(this).val() == '1367'){
				$('.participant').show();
				$('input[name*="[best-cases]"]').show();
				$('input[name*="[best-cases]"]').attr('required', 'required');
			}
			else if($(this).val() == '1364'){
				$('.participant').hide();
				$('.government-departments').hide();
				$('input[name*="[best-cases]"]').hide();
			}
			else{
				$('.participant').hide();
				$('.government-departments').hide();
				$('input[name*="[best-cases]"]').show();
				$('input[name*="[best-cases]"]').attr('required', 'required');
			}
	});*/
	
	
	if (document.location.href.indexOf('/user-account/?_err') != -1) {
	  $('.formbox.forget').show();
	} 
	//$('input[name*="[phone-number]"]').attr('pattern', '^([\+]{0,1}[0-9]{5,})?$');
	$('input[name*="[email]"]').attr('type', 'email');

	$('input[name*="forget_login"]').attr('pattern', '^(([a-zа-яё0-9_-]+\.)*[a-zа-яё0-9_-]+@[a-zа-яё0-9-]+(\.[a-zа-яё0-9-]+)*\.[a-zа-яё]{2,6})?$');
	//$('input[name*="[year-of-birth]"]').attr('pattern', '^[ 0-9]+$');
	
	//empty_field = 'Поле обязательно к заполнению';
	//invalid_field = 'Поле заполнено неверно';
	/* $('form .btn').click(function(event){
		 
		event.preventDefault();
		
		block = $(this).parents('form');
		
		method = block.attr('data-method');
		var formData = block.serialize();
		var requestUrl = block.attr('action');
		
		if(block.find('input:invalid').length){
		
			block.find('input:invalid').each(function(){
				
				if(!($(this).hasClass('invalid')))
				{	$(this).addClass('invalid');
					$(this).removeClass('valid');
					
				}
				
				
			});
			block.find('input:valid').each(function(){
		
				$(this).removeClass('invalid');
			});	
		}
		else{
		
			block.find('input').each(function(){
				$(this).removeClass('invalid');
				
			});
			if(block.find('input[name="poltica"]').length){
				if(block.find('input[name="poltica"]').prop('checked') == true ){
				
					
					if(method == 'ajax'){
						$.ajax({
							url: requestUrl+'/.json',
							type: 'POST',
							dataType: 'json',
							data: formData,
							})
							.done(function(data) {
								 if(data.success!='false'){
									console.log(data);
								 }
								 else{
									

								 }
								 
							})
							.fail(function(msg) {
								
							})
							.always(function() {
								
							});
					}
					else{
						block.submit();
					}
				}
			}
			else{
				if(method == 'ajax'){
						$.ajax({
							url: requestUrl+'/.json',
							type: 'POST',
							dataType: 'json',
							data: formData,
							})
							.done(function(data) {
								 if(data.success!='false'){
									console.log(data);
									
									 $('.error').show();
								 }
								 else{
									

								 }
								 
							})
							.fail(function(msg) {
								 location.reload();
							})
							.always(function() {
								
							});
					}
					else{
						block.submit();
					}
			}
			
		}
		
	});*/
	
	
	/*$('form input').focusout(function(){
		if($(this).is(':invalid')){
			if(!($(this).hasClass('invalid')))
				{
					$(this).removeClass('valid');
					$(this).addClass('invalid');
			
				}
	
			
				
		}
		else{
			$(this).removeClass('invalid');
			$(this).addClass('valid');
			if($(this).attr('name') == 'data[new][email]')
			{
				value = $(this).val();
				$('input[name="login"]').val(value);
				$('input[name="email"]').val(value);
			}
			if($(this).attr('name') == 'data[new][namen]')
			{
				value = $(this).val();
				$('input[name*="[fname]"]').val(value);
				
			}
		}
	});*/
	

});
