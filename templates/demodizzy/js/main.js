$(document).ready(function() {
	if (document.location.href.indexOf('/en/') == -1) {
		$.datepicker.setDefaults($.datepicker.regional['ru']);
		//alert('ru');
	} 
	if (document.location.href.indexOf('/en/') != -1) {
		$.datepicker.setDefaults($.datepicker.regional['en-GB']);
		//alert('en');
	} 
	if (document.referrer.indexOf('/program/') == -1) {
		$('a.btn.return').hide();
	}
	
	
	$('.showmenu').append('<b></b><b></b><b></b>');
	$('.footer').after('<div class="navmenu_overlay"></div>');
	$('.showmenu').on('click', function() {
		$('.navmenu_ul').addClass('open');
		$('.navmenu_overlay').show();
		return false;
	});
	$('.navmenu_overlay').on('click touchstart', function() {
		$('.navmenu_ul').removeClass('open');
		$('.navmenu_overlay').hide();
	});
	//$(".datepicker").datepicker();
	$('a.open_forget').click(function(event) {
		event.preventDefault();
		$('.forget').toggle();
	});	
	$('a.open_hotel_form').click(function(event) {
		event.preventDefault();
		id = $(this).attr('href');
		$(id).toggle();
	});	
	$(".datepicker").datepicker({
   onSelect: function(dateText, inst) {$(this).removeClass('invalid'); $(this).addClass('valid');}
});
	$('#headerslider').owlCarousel({
		items: 1,
		margin: 0,
		smartSpeed: 450,
		stopOnHover: true,
		autoplayHoverPause: true,
		autoplay: true,
		nav: false,
		dots: true,
		
		autoplayTimeout: 5000,
		mouseDrag: true,
		controlsClass: 'container'
	});

	$('.selectlang').styler();
	$('input.radio, input.checkbox, .selectbox, input[type="file"]').styler();
	//$('input.radio, input.checkbox').styler();
	$('#tabsbox').tabs({
		collapsible: false,
		hide: { effect: "fade", duration: 100 },
		show: { effect: "fade", duration: 100 }
	});
	
	$('form input, form select').focusout(function(){
		if($(this).is(':invalid')){
			if(!($(this).hasClass('invalid')))
				{
					$(this).removeClass('valid');
					$(this).addClass('invalid');
					
			
				}
			//alert($(this).attr('name'));
			
				
		}
		else{
			$(this).removeClass('invalid');
			$(this).addClass('valid');
			if($(this).attr('name') == 'data[new][email]')
			{
				value = $(this).val();
				$('input[name="login"]').val(value);
				$('input[name="email"]').val(value);
			}
			if($(this).attr('name') == 'data[new][namen]')
			{
				value = $(this).val();
				$('input[name="data[new][fname]"]').val(value);
				
			}
			if($(this).hasClass('up'))
			{
				value = $(this).val();
				$(this).val(value.toUpperCase());
				
			}
			if($(this).attr('type') == 'file'){
						$(this).parents('.jq-file').addClass('valid');
						$(this).parents('.jq-file').removeClass('invalid');
						
					}
		}
	});
	$('form .btn').click(function(event){
		 
		event.preventDefault();
		
		block = $(this).parents('form');
		
		method = block.attr('data-method');
		var formData = block.serialize();
		var requestUrl = block.attr('action');
		//linkdiv = $(this).attr('id');
		if(block.find('*:invalid').length){
			//alert('не норм 1');
			block.find('*:invalid').each(function(){
				//console.log('invalid '+$(this).attr('name')+'<br/>');
				if(!($(this).hasClass('invalid')))
				{	$(this).addClass('invalid');
					$(this).removeClass('valid');
					if($(this).attr('type') == 'file'){
						$(this).parents('.jq-file').addClass('invalid');
						$(this).parents('.jq-file').removeClass('valid');
						
					}
				}
				if($(this).attr('type') == 'file'){
						$(this).parents('.jq-file').addClass('invalid');
						$(this).parents('.jq-file').removeClass('valid');
						
					}
				
			});
			block.find('*:valid').each(function(){
			//	console.log('valid '+$(this).attr('name')+'<br/>');
				$(this).removeClass('invalid');
				if($(this).attr('type') == 'file'){
						$(this).parents('.jq-file').removeClass('invalid');
						
					}
			});	
		}
		else{
			//alert('норм 1');
			block.find('input, select').each(function(){
				$(this).removeClass('invalid');
				if($(this).attr('type') == 'file'){
					$(this).parents('.jq-file').removeClass('invalid');
						
				}
				
			});
		
			if(block.find('input[name="poltica"]').length){
				if(block.find('input[name="poltica"]').prop('checked') == true ){
					//alert('норм');
					
					if(method == 'ajax'){
						$.ajax({
							url: requestUrl+'/.json',
							type: 'POST',
							dataType: 'json',
							data: formData,
							})
							.done(function(data) {
								 if(data.success!='false'){
									console.log(data);
								 }
								 else{
									

								 }
								 
							})
							.fail(function(msg) {
								
							})
							.always(function() {
								
							});
					}
					else{
						block.submit();
					}
				}
			}
			else{
				if(method == 'ajax'){
						$.ajax({
							url: requestUrl+'/.json',
							type: 'POST',
							dataType: 'json',
							data: formData,
							})
							.done(function(data) {
								 if(data.success!='false'){
									console.log(data);
									// block.append("<p>".i18n.user_reauth."</p>");
									 $('.error').show();
								 }
								 else{
									

								 }
								 
							})
							.fail(function(msg) {
								 location.reload();
							})
							.always(function() {
								
							});
					}
					else{
						block.submit();
					}
			}
			
		}
		
	});
	if (document.location.href.indexOf('/user-account/?_err') != -1) {
	  $('.formbox.forget').show();
	} 
	$('input[name*="[best-cases]"]').attr('required', 'required');
	$('select[name*="[participant-category]"]').on('change', function() {
	//	alert('open');
			if($(this).val() == '1367'){
				$('.participant').styler();
				$('.participant').addClass('selectbox');
				$('.participant').show();
				$('select[name*="[participant]"]').attr('required', 'required');
				$('input[name*="[best-cases]"]').show();
				$('input[name*="[best-cases]"]').attr('required', 'required');
			}
			else if($(this).val() == '1368'){
				$('.participant').hide();
				$('select[name*="[participant]"]').attr('required', false);
				$('.government-departments').hide();
				$('input[name*="[best-cases]"]').hide();
				$('input[name*="[best-cases]"]').attr('required', false);
			}
			else{
				$('.participant').hide();
				$('select[name*="[participant]"]').attr('required', false);
				$('.government-departments').hide();
				$('select[name*="[government-departments]"]').attr('required', false);
				$('input[name*="[best-cases]"]').show();
				$('input[name*="[best-cases]"]').attr('required', 'required');
			}
	});
	$('select[name*="[participant]"]').on('change', function() {
		//alert('open');
			if($(this).val() == '1376'){
				$('.government-departments').styler();
				$('.government-departments').addClass('selectbox');
				$('.government-departments').show();
				$('select[name*="[government-departments]"]').attr('required', 'required');
			}
			else{
				
				$('.government-departments').hide();
				$('select[name*="[government-departments]"]').attr('required', false);
			}
	});
// Rating
	$('.rating').append('<span></span><span></span><span></span><span></span><span></span>');
	
	$('.selectlang a').on('click', function(event) {
		event.preventDefault();
		value = $(this).attr('data-value');
			if(value == 'RUS'){
				//alert('rus');
				var url = window.location.href;
					url2 = '/en/';
				url = url.replace (new RegExp (url2, 'g'), '/');
				document.location.href = url;
				return false;
			}
			else{
				//alert('eng');
				var host = window.location.host;
				var pathname = window.location.pathname;
				url = 'http://'+host+'/en'+pathname;
				//alert(url);
				document.location.href = url;
				return false;
			}
			
		});
	/*$('.participant').hide();
	$('.government-departments').hide();*/
});