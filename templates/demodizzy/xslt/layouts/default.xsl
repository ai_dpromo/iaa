<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:umi="http://www.umi-cms.ru/TR/umi"
				xmlns:xlink="http://www.w3.org/TR/xlink">

	<xsl:template match="/" mode="layout">
	
<!--	<xsl:choose>
		<xsl:when test="document('udata://custom/getUserIp')/udata = '94.19.22.99' or document('udata://custom/getUserIp')/udata = '109.172.15.19'" >-->
		<html lang="ru">
		<head>
			<meta charset="utf-8" />
			
			<meta http-equiv="X-UA-Compatible" content="IE=edge" />
			<meta name="viewport" content="width=device-width, initial-scale=1"/>
			<link rel="shortcut icon" href="/favicon.ico" />
		
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta name="keywords" content="{//meta/keywords}" />
			<meta name="description" content="{//meta/description}" />
			<title><xsl:value-of select="$name" /></title>
			<link href="/templates/demodizzy/css/jquery.fancybox.css" rel="stylesheet" type="text/css" />
			<link href="/templates/demodizzy/css/owl.carousel.css" rel="stylesheet" type="text/css" />
			<link href="/templates/demodizzy/css/style2.css" rel="stylesheet" type="text/css" />
			<link href="/templates/demodizzy/css/responsive.css" rel="stylesheet" type="text/css" />
			<link href="/templates/demodizzy/css/dp.css" rel="stylesheet" type="text/css" />
			<link href="/templates/demodizzy/css/kiv.css" rel="stylesheet" type="text/css" />
			<link media="print" type="text/css" rel="stylesheet" href="/templates/demodizzy/css/print.css" />
			
		</head>

		<body class="">
			<xsl:choose>
				<xsl:when test="result[page/@is-default = '1']" priority="1">
					<xsl:attribute name="class">
						<xsl:text>home_page</xsl:text>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					
				</xsl:otherwise>
			</xsl:choose>
		<div class="mainbox">
		<xsl:if test="not(result[page/@is-default = '1'])">
			<xsl:choose>
				<xsl:when test="$document-page-id mod 3 = 1">
					<xsl:attribute name="style">
						<xsl:text>background: url(/templates/demodizzy/images/bg1.png) center no-repeat;</xsl:text>
					</xsl:attribute>
				</xsl:when>
				<xsl:when test="$document-page-id mod 3 = 2">
					<xsl:attribute name="style">
						<xsl:text>background: url(/templates/demodizzy/images/bg2.png) center no-repeat;</xsl:text>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="style">
						<xsl:text>background: url(/templates/demodizzy/images/bg3.png) center no-repeat;</xsl:text>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
			<header class="header">
				<xsl:variable name="banner">
					<xsl:if test="result[page/@is-default = '1']">
						<xsl:choose>
							<xsl:when test="$lang-prefix = '/en' ">
								<xsl:value-of select="325"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="323"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
					<xsl:if test="not(result[page/@is-default = '1'])">
						<xsl:choose>
							<xsl:when test="$lang-prefix = '/en' ">
								<xsl:value-of select="373"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="371"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
				</xsl:variable>
				<div id="headerslider">
					<xsl:for-each select="document(concat('udata://content/menu///',$banner,'//2'))/udata/items/item">
						<div class="item item{position()}">
						<xsl:if test="document(concat('upage://', @id, '.slajd'))//value">
							<xsl:attribute name="style">
								background-image: url(<xsl:value-of select="document(concat('upage://', @id, '.slajd'))//value"/>)
							</xsl:attribute>
						</xsl:if>
							<div class="container">
								<xsl:value-of select="document(concat('upage://', @id, '.content'))//value" disable-output-escaping="yes"/>
								
							</div><!-- #container -->
						</div><!-- #item -->	
					</xsl:for-each>
					
				</div><!-- #headerslider -->
			</header><!-- #Header -->
			<div class="logo_print print">
				<xsl:variable name="banner-logo">
						<xsl:choose>
							<xsl:when test="$lang-prefix = '/en'">
								<xsl:text>/templates/demodizzy/images/logo-eng-bottom.svg</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>/templates/demodizzy/images/logo-rus-bottom.svg</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<img src="{$banner-logo}"/>
			</div>

			<nav class="navmenu">
				<div class="container">
					<div class="showmenu"></div>
					<ul class="navmenu_ul">
						<xsl:choose>
							<xsl:when test="$lang-prefix = '/en'">
								<xsl:for-each select="document('udata://menu/draw/main_en')/udata/item" >
									
									<li >
										<xsl:if test="position() = 1">
											<xsl:if test="@link = $request-uri">
												<xsl:attribute name="class">
													<xsl:text>active</xsl:text>
												</xsl:attribute>
											</xsl:if>
										</xsl:if>
										<xsl:if test="position() != 1">
											<xsl:if test="@status">
												<xsl:attribute name="class">
													<xsl:text>active</xsl:text>
												</xsl:attribute>
											</xsl:if>
										</xsl:if>
									<a href="{@link}"><xsl:value-of select="@name"/></a></li>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<xsl:for-each select="document('udata://menu/draw/main')/udata/item">
									<li data="{@status}">
										<xsl:if test="position() = 1">
											<xsl:if test="@link = $request-uri">
												<xsl:attribute name="class">
													<xsl:text>active</xsl:text>
												</xsl:attribute>
											</xsl:if>
										</xsl:if>
										<xsl:if test="position() != 1">
											<xsl:if test="@status">
												<xsl:attribute name="class">
													<xsl:text>active</xsl:text>
												</xsl:attribute>
											</xsl:if>
										</xsl:if>
									
									<a href="{@link}"><xsl:value-of select="@name"/></a></li>
								</xsl:for-each>
							</xsl:otherwise>
						</xsl:choose>
					</ul>
					<div class="rt">
						<!--<xsl:choose>
							<xsl:when test="$lang-prefix = '/en'">
								<a href="/en/home/user-account" class="lk_link">User account</a>
							</xsl:when>
							<xsl:otherwise>
								<a href="/home/user-account" class="lk_link">Личный кабинет</a>
							</xsl:otherwise>
						</xsl:choose>-->
						<div class="selectlang">
							<xsl:choose>
								<xsl:when test="$lang-prefix = '/en'">
									
									<a href="#" data-value="RUS">RUS</a>
								</xsl:when>
								<xsl:otherwise>
									<a href="#" data-value="ENG">ENG</a>
									
								</xsl:otherwise>
							</xsl:choose>
						</div>
						<!--<select class="selectlang">
							<xsl:choose>
								<xsl:when test="$lang-prefix = '/en'">
									<option value="ENG" selected="selected">ENG</option>
									<option value="RUS">RUS</option>
								</xsl:when>
								<xsl:otherwise>
									<option value="ENG">ENG</option>
									<option value="RUS" selected="selected">RUS</option>
								</xsl:otherwise>
							</xsl:choose>
							
						</select>-->
					</div>
				</div>
			</nav><!-- #navmenu -->


			<div class="container container_middle">
				<xsl:choose>
					<xsl:when test="result[page/@is-default = '1']" priority="1">
						<xsl:variable name="sp">
							<xsl:choose>
								<xsl:when test="$lang-prefix = '/en'">
									<xsl:value-of select="491"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="490"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<div class="middle">
						<!--	<div class="pagetitle">&welcome-speech;</div>

							<div class="greeting_row">
								<xsl:for-each select="document(concat('udata://content/menu///',$sp,'//2'))/udata/items/item">
									
									<div class="greeting">
										<a href="{@link}">
											<div class="greeting_img"><img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{@name}"/></div>
											<div class="greeting_text">
												<span class="greeting_name"><xsl:value-of select="@name"/></span>
												<span class="greeting_type"><xsl:value-of select="document(concat('upage://', @id, '.dolzhnost'))//value"/></span>
												<xsl:value-of select="document(concat('upage://', @id, '.anons'))//value" disable-output-escaping="yes"/>
											</div>
											<div class="greeting_logo"><img src="{document(concat('upage://', @id, '.logotip'))//value}" alt="{@name}"/></div>
										</a>
									</div>
								</xsl:for-each>
								
							</div>-->
							<div class="pagetitle">&dear-colleagues;</div>
								<div class="greeting_row">
									<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
								</div>

							<div class="pagetitle">&INDUSTRY-PARTNERS;</div>

							<div class="partners">
								<xsl:variable name="ip">
									<xsl:choose>
										<xsl:when test="$lang-prefix = '/en'">
											<xsl:value-of select="295"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="297"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:for-each select="document(concat('udata://content/menu///',$ip,'//2'))/udata/items/item">
									<a target="_blank" href="{document(concat('upage://', @id, '.link'))//value}"><span><img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{@name}"/></span></a>
								</xsl:for-each>
							
								
							</div><!-- #partners -->

						</div><!-- #middle -->
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="result" />
					</xsl:otherwise>
				</xsl:choose>

				<footer class="footer">
				<xsl:variable name="banner-bottom">
						<xsl:choose>
							<xsl:when test="$lang-prefix = '/en'">
								<xsl:text>/templates/demodizzy/images/logo-eng-bottom.svg</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>/templates/demodizzy/images/logo-rus-bottom.svg</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<div class="row">
						<div class="grid4 hidemb">
							<div class="footlogo">
								<a href="#"><img src="{$banner-bottom}" alt=""/></a>
							</div>
							<a href="http://www.iaaglobal.org/" target="_blank"><div class="iaalogo"></div></a>
						</div>
						<div class="grid2 footmenu">
							<ul>
								<xsl:choose>
									<xsl:when test="$lang-prefix = '/en'">
										<xsl:for-each select="document('udata://menu/draw/main_en')/udata/item" >
											<xsl:if test="@is-active = '1'">
												<li data="{@status}">
													
													<a href="{@link}"><xsl:value-of select="@name"/></a></li>
											</xsl:if>
										</xsl:for-each>
									</xsl:when>
									<xsl:otherwise>
										<xsl:for-each select="document('udata://menu/draw/main')/udata/item">
											<xsl:if test="@is-active = '1'">
												<li><a href="{@link}"><xsl:value-of select="@name"/></a></li>
											</xsl:if>
										</xsl:for-each>
									</xsl:otherwise>
								</xsl:choose>
								
							</ul>
						</div>
						<div class="grid3 foot_app">
							<div class="inline">
								<div class="app_mob"></div>
								<div class="foot_app_rt">
									<span>&mobile-app;</span>
									<a href="{$site-settings[@name = 'ssylka']/value}" target="_blank"  class="google_play"></a>
									<a href="{$site-settings[@name = 'ssylka_ehpstor']/value}" target="_blank" class="appstore"></a>
								</div>
							</div>
						</div>
						<div class="grid3 footphone">
							<span>&EXECUTIVE-BOARD;</span>
							<b><xsl:value-of select="$site-settings[@name = 'telefon']/value"/></b>
							<a href="{$site-settings[@name = 'ssylka_ne_telegram']/value}" target="_blank" class="soc1"></a>
							<a href="{$site-settings[@name = 'ssylka_na_fejsbuk']/value}" target="_blank" class="soc2"></a>
						</div>
					</div><!-- #row -->
					<div class="copyright">
						<div class="copyright_left">
								&#169; «&international-ad-issues-summit;», 2017<br/>
							<a href="{$lang-prefix}/home/privacy-policy">&privacy-policy;</a>
						</div>
						<a href="http://dpromo.su" target="_blank" class="dpromo"></a>
					</div>
				</footer><!-- #footer -->
			</div><!-- #container_middle -->
		</div>

		<div id="ontop"></div>


		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
		<script src="/templates/demodizzy/js/vendor/owl.carousel.min.js" type="text/javascript"></script>
		<script src="/templates/demodizzy/js/jquery-ui-i18n.js" type="text/javascript"></script>
		<script src="/templates/demodizzy/js/vendor/jquery.formstyler.min.js" type="text/javascript"></script>
		<script src="/templates/demodizzy/js/vendor/jquery.fancybox.js" type="text/javascript"></script>
		<script src="/templates/demodizzy/js/vendor/jquery.waypoints.min.js" type="text/javascript"></script>
		<script src="/templates/demodizzy/js/vendor/jquery.placeholder.min.js" type="text/javascript"></script>
		<script src="/templates/demodizzy/js/vendor/respond.min.js" type="text/javascript"></script>
		<script src="/templates/demodizzy/js/vendor/modernizr.min.js" type="text/javascript"></script>
		<script src="/templates/demodizzy/js/main.js" type="text/javascript"></script>
		<script src="/templates/demodizzy/js/common.js" type="text/javascript"></script>
		
		<xsl:value-of select="$site-settings[@name = 'kod_schetchika_yandeks_metrika']/value" disable-output-escaping="yes"/>
		</body>
	</html>
	<!--</xsl:when>
	<xsl:otherwise>
		<html>
			<head>
				<meta http-equiv="X-UA-Compatible" content="IE=edge" />
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<meta name="keywords" content="{//meta/keywords}" />
				<meta name="description" content="{//meta/description}" />
				<title><xsl:value-of select="$document-title" /></title>
				
				
			<link rel="stylesheet" media="all" href="/templates/demodizzy/css/style.css"/>
			<meta name="yandex-verification" content="7244c113e21d0c33" />
			<meta name="viewport" content="width=device-width"/>
			<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
		</head>

			<body>
			<script type="text/javascript">
				$( document ).ready(function() {
					$('body').height($(window).height());
				});
				
			</script>
				
				
			<script type="text/javascript">
				(function (d, w, c) {
					(w[c] = w[c] || []).push(function() {
						try {
							w.yaCounter45245334 = new Ya.Metrika({
								id:45245334,
								clickmap:true,
								trackLinks:true,
								accurateTrackBounce:true,
								webvisor:true
							});
						} catch(e) { }
					});

					var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
					s.type = "text/javascript";
					s.async = true;
					s.src = "https://mc.yandex.ru/metrika/watch.js";

					if (w.opera == "[object Opera]") {
						d.addEventListener("DOMContentLoaded", f, false);
					} else { f(); }
				})(document, window, "yandex_metrika_callbacks");
			</script>
			<noscript><div><img src="https://mc.yandex.ru/watch/45245334" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
			

				
			</body>
		</html>
		</xsl:otherwise>
		</xsl:choose>-->
	</xsl:template>
	
</xsl:stylesheet>
