<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output encoding="utf-8" method="html" indent="yes" />

	<xsl:template match="mail_registrated_subject">
		&registration; <xsl:value-of select="domain" />
	</xsl:template>
	
	<xsl:template match="mail_registrated_subject_noactivation">
		&registration; <xsl:value-of select="domain" />
	</xsl:template>
	
	<xsl:template match="mail_registrated_noactivation">
		<p>
			<xsl:text>Здравствуйте, </xsl:text>
			<xsl:value-of select="lname" />
			<xsl:text> </xsl:text>
			<xsl:value-of select="fname" />
			<xsl:text> </xsl:text>
			<xsl:value-of select="father_name" />
			<xsl:text>,</xsl:text><br />
			<xsl:text>Вы зарегистрировались на сайте </xsl:text>
			<a href="http://{domain}">
				<xsl:value-of select="domain" />
			</a>.
		</p>
		<p>
			<xsl:text>Логин: </xsl:text>
			<xsl:value-of select="login" /><br />
			<xsl:text>Пароль: </xsl:text>
			<xsl:value-of select="password" />
		</p>
	</xsl:template>

	<xsl:template match="mail_registrated_old">
		<p>
			<xsl:text>&welcome; </xsl:text>
			
			<xsl:text>&registered-on-the-site; </xsl:text>
			<a href="http://{domain}">
				<xsl:value-of select="domain" />
			</a>.
		</p>
		
		<p>
		<p>
			&registration-activation-note;
			
		</p>
			
		</p>
	</xsl:template>
	<xsl:template match="mail_registrated">
		<p>
			<xsl:text>&hello; </xsl:text>
			
			<xsl:text>&application; </xsl:text>
			
		</p>
		
		<p>
			<p>
				<xsl:text>&best-regards;</xsl:text><br/>
				
				<xsl:text>&best-regards2;</xsl:text>
			</p>
			
		</p>
	</xsl:template>


	<xsl:template match="mail_admin_registrated_subject">
		<xsl:text>Зарегистрировался новый пользователь</xsl:text>
	</xsl:template>

	<xsl:template match="mail_admin_registrated">
		<p>
			
			
			
			<table cellspacing="0">
				<xsl:for-each select="document(concat('uobject://', user_id))/udata/object/properties//property[@name != 'groups' and @name != 'target' and @name != 'kod_dlya_prilozheniya' and @name != 'login' and @name != 'email' and @name != 'fname']">
					
						<tr>
							<td  style="border:1px solid #ccc; padding:3px; margin:0;">
								<xsl:value-of select="title" /> :
							</td>
							<td  style="border:1px solid #ccc; padding:3px; margin:0;">
								<xsl:choose>
									
									<xsl:when test="value/item">
										
										
										<xsl:value-of select="value/item/@name" /><br/>
									</xsl:when>
									<xsl:when test="value/@path">
									
										<a href="{value}"><xsl:value-of select="value" /></a><br/>
									</xsl:when>
									
									<xsl:otherwise>
										<xsl:value-of select="value" /><br/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
				</xsl:for-each>
				
			</table>
		</p>
	</xsl:template>

</xsl:stylesheet>