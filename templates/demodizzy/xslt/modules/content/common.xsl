<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">
	<xsl:template match="result[@module = 'content']">
		<div class="middle">
		
			<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>

			<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>

			<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
			
		</div><!-- #middle -->
	</xsl:template>
	
	<xsl:template match="result" mode="header">
		<h1>
			<xsl:value-of select="@header" />
		</h1>
	</xsl:template>
	
	<xsl:template match="result[@pageId]" mode="header">
		<h1 umi:element-id="{@pageId}" umi:field-name="h1" umi:empty="&empty-page-name;">
			<xsl:value-of select="@header" />
		</h1>
	</xsl:template>
	<xsl:template match="result[@module = 'content'][@method = 'content'][page/@type-id = '158']">
		<div class="middle">
		<xsl:variable name="parents" select="parents"/>
			
			
		
			<div class="row">
				<div class="grid3 sidebar">
					<div class="leftmenu">
						<ul>
							<xsl:for-each select="document(concat('udata://content/menu///', parents/page[2]/@id , '//2'))/udata/items/item">
								<xsl:if test="@id = '253' or @id ='274'">
											<xsl:if test="$user-info//property[@name = 'dostup_v_mobilnoe_prilozhenie']/value = '1'">
												<li>
													<xsl:if test="$request-uri = @link">
														<xsl:attribute name="class">
															active
														</xsl:attribute>
													</xsl:if>
													
													
													
													<a href="{@link}"><xsl:value-of select="@name" /></a>
												</li>
											</xsl:if>
										</xsl:if>
										<xsl:if test="@id != '253' and @id !='274'">
											
												<li>
													<xsl:if test="$request-uri = @link">
														<xsl:attribute name="class">
															active
														</xsl:attribute>
													</xsl:if>
													
												
													
													<a href="{@link}"><xsl:value-of select="@name" /></a>
												</li>
											
										</xsl:if>
							</xsl:for-each>
							
						</ul>
					</div><!-- #leftmenu -->

					
					
				</div><!-- #sidebar -->
				<div class="showmb">
					<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
					<div class="pagetitle">
						
						<xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" />
							
					</div>
				</div>

				<div class="grid9 rightcol">
			
					<div class="hidemb">
						<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
						<div class="pagetitle">
							
							<xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" />
							
						
						</div>
			
					</div>

					
							<div class="hotel_row">
					
								<xsl:for-each select="document(concat('udata://content/menu///', page/@id , '//2'))/udata/items/item">
									
									<div class="hotel">
										<div class="hotel_inner">
											<div class="hotel_img"><a href="{@link}"><img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{@name}"/></a></div>
											<div class="hotel_text">
												
												<div class="hotel_name"><a href="{@link}"><xsl:value-of select="@name" /></a></div>
												<xsl:value-of select="document(concat('upage://', @id, '.anons'))//value" disable-output-escaping="yes" />
											</div>
										</div>
									</div><!-- #hotel -->
								</xsl:for-each>
							</div>
							
						<xsl:value-of select="document(concat('upage://', page/@parentId, '.content'))//value" disable-output-escaping="yes" />
				
				
				</div><!-- #rightcol -->
			</div><!-- #row -->

		</div><!-- #middle -->
	</xsl:template>
	<xsl:template match="result[@module = 'content'][@method = 'content'][page/@type-id = '153']">
		<div class="middle">
		<xsl:variable name="parents" select="parents"/>
		
			
			
			<div class="row">
				<div class="grid3 sidebar">
					<div class="leftmenu">
						<ul>
							<xsl:for-each select="document(concat('udata://content/menu///', parents/page[2]/@id , '//2'))/udata/items/item">
										<li>
											<xsl:if test="$parents/page[3]/@link = @link">
												<xsl:attribute name="class">
													active
												</xsl:attribute>
											</xsl:if>
											<a href="{@link}"><xsl:value-of select="@name" /></a>
										</li>
							</xsl:for-each>
							
						</ul>
					</div><!-- #leftmenu -->
					<a href="javascript:(print());" class="readmore print_link">&print;</a>
				</div><!-- #sidebar -->
				<div class="showmb">
					<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
					<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
					<xsl:if test="@id = 254 or @id = 275"><div class="pagetitle_small">/ &day-0; /</div></xsl:if>
				</div>


				<div class="grid9 rightcol">
			
					<div class="hidemb">
						<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
						<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
						<xsl:if test="@id = 254 or @id = 275"><div class="pagetitle_small">/ &day-0; /</div></xsl:if>
					</div>
				<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
				
				<xsl:for-each select="document(concat('udata://content/menu///', page/@id , '//2'))/udata/items/item">
						
							<div class="event">
								<xsl:if test="document(concat('upage://', @id, '.ne_otobrazhat_vremya_v_spiske'))//value = '1'">
									<xsl:attribute name="class">
										event event2
									</xsl:attribute>
								</xsl:if>
								<div class="event_tb">
									<xsl:if test="not(document(concat('upage://', @id, '.ne_otobrazhat_vremya_v_spiske'))//value)">
										<span class="event_date">
											<xsl:value-of select="document(concat('upage://', @id, '.vremya'))//value" />
										</span>
									</xsl:if>
									<div class="event_name"><xsl:value-of select="document(concat('upage://', @id, '.mesto'))//value" /></div>
									
									<div class="event_text">
										<xsl:if test="document(concat('upage://', @id, '.content'))//value"><a href="{@link}"><xsl:value-of select="@name" /></a></xsl:if>
										<xsl:if test="not(document(concat('upage://', @id, '.content'))//value)"><xsl:value-of select="@name" /></xsl:if>
										<xsl:if test="document(concat('upage://', @id, '.anons'))//value"><small><xsl:value-of select="document(concat('upage://', @id, '.anons'))//value" disable-output-escaping="yes"/>	</small></xsl:if>
									</div>
								</div>
							</div><!-- #event -->				
										
				</xsl:for-each>
				<!--<xsl:if test="document(concat('udata://content/menu///', page/@id , '//2?extProps=spikery'))/udata/items/item//property[@name = 'spikery']/value/page/@id">
					<xsl:variable name="sp_m" select="document(concat('udata://content/menu///', page/@id , '//2?extProps=spikery'))/udata/items/item//property[@name = 'spikery']/value/page/@id" />
					<br/>			
					<div class="pagetitle3">&speakers;</div>
							<div class="row predsrow spikers">
								<xsl:for-each select="document('usel://speakers_all/')/udata/page">
										<xsl:if test="@id = $sp_m">			
											<div class="grid6">
												<div class="predsbox">
													<img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{name}"/>
													<a href="{@link}"><span><xsl:value-of select="name" /></span></a>
													<p><xsl:value-of select="document(concat('upage://', @id, '.dolzhnost'))//value" /></p>
												</div>
											</div>
										</xsl:if>
								</xsl:for-each>
								
							</div>
					</xsl:if>-->

				</div><!-- #rightcol -->
			</div><!-- #row -->

		</div><!-- #middle -->
	</xsl:template>
	<xsl:template match="result[@module = 'content'][@method = 'content'][page/@id = '356' or page/@id = '358']">
		<div class="middle">
		<xsl:variable name="parents" select="parents"/>
		<xsl:variable name="parent" select="page/@parentId"/>
		<xsl:variable name="menu">
			<xsl:choose>
					<xsl:when test="$lang-prefix = '/en'">
						<xsl:value-of select="258"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="237"/>
					</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
			
			<div class="row">
			
				<div class="grid3 sidebar">
					<div class="leftmenu">
						<ul>
							<xsl:for-each select="document(concat('udata://content/menu///', $menu , '//2'))/udata/items/item">
										<li>
											<xsl:if test="$parents/page[3]/@link = @link">
												<xsl:attribute name="class">
													active
												</xsl:attribute>
											</xsl:if>
											<a href="{@link}"><xsl:value-of select="@name" /></a>
										</li>
							</xsl:for-each>
						</ul>
					</div><!-- #leftmenu -->
					<a href="javascript:(print());" class="readmore print_link">&print;</a>
				</div><!-- #sidebar -->
		
			<div class="showmb">
				<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
				<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
				
			</div>

				<div class="grid9 rightcol">
					
					<div class="hidemb">
						
						<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
						<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value"/></div>
						
					</div>
					<div class="row predsrow spikers">
								<xsl:for-each select="document('usel://speakers_all/')/udata/page">
											
											<div class="grid6">
												<div class="predsbox">
													<img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{name}"/>
													<a href="{@link}"><span><xsl:value-of select="name" /></span></a>
													<p><xsl:value-of select="document(concat('upage://', @id, '.dolzhnost'))//value" /></p>
												</div>
											</div>
								
								</xsl:for-each>
								
							</div><!-- #orgrow -->
					
				</div><!-- #rightcol -->
			</div><!-- #row -->

		</div><!-- #middle -->
		
	</xsl:template>
	<xsl:template match="result[@module = 'content'][@method = 'content'][page/@type-id = '156' or page/@type-id = '150']">
		<div class="middle">
		<xsl:variable name="parents" select="parents"/>
		<xsl:variable name="parent" select="page/@parentId"/>
		<xsl:variable name="menu">
			<xsl:choose>
					<xsl:when test="$lang-prefix = '/en'">
						<xsl:value-of select="258"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="237"/>
					</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
			

			<div class="row">
			<xsl:if test="page/@type-id = '156'">
				<div class="grid3 sidebar">
					<div class="leftmenu">
						<ul>
							<xsl:for-each select="document(concat('udata://content/menu///', $menu , '//2'))/udata/items/item">
										<li>
											<xsl:if test="$parents/page[3]/@link = @link">
												<xsl:attribute name="class">
													active
												</xsl:attribute>
											</xsl:if>
											<a href="{@link}"><xsl:value-of select="@name" /></a>
										</li>
							</xsl:for-each>
						</ul>
					</div><!-- #leftmenu -->
					<a href="javascript:(print());" class="readmore print_link">&print;</a>
				</div><!-- #sidebar -->
			</xsl:if>
				<div class="showmb">
					<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
					<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
					
					<div class="pagetitle_small"><xsl:value-of select=".//property[@name = 'dolzhnost']/value" /></div>
					<xsl:if test="page/@type-id = '156'">
						<a class="btn return" href="#" onclick="history.back();return false;">&return;</a>
					</xsl:if>
				</div>
				<div class="">
					<xsl:if test="page/@type-id = '156'">
						<xsl:attribute name="class">
							<xsl:text>grid9 rightcol</xsl:text>
						</xsl:attribute>
					</xsl:if>
					<div class="hidemb">
						
							<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
						<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value"/></div>
						
						<div class="pagetitle_small"><xsl:value-of select=".//property[@name = 'dolzhnost']/value" /></div>
						<xsl:if test="page/@type-id = '156'">
							<a class="btn return" href="#" onclick="history.back();return false;">&return;</a>
						</xsl:if>
					</div>

					<div class="greeting greeting_single">
						<div class="greeting_img"><img src="{.//property[@name = 'menu_pic_ua']/value}" alt="{.//property[@name = 'h1']/value}"/></div>
						<div class="greeting_text">
							<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
						</div>
					</div><!-- #greeting -->
					<xsl:variable name="day" select="document(concat('udata://content/menu///', $menu , '//2'))/udata/items/item[1]/@id"/>
					<xsl:for-each select="document(concat('udata://content/menu///', $day , '//2'))/udata/items/item">
					<xsl:if test="document(concat('usel://speakers/?page=', $document-page-id, '&amp;id=', @id))/udata/page">
						<h2 style="text-transform:uppercase"><xsl:value-of select="@name" /></h2>
						<xsl:for-each select="document(concat('usel://speakers/?page=', $document-page-id, '&amp;id=', @id))/udata/page">
							<div class="event event_single">
								<div class="event_tb">
									<span class="event_date"><xsl:value-of select="document(concat('upage://', @id, '.vremya'))//value" /></span>
									<div class="event_name"><xsl:value-of select="document(concat('upage://', @id, '.mesto'))//value" /></div>
									<div class="event_text">
										<a href="{@link}"><xsl:value-of select="name" /></a>
										<xsl:variable name="topic" select="document(concat('usel://topic/?page=', @id, '&amp;id=', $document-page-id))/udata/page"/>
											<div class="topic">	
												<span><xsl:if test="document(concat('usel://topic/?page=', @id, '&amp;id=', $document-page-id))/udata/page">&topic;</xsl:if></span>
												<p><xsl:value-of select="$topic/name" /></p>
											</div>
									
									</div>
									
								</div>
							</div><!-- #event -->				
						</xsl:for-each>
						</xsl:if>
					</xsl:for-each>
				</div><!-- #rightcol -->
			</div><!-- #row -->

		</div><!-- #middle -->

	</xsl:template>
	<xsl:template match="result[@module = 'content'][@method = 'content'][page/@type-id = '140']">
	
		<div class="middle">
			
			<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
			
			

			<div class="pagetitle">&about-summit;</div>
			<div class="about_text">
				<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
				
			</div><!-- #about_text -->

			<div class="pagetitle">&ORGANIZERS;</div>
			<div class="bordrow">
			
				<div class="row orgrow">
					<xsl:for-each select=".//property[@name = 'organizatory']/value/page">
						<div class="grid4 orgbox">
							<a  href="{document(concat('upage://', @id, '.link'))//value}">
							<xsl:if test="contains(document(concat('upage://', @id, '.link'))//value, 'http')">
								<xsl:attribute name="target">
									<xsl:text>_blank </xsl:text>
								</xsl:attribute>
							</xsl:if>
							<img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt=""/>
							
							<span><xsl:value-of select="name"/></span></a>
						</div>
					</xsl:for-each>
					
				
				</div>
			</div><!-- #orgrow -->
			
				<!--<xsl:variable name="gp">
							<xsl:choose>
								<xsl:when test="$lang-prefix = '/en'">
									<xsl:value-of select="491"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="490"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
				<div class="greeting_row">
								<xsl:for-each select="document(concat('udata://content/menu///',$gp,'//2'))/udata/items/item">
									
									<div class="greeting">
										<a href="{@link}">
											<div class="greeting_img"><img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{@name}"/></div>
											<div class="greeting_text">
												<span class="greeting_name"><xsl:value-of select="@name"/></span>
												<span class="greeting_type"><xsl:value-of select="document(concat('upage://', @id, '.dolzhnost'))//value"/></span>
												<xsl:value-of select="document(concat('upage://', @id, '.anons'))//value" disable-output-escaping="yes"/>
											</div>
											<div class="greeting_logo"><img src="{document(concat('upage://', @id, '.logotip'))//value}" alt="{@name}"/></div>
										</a>
									</div>
								</xsl:for-each>
								
							</div>-->
			

			<div class="pagetitle">&CO-CHAIRMEN;</div>
				<xsl:variable name="sp">
					<xsl:choose>
						<xsl:when test="$lang-prefix = '/en'">
							<xsl:value-of select="291"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="289"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<div class="bordrow">
			
					<div class="row orgrow">
						<xsl:for-each select="document(concat('udata://content/menu///',$sp,'//2'))/udata/items/item">
							<div class="grid4">
								<div class="predsbox">
									<a href="{@link}">
										<img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{@name}"/> 
										<span><xsl:value-of select="@name"/></span>
										<p><xsl:value-of select="document(concat('upage://', @id, '.dolzhnost'))//value" /></p>
									</a>
								</div>
							</div>
						</xsl:for-each>
					</div>
				</div>

			<div class="pagetitle">&ORGANIZING-COMMITTEE;</div>
			<xsl:for-each select=".//property[@name = 'organizacionnyj_komitet']/value/page">
				<xsl:value-of select="document(concat('upage://', @id, '.content'))//value" disable-output-escaping="yes" />
			</xsl:for-each>
			

			<div class="pagetitle">&INDUSTRY-PARTNERS;</div>
			<div class="bordrow">
				<div class="photorow">
					<xsl:for-each select=".//property[@name = 'industrialnye_partnery']/value/page">
						<a target="_blank" href="{document(concat('upage://', @id, '.link'))//value}">
							<span><img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt=""/></span>
						</a>
					</xsl:for-each>
				</div><!-- #photorow -->
			</div><!-- #bordrow -->

			<div class="pagetitle">&EXECUTIVE-BOARD;</div>
			<xsl:value-of select=".//property[@name = 'ispolnitelnaya_direkciya']/value" disable-output-escaping="yes" />
		</div>
	</xsl:template>
	<xsl:template match="result[@module = 'content'][@method = 'content'][page/@type-id = '159']">
		<div class="middle">
			
			<xsl:variable name="parent" select="parents"/>
			<div class="row">
				<div class="grid3 sidebar">
				
					<div class="leftmenu">
						<ul>
							
									<xsl:for-each select="document(concat('udata://content/menu///', parents/page[2]/@id , '//2'))/udata/items/item">
										
										
										<xsl:if test="@id = '253' or @id ='274'">
											<xsl:if test="$user-info//property[@name = 'dostup_v_mobilnoe_prilozhenie']/value = '1'">
												<li>
													<xsl:if test="@link = $parent/page[3]/@link">
														<xsl:attribute name="class">
															active
														</xsl:attribute>
													</xsl:if>
													
													
													
													<a href="{@link}"><xsl:value-of select="@name" /></a>
												</li>
											</xsl:if>
										</xsl:if>
										<xsl:if test="@id != '253' and @id !='274'">
											
												<li>
													<xsl:if test="@link = $parent/page[3]/@link">
														<xsl:attribute name="class">
															active
														</xsl:attribute>
													</xsl:if>
													
												
													
													<a href="{@link}"><xsl:value-of select="@name" /></a>
												</li>
											
										</xsl:if>
										
									</xsl:for-each>
								
						</ul>
					</div><!-- #leftmenu -->
				</div><!-- #sidebar -->
			<div class="showmb">
				<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
				<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
			</div>
				<div class="grid9 rightcol">
					<xsl:apply-templates select="document('udata://core/navibar/')/udata" mode="hidemb"/>
					<div class="pagetitle_float">
						<div class="pagetitle hidemb"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
						
					</div><!-- #pagetitle_float -->
					<div class="greeting greeting_single">
						
						<xsl:if test=".//property[@name = 'menu_pic_ua']/value"><div><img src="{.//property[@name = 'menu_pic_ua']/value}" alt="{.//property[@name = 'h1']/value}"/></div>
							<xsl:if test="parents/page[2]/@type-id = '149'">
								<br/><div class="datebox"><xsl:value-of select=".//property[@name = 'cena']/value" disable-output-escaping="yes" /></div>
							</xsl:if>
								<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
							
						</xsl:if>
						<xsl:if test="not(.//property[@name = 'menu_pic_ua']/value)">
							<xsl:if test="parents/page[2]/@type-id = '149'">
								<br/><div class="datebox"><xsl:value-of select=".//property[@name = 'cena']/value" disable-output-escaping="yes" /></div>
							</xsl:if>
							<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
						</xsl:if>
					</div><!-- #greeting -->
					
						<xsl:value-of select="document(concat('upage://', parents/page[3]/@id, '.content'))//value" disable-output-escaping="yes" />
				</div><!-- #row -->
			</div>
		
		</div>
	</xsl:template>
	<xsl:template match="result[@module = 'content'][@method = 'content'][page/@type-id = '155' or page/@type-id='154']">
		<div class="middle">
		<xsl:variable name="parents" select="parents"/>
			

			<div class="row">
				<div class="grid3 sidebar">
					<div class="leftmenu">
						<ul>
							<xsl:for-each select="document(concat('udata://content/menu///', parents/page[2]/@id , '//2'))/udata/items/item">
										<li>
											<xsl:if test="$parents/page[3]/@link = @link">
												<xsl:attribute name="class">
													active
												</xsl:attribute>
											</xsl:if>
											<a href="{@link}"><xsl:value-of select="@name" /></a>
										</li>
							</xsl:for-each>
						</ul>
					</div><!-- #leftmenu -->
					<a href="javascript:(print());" class="readmore print_link">&print;</a>
				</div><!-- #sidebar -->
				<div class="showmb">
					<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
					<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
					<xsl:if test=".//property[@name = 'anons']/value and page/@type-id='155'">
						<div class="pagetitle_small"><xsl:value-of select=".//property[@name = 'anons']/value" disable-output-escaping="yes"/></div>
					</xsl:if>
				</div>

				<div class="grid9 rightcol">
			
					<div class="hidemb">
						<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
						<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
							<xsl:if test=".//property[@name = 'anons']/value and page/@type-id='155'">
							<div class="pagetitle_small"><xsl:value-of select=".//property[@name = 'anons']/value" disable-output-escaping="yes" /></div>
						</xsl:if>
					</div>
					
					<xsl:if test=".//property[@name = 'vremya']/value or .//property[@name = 'mesto']/value">
						<div class="datebox"><xsl:if test=".//property[@name = 'vremya']/value"><xsl:value-of select=".//property[@name = 'vremya']/value"/>, </xsl:if><xsl:value-of select=".//property[@name = 'mesto']/value"/></div>
					</xsl:if>
					<article class="post_text">
						<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
					</article>

					
					
					<xsl:if test=".//property[@name = 'spiker']/value">
						<div class="pagetitle3">&speakers;</div>
						<div class="row predsrow spikers">
							<xsl:for-each select=".//property[@name = 'spiker']/value/page">
												
									<div class="grid6">
										<div class="predsbox">
											<img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{@name}"/>
											<a href="{@link}"><span><xsl:value-of select="name" /></span></a>
											<p><xsl:value-of select="document(concat('upage://', @id, '.dolzhnost'))//value" /></p>
										</div>
									</div>
							</xsl:for-each>
							
						</div><!-- #orgrow -->
					</xsl:if>
					<xsl:if test=".//property[@name = 'spikery']/value">
						<div class="pagetitle3">&speakers;</div>
						<div class="row predsrow spikers">
							<xsl:for-each select=".//property[@name = 'spikery']/value/page">
												
									<div class="grid6">
										<div class="predsbox">
											<img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{@name}"/>
											<a href="{@link}"><span><xsl:value-of select="name" /></span></a>
											<p><xsl:value-of select="document(concat('upage://', @id, '.dolzhnost'))//value" /></p>
											<xsl:variable name="topic" select="document(concat('usel://topic/?page=', $document-page-id, '&amp;id=', @id))/udata/page"/>
											<div class="topic">	
												<span><xsl:if test="document(concat('usel://topic/?page=', $document-page-id, '&amp;id=', @id))/udata/page">&topic;</xsl:if></span>
												<p><xsl:value-of select="$topic/name" /></p>
											</div>
										</div>
									</div>
								<!--	<div class="grid6">
										<div class="predsbox topic">
											
												<xsl:variable name="topic" select="document(concat('usel://topic/?page=', $document-page-id, '&amp;id=', @id))/udata/page"/>
												
												<span><xsl:if test="document(concat('usel://topic/?page=', $document-page-id, '&amp;id=', @id))/udata/page">&topic;</xsl:if></span>
												<p><xsl:value-of select="$topic/name" /></p>
											
										</div>
									</div>-->
							</xsl:for-each>
							
						</div><!-- #orgrow -->
					</xsl:if>
				</div><!-- #rightcol -->
			</div><!-- #row -->

		</div><!-- #middle -->

	</xsl:template>
	<xsl:template match="result[@module = 'content'][@method = 'content'][page/@type-id = '148']">
		<div class="middle">
			
			<xsl:variable name="parent" select="parents"/>
			<div class="row">
				<div class="grid3 sidebar">
				
					<div class="leftmenu">
						<ul>
							
									<xsl:for-each select="document(concat('udata://content/menu///', parents/page[2]/@id , '//2'))/udata/items/item">
										
											<li>
												<xsl:if test="@link = $parent/page[3]/@link">
												
													<xsl:attribute name="class">
														active
													</xsl:attribute>
												</xsl:if>
												<a href="{@link}"><xsl:value-of select="@name"/></a></li>
										
									</xsl:for-each>
								
						</ul>
					</div><!-- #leftmenu -->
				</div><!-- #sidebar -->
			<div class="showmb">
				<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
				<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
			</div>
				<div class="grid9 rightcol">
					<xsl:apply-templates select="document('udata://core/navibar/')/udata" mode="hidemb"/>
					<div class="pagetitle_float">
						<div class="pagetitle hidemb"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
						
					</div><!-- #pagetitle_float -->
					<div class="greeting greeting_single">
						<div class="rating rating-{document(concat('upage://', $document-page-id, '.rejting'))//value}"></div>
						<br/>
						<xsl:if test=".//property[@name = 'menu_pic_ua']/value"><img src="{.//property[@name = 'menu_pic_ua']/value}" alt="{.//property[@name = 'h1']/value}"/>
							
								<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
							
						</xsl:if>
						<xsl:if test="not(.//property[@name = 'menu_pic_ua']/value)">
							<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
						</xsl:if>
					</div><!-- #greeting -->
				</div><!-- #row -->
			</div>
		
		</div>
	</xsl:template>
	<xsl:template match="result[@module = 'content'][@method = 'content'][page/@type-id = '141']">
		<xsl:variable name="redirect">
			<xsl:choose>
					<xsl:when test="$lang-prefix = '/en'">
						<xsl:value-of select="307"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="305"/>
					</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="document(concat('udata://content/redirect/', $redirect))" />
	</xsl:template>
	<xsl:template match="result[@module = 'content'][@method = 'content'][page/@type-id = '149']">
		<div class="middle">
		<xsl:variable name="menu">
			<xsl:choose>
					<xsl:when test="$lang-prefix = '/en'">
						<xsl:value-of select="263"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="242"/>
					</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!--<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
		<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>-->
			<xsl:choose>
				<xsl:when test="$user-type = 'guest'">
					<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
					<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
					<div class="row usercab">
						<div class="grid4">
							<div class="formbox">
								<form class="login" action="{$lang-prefix}/users/login_do/" method="post"  data-method="ajax">
									<input type="text"  class="styler" placeholder="E-mail" required="required" name="login" />
									<input type="password" class="styler" placeholder="&password;" required="required" name="password"  />
									<!--<input type="email" name="email" class="styler" placeholder="E-mail" required="required"/>-->
									<!--<input type="password" name="password" class="styler" placeholder="Пароль"/>-->
									<input type="submit" value="&log-in;" class="btn"/>
								</form>
								<div class="error"><p><xsl:text>&user-reauth;</xsl:text></p></div>
							</div>
							<p class="recall"><a class="open_forget" href="#">&forget-password;</a></p>
							<div class="formbox forget" >
								<form id="forget" method="post" action="{$lang-prefix}/users/forget_do/" >
									<input type="text"  class="styler" placeholder="E-mail" required="required"  name="forget_login" />
									
									
									<input type="submit" class="btn" value="&forget-button;" />
										
								</form>
								<xsl:apply-templates select="document('udata://system/listErrorMessages')" />
							</div>
						</div>
						<div class="grid8">
							<div class="usercab_text">
								<p>&we-send-you-details;</p>
								<p><a href="{$lang-prefix}/home/accreditation/accreditation-form/">&fill-in-the-accreditation-form;</a></p>
							</div>
						</div>
					</div><!-- #usercab -->

				</xsl:when>
				<xsl:otherwise>
					
					
					<div class="row">
						<div class="grid3 sidebar">
							<div class="leftmenu">
								<ul data="{$user-info//property[@name = 'dostup_v_mobilnoe_prilozhenie']/value}">
									<xsl:for-each select="document(concat('udata://content/menu///', $menu , '//2'))/udata/items/item">
										<xsl:if test="@id = '253' or @id ='274'">
											<xsl:if test="$user-info//property[@name = 'dostup_v_mobilnoe_prilozhenie']/value = '1'">
												<li>
													<xsl:if test="$request-uri = @link">
														<xsl:attribute name="class">
															active
														</xsl:attribute>
													</xsl:if>
													
													<xsl:if test="position() = 1" >
														<xsl:if test="$document-page-id = '242' or $document-page-id = '263'">
															<xsl:attribute name="class">
																active
															</xsl:attribute>
														</xsl:if>
													</xsl:if>
													
													<a href="{@link}"><xsl:value-of select="@name" /></a>
												</li>
											</xsl:if>
										</xsl:if>
										<xsl:if test="@id != '253' and @id !='274'">
											
												<li>
													<xsl:if test="$request-uri = @link">
														<xsl:attribute name="class">
															active
														</xsl:attribute>
													</xsl:if>
													
													<xsl:if test="position() = 1" >
														<xsl:if test="$document-page-id = '242' or $document-page-id = '263'">
															<xsl:attribute name="class">
																active
															</xsl:attribute>
														</xsl:if>
													</xsl:if>
													
													<a href="{@link}"><xsl:value-of select="@name" /></a>
												</li>
											
										</xsl:if>
									</xsl:for-each>
									
								</ul>
								
							</div><!-- #leftmenu -->
							<a href="{$lang-prefix}/users/logout/" class="btn">
									<xsl:text>&log-out;</xsl:text>
								</a>
						</div>
						<div class="showmb">
								<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
								<div class="pagetitle">
									
									<xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" />
									
								
								</div>
								
					
							</div>
						<div class="grid9 rightcol">
			
							<div class="hidemb">
								<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
								<div class="pagetitle">
									
									<xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" />
									
								
								</div>
								
					
							</div>
							<xsl:choose>
								<xsl:when test="page/@id = '494' or page/@id = '495'">
									<!--<xsl:variable name="sp">
										<xsl:choose>
											<xsl:when test="$lang-prefix = '/en'">
												<xsl:value-of select="266"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="245"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:variable>-->
									<xsl:for-each select="document(concat('udata://content/menu///', page/@id , '//2'))/udata/items/item">
										<div class="obrprog_banner">
											<a href="{@link}"><img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{name}"/></a>
										</div>
										<!-- #obrprog -->
									</xsl:for-each>
									
								</xsl:when>
								<xsl:when test="page/@id = '242' or page/@id = '263'">
								
									<div class="formbox">
										<xsl:for-each select="$user-info/udata//group[@name = 'akkreditaciya1']/property">
											<xsl:choose>
												<xsl:when test="@type='relation'">
													<input type="text" name="fio" class="styler" value='{title}: {value/item/@name}' disabled='disabled'/>
												</xsl:when>
												<xsl:otherwise>
													<input type="text" name="fio" class="styler" value='{title}: {value}' disabled='disabled'/>
												</xsl:otherwise>
											</xsl:choose>
											
										</xsl:for-each>
											
										
									</div>
									<div class="formbox">
										<xsl:for-each select="$user-info/udata//group[@name = 'akkreditaciya2']/property">
											<xsl:choose>
												<xsl:when test="@type='relation'">
													<input type="text" name="fio" class="styler" value='{title}: {value/item/@name}' disabled='disabled'/>
												</xsl:when>
												<xsl:otherwise>
													<input type="text" name="fio" class="styler" value='{title}: {value}' disabled='disabled'/>
												</xsl:otherwise>
											</xsl:choose>
											
										</xsl:for-each>
									</div>
									<div class="formbox">
										<h3 style="text-align:center">&BADGE;</h3>
										<xsl:for-each select="$user-info/udata//group[@name = 'informaciya_dlya_bejdzha']/property">
											<xsl:choose>
												<xsl:when test="@type='relation'">
													<input type="text" name="fio" class="styler" value='{title}: {value/item/@name}' disabled='disabled'/>
												</xsl:when>
												<xsl:otherwise>
													<input type="text" name="fio" class="styler" value='{title}: {value}' disabled='disabled'/>
												</xsl:otherwise>
											</xsl:choose>
											
										</xsl:for-each>
									</div>
									<div class="formbox">
										<xsl:for-each select="$user-info/udata//group[@name = 'akkreditaciya3']/property">
											<xsl:choose>
												<xsl:when test="@type='relation'">
													<input type="text" name="fio" class="styler" value='{title}: {value/item/@name}' disabled='disabled'/>
												</xsl:when>
												<xsl:otherwise>
													<input type="text" name="fio" class="styler" value='{title}: {value}' disabled='disabled'/>
												</xsl:otherwise>
											</xsl:choose>
											
										</xsl:for-each>
									</div>
										<div class="formbox">
										<xsl:for-each select="$user-info/udata//group[@name = 'akkreditaciya4']/property">
											<xsl:if test="@name != 'kod_dlya_prilozheniya'">
												<xsl:choose>
													<xsl:when test="@type='relation'">
														<input type="text" name="{@name}" class="styler" value='{title}: {value/item/@name}' disabled='disabled'/>
													</xsl:when>
													<xsl:otherwise>
														<input type="text" name="{@name}" class="styler" value='{title}: {value}' disabled='disabled'/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
										</xsl:for-each>
									</div>
									
								</xsl:when>
								<xsl:when test="page/@id = '274'">
									<xsl:if test="$user-info//property[@name = 'dostup_v_mobilnoe_prilozhenie']/value = '1'">
											<div class="apppost">
												<div class="apppost_image"><img src="/templates/demodizzy/images/pics/app.png" alt=""/></div>
												<div class="apppost_text">
													<p>For comfort stay at the Summit please use the mobile app</p>
													<ul>
														<li><span class="num">1)</span> <a href="#">Download App</a></li>
														<li><span class="num">2)</span> Please use your personal code <br/><span class="code"><xsl:value-of select="$user-info/udata//property[@name = 'kod_dlya_prilozheniya']/value"/></span></li>
													</ul>
												</div>
											</div><!-- #apppost -->
									</xsl:if>
									<xsl:if test="not($user-info//property[@name = 'dostup_v_mobilnoe_prilozhenie']/value)">
										<xsl:variable name="redirect" select="page/@parentId" />
										<xsl:value-of select="document(concat('udata://content/redirect/', $redirect))" />
									</xsl:if>
								</xsl:when>
								<xsl:when test="page/@id = '253'">
								<xsl:if test="$user-info//property[@name = 'dostup_v_mobilnoe_prilozhenie']/value = '1'">
										<div class="apppost">
											<div class="apppost_image"><img src="/templates/demodizzy/images/pics/app.png" alt=""/></div>
											<div class="apppost_text">
											<p>Для организации комфортного пребывания на Саммите, Вам доступно мобильное приложение.</p>
												<ul>
													<li><span class="num">1)</span> <a href="#">Скачайте приложение</a></li>
													<li><span class="num">2)</span>  Введите персональный код <br/><span class="code"><xsl:value-of select="$user-info/udata//property[@name = 'kod_dlya_prilozheniya']/value"/></span></li>
												</ul>
											</div>
										</div><!-- #apppost -->
										</xsl:if>
									<xsl:if test="not($user-info//property[@name = 'dostup_v_mobilnoe_prilozhenie']/value)">
										<xsl:variable name="redirect" select="page/@parentId" />
										<xsl:value-of select="document(concat('udata://content/redirect/', $redirect))" />
									</xsl:if>
								</xsl:when>
								<xsl:when test="page/@id = '252'">
									<xsl:if test="$user-info/udata//property[@name = 'rek']/value/page">
										<div class="pagetitle_warn">
											<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
										</div>
									</xsl:if>
										<div class="hotel_row">
											<xsl:for-each select="$user-info/udata//property[@name = 'rek']/value/page">
												
												<div class="hotel">
													<div class="hotel_inner">
														<div class="hotel_img"><a href="{document(concat('upage://', @id, '.ssylka'))//value}"><img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{name}"/></a></div>
														<div class="hotel_text">
															<div class="rating rating-{document(concat('upage://', @id, '.rejting'))//value}"></div>
															<div class="hotel_name"><a href="{document(concat('upage://', @id, '.ssylka'))//value}"><xsl:value-of select="name" disable-output-escaping="yes" /></a></div>
															<p><xsl:value-of select="document(concat('upage://', @id, '.anons'))//value" disable-output-escaping="yes" /></p>
															<a href="#id{@id}" class="readmore print_link open_hotel_form">&booking;</a>
															<div class="hotel_form" id="id{@id}">
															
																<form xmlns="http://www.w3.org/1999/xhtml" method="post" action="/webforms/send/"   enctype="multipart/form-data">
																	<div class="formbox">
																		<xsl:choose>
																			<xsl:when test="$user-info//property[@name = 'inostranec']/value = '1'">
																				<input type="hidden" name="system_email_to" value="1823"/>
																			</xsl:when>
																			<xsl:otherwise>
																				<input type="hidden" name="system_email_to" value="1839"/>
																			</xsl:otherwise>
																		</xsl:choose>
																	
																		<input type="text" name="data[new][arrivaldate]" required="required" class="styler datepicker" placeholder="&arrival-date;" value=""/>
																		<input type="text" name="data[new][departuredate]" required="required" class="styler datepicker" placeholder="&departure-date;" value=""/>
																		<input type="text" name="data[new][hotel]" required="required" class="styler" placeholder="&hotel;" value="{name}"/>
																		<input type="text" name="data[new][name]" required="required" class="styler" placeholder="&name;" value="{$user-info//property[@name = 'namen']/value}"/>
																		<input type="text" name="data[new][email]" required="required" class="styler" placeholder="&e-mail;" value="{$user-info//property[@name = 'email']/value}"/>
																		<input type="text" name="data[new][honenumber]" required="required" class="styler" placeholder="&phone;" value="{$user-info//property[@name = 'phone-number']/value}"/>
																		<textarea name="data[new][komment]"  class="styler" placeholder="&comment-submit;" ></textarea>
																	</div>		
																	
																		
																		<input type="hidden" name="system_form_id" value="160"/>
																		<input type="hidden" name="ref_onsuccess" value="/webforms/posted/160/"/>
																	
																	<div class="formbox_bottom">
																		<input type="submit" class="btn" value="&send;"/>
																	</div>
																</form>
															
																<!--<xsl:apply-templates select="document('udata://webforms/add/160')/udata"/>-->
															</div>
														</div>
													</div>
													
												</div><!-- #hotel -->
											</xsl:for-each>
											
										</div><!-- #hotel_row -->
								</xsl:when>
									<xsl:when test="page/@id = '273'">
										<div class="pagetitle_warn">
											<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
										</div>

										<div class="hotel_row">
											<xsl:for-each select="$user-info/udata//property[@name = 'rek']/value/page">
												
												<div class="hotel">
													<div class="hotel_inner">
														<div class="hotel_img"><a href="{document(concat('upage://', @id, '.ssylka'))//value}"><img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{name}"/></a></div>
														<div class="hotel_text">
															<div class="rating rating-{document(concat('upage://', @id, '.rejting'))//value}"></div>
															<div class="hotel_name"><a href="{document(concat('upage://', @id, '.ssylka'))//value}"><xsl:value-of select="name" disable-output-escaping="yes" /></a></div>
															<p><xsl:value-of select="document(concat('upage://', @id, '.anons'))//value" disable-output-escaping="yes" /></p>
															<a href="#id{@id}" class="readmore print_link open_hotel_form">&booking;</a>
															<div class="hotel_form" id="id{@id}">
													
																<form xmlns="http://www.w3.org/1999/xhtml" method="post" action="/webforms/send/"   enctype="multipart/form-data">
																	<div class="formbox">
																		<xsl:choose>
																			<xsl:when test="$user-info//property[@name = 'inostranec']/value = '1'">
																				<input type="hidden" name="system_email_to" value="1823"/>
																			</xsl:when>
																			<xsl:otherwise>
																				<input type="hidden" name="system_email_to" value="1839"/>
																			</xsl:otherwise>
																		</xsl:choose>
																		<input type="text" name="data[new][arrivaldate]" required="required" class="styler datepicker" placeholder="&arrival-date;" value=""/>
																		<input type="text" name="data[new][departuredate]" required="required" class="styler datepicker" placeholder="&departure-date;" value=""/>
																		<input type="text" name="data[new][hotel]" required="required" class="styler" placeholder="&hotel;" value="{name}"/>
																		<input type="text" name="data[new][name]" required="required" class="styler" placeholder="&name;" value="{$user-info//property[@name = 'namen']/value}"/>
																		<input type="text" name="data[new][email]" required="required" class="styler" placeholder="&e-mail;" value="{$user-info//property[@name = 'email']/value}"/>
																		<input type="text" name="data[new][honenumber]" required="required" class="styler" placeholder="&phone;" value="{$user-info//property[@name = 'phone-number']/value}"/>
																		<textarea name="data[new][komment]"  class="styler" placeholder="&comment-submit;" ></textarea>
																	</div>		
																	
																		
																		<input type="hidden" name="system_form_id" value="160"/>
																		<input type="hidden" name="ref_onsuccess" value="/webforms/posted/160/"/>
																	
																	<div class="formbox_bottom">
																		<input type="submit" class="btn" value="&send;"/>
																	</div>
																</form>
															
																<!--<xsl:apply-templates select="document('udata://webforms/add/160')/udata"/>-->
															</div>
														
														</div>
													</div>
												
												</div>
											</xsl:for-each>
											
										</div>
								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>  
	<xsl:template match="result[@module = 'content'][@method = 'content'][page/@id = '240' or page/@id = '261']">
		<div class="middle">
		
		
				<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>

				<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
			
			<div id="tabsbox">
				<div class="tabs">
					<ul>
						<xsl:for-each select="document(concat('udata://content/menu///', page/@id , '//2'))/udata/items/item">
							<li>
											
								<a href="#block{position()}"><xsl:value-of select="@name" /></a>
							</li>
						</xsl:for-each>
						
					</ul>
				</div>
				<xsl:for-each select="document(concat('udata://content/menu///', page/@id , '//2'))/udata/items/item">
					<xsl:choose>
						<xsl:when test='position() = 1'>
							<div id="block1">
								<div class="pagetitle3">&INDUSTRY-PARTNERS;</div>
								<div class="row partners_row">
									<xsl:for-each select="document(concat('udata://content/menu///', @id , '//2'))/udata/items/item">
										<div class="grid4">
											<a target="_blank"  href="{document(concat('upage://', @id, '.link'))//value}"><img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{@name}"/></a>
										</div>
									</xsl:for-each>
									
								</div>
							</div>
						</xsl:when>
						<xsl:when test='position() = 2'>
							<xsl:variable name="sp">
								<xsl:choose>
									<xsl:when test="$lang-prefix = '/en'">
										<xsl:value-of select="407"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="405"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							
							<div id="block2">
								<xsl:if test="document(concat('udata://content/menu///',$sp,'//2'))/udata/items/item">
								<div class="pagetitle3">&general-media-partners;</div>
								</xsl:if>
								<xsl:for-each select="document(concat('udata://content/menu///',$sp,'//2'))/udata/items/item">
									<div class="genbox">
										<div class="genbox_inner genbox_inner_banners">
											<div class="genbox_left"><a target="_blank" href="{document(concat('upage://', @id, '.link'))//value}"><img src="{document(concat('upage://', @id, '.logotip'))//value}" alt="{@name}"/></a></div>
											<div class="genbox_right genbox_banner"><img src="{document(concat('upage://', @id, '.banner'))//value}" alt="{@name}"/></div>
										</div>
										<div class="genbox_inner">
											<a href="{@link}">
												<div class="genbox_left genbox_photo"><img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{@name}"/></div>
												<div class="genbox_right">
													<span class="genbox_name">&greeting-to-the-summit-participants;</span>
													<span class="genbox_name2"><xsl:value-of select="@name"/></span>
													<span class="genbox_name3"><xsl:value-of select="document(concat('upage://', @id, '.dolzhnost'))//value"/></span>
													<xsl:value-of select="document(concat('upage://', @id, '.anons'))//value" disable-output-escaping="yes"/>
												</div>
											</a>
										</div>
									</div><!-- #genbox -->
								</xsl:for-each>
								<xsl:for-each select="document(concat('udata://content/menu///', @id,'//2'))/udata/items/item[@id != $sp]">
									<div class="pagetitle3"><xsl:value-of select="@name"/></div>
									<div class="photorow">
										<xsl:for-each select="document(concat('udata://content/menu///', @id ,'//2'))/udata/items/item">
											<a target="_blank" href="{document(concat('upage://', @id, '.link'))//value}">
												<span><img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{@name}"/></span>
											</a>
										</xsl:for-each>
									</div><!-- #photorow -->
								</xsl:for-each>
									
								
							</div>
						</xsl:when>
						<xsl:when test='position() = 3'>
							<div id="block3">
								<xsl:variable name="sp2">
									<xsl:choose>
										<xsl:when test="$lang-prefix = '/en'">
											<xsl:value-of select="719"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="718"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:if test="document(concat('udata://content/menu///',$sp2,'//2'))/udata/items/item">
								<div class="pagetitle3">&general-media-partners;</div>
								</xsl:if>
								<xsl:for-each select="document(concat('udata://content/menu///',$sp2,'//2'))/udata/items/item">
									<div class="genbox">
										<div class="genbox_inner genbox_inner_banners">
											<div class="genbox_left"><a target="_blank" href="{document(concat('upage://', @id, '.link'))//value}"><img src="{document(concat('upage://', @id, '.logotip'))//value}" alt="{@name}"/></a></div>
											<div class="genbox_right genbox_banner"><img src="{document(concat('upage://', @id, '.banner'))//value}" alt="{@name}"/></div>
										</div>
										<div class="genbox_inner">
											<a href="{@link}">
												<div class="genbox_left genbox_photo"><img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{@name}"/></div>
												<div class="genbox_right">
													<span class="genbox_name">&greeting-to-the-summit-participants;</span>
													<span class="genbox_name2"><xsl:value-of select="@name"/></span>
													<span class="genbox_name3"><xsl:value-of select="document(concat('upage://', @id, '.dolzhnost'))//value"/></span>
													<xsl:value-of select="document(concat('upage://', @id, '.anons'))//value" disable-output-escaping="yes"/>
												</div>
											</a>
										</div>
									</div><!-- #genbox -->
								</xsl:for-each>
								<xsl:for-each select="document(concat('udata://content/menu///', @id,'//2'))/udata/items/item[@id != $sp2] ">
									<div class="pagetitle3"><xsl:value-of select="@name"/></div>
									<div class="photorow">
										<xsl:for-each select="document(concat('udata://content/menu///', @id ,'//2'))/udata/items/item">
											<a target="_blank" href="{document(concat('upage://', @id, '.link'))//value}">
												<span><img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{@name}"/></span>
											</a>
										</xsl:for-each>
									</div><!-- #photorow -->
								</xsl:for-each>
							</div>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
				


				


			</div>

		</div>
	</xsl:template>
	<xsl:template match="result[@module = 'content'][@method = 'content'][page/@id = '245' or page/@id = '266']">
		<div class="middle">

			

			<div class="row">
				<div class="grid3 sidebar">
					<div class="leftmenu">
						<ul>
							<xsl:for-each select="document(concat('udata://content/menu///', page/@parentId , '//2'))/udata/items/item">
										<li>
											<xsl:if test="$request-uri = @link">
												<xsl:attribute name="class">
													active
												</xsl:attribute>
											</xsl:if>
											<a href="{@link}"><xsl:value-of select="@name" /></a>
										</li>
							</xsl:for-each>
							
						</ul>
					</div><!-- #leftmenu -->
					<a href="javascript:(print());" class="readmore print_link">&print;</a>
				</div><!-- #sidebar -->
				<div class="showmb">
					<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>

					<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
				</div>

				<div class="grid9 rightcol">
			
					<div class="hidemb">
						<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>

						<div class="pagetitle print"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
					</div>
					<xsl:if test="page/@id = '245'">
					<p>Заявку на экскурсионное обслуживание можно подать в отел по работе с экскурсионными программами по телефону +7 911 261 7611, с 10:00 до 19:00,<br/> или по адресу электронной почты: bookings@u24.services.
					Контактное лицо: Диана Новожилова. <br/>Обращаем Ваше внимание, что стоимость экскурсионного обслуживания оплачивается участником самостоятельно.</p>
					</xsl:if>
					<xsl:for-each select="document(concat('udata://content/menu///', page/@id , '//2'))/udata/items/item">
							<!--<div class="obrprog print">
								<div class="obrprog_date"><xsl:value-of select="document(concat('upage://', @id, '.vremya_i_data'))//value" disable-output-escaping="yes" /></div>
								<div class="obrprog_logo"><a href="{document(concat('upage://', @id, '.ssylka_vuza'))//value}"><img src="{document(concat('upage://', @id, '.logo_vuza'))//value}" alt="{@name}"/></a></div>
								<div class="obrprog_name"><xsl:value-of select="@name" /></div>
								
								<xsl:for-each select="document(concat('upage://', @id, '.spiker'))//value/page">
									<div class="predsbox predsbox_short">
										
										<img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{name}"/>
										<a href="{@link}"><span><xsl:value-of select="name"/></span></a>
										<p><xsl:value-of select="document(concat('upage://', @id, '.dolzhnost'))//value" disable-output-escaping="yes" /></p>
									</div>
								</xsl:for-each>
									
							</div>-->
							
							<div class="obrprog_banner">
								<a href="{@link}"><img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{name}"/></a>
							</div>
							
						
							
							<!-- #obrprog -->
					</xsl:for-each>
					<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
						
					


				</div><!-- #rightcol -->
			</div><!-- #row -->

		</div><!-- #middle -->
	</xsl:template> 
	<xsl:template match="result[@module = 'content'][@method = 'content'][page/@id = '244' or page/@id = '265']">
		<div class="middle">

			

			<div class="row">
				<div class="grid3 sidebar">
					<div class="leftmenu">
						<ul>
							<xsl:for-each select="document(concat('udata://content/menu///', page/@parentId , '//2'))/udata/items/item">
										<li>
											<xsl:if test="$request-uri = @link">
												<xsl:attribute name="class">
													active
												</xsl:attribute>
											</xsl:if>
											<a href="{@link}"><xsl:value-of select="@name" /></a>
										</li>
							</xsl:for-each>
							
						</ul>
					</div><!-- #leftmenu -->
					<a href="javascript:(print());" class="readmore print_link">&print;</a>
				</div><!-- #sidebar -->
				<div class="showmb">
					<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>

					<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
				</div>

				<div class="grid9 rightcol">
			
					<div class="hidemb">
						<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>

						<div class="pagetitle print"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
					</div>
					
					<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
					
					<xsl:for-each select="document(concat('udata://content/menu///', page/@id , '//2'))/udata/items/item">
						<xsl:if test="position() = 1">	
							<h2 style="text-transform:uppercase">&theoretical-unit;</h2>
						</xsl:if>
						<xsl:if test="position() = 2">	
							<h2 style="text-transform:uppercase">&practical-unit;</h2>
						</xsl:if>
						<div class="obrprog print">
								<div class="obrprog_date"><xsl:value-of select="document(concat('upage://', @id, '.vremya_i_data'))//value" disable-output-escaping="yes" /></div>
								<div class="obrprog_logo"><a target="_blank" href="{document(concat('upage://', @id, '.ssylka_vuza'))//value}"><img src="{document(concat('upage://', @id, '.logo_vuza'))//value}" alt="{@name}"/></a></div>
								<div class="obrprog_name">
									<xsl:if test="document(concat('upage://', @id, '.content'))//value">
											<a href="{@link}"><xsl:value-of select="@name"/></a>
										</xsl:if>
										<xsl:if test="not(document(concat('upage://', @id, '.content'))//value)">
											<xsl:value-of select="@name"/>
										</xsl:if>
								
								
								</div>
								<div class="obrprog_text">
								
									<xsl:value-of select="document(concat('upage://', @id, '.anons'))//value" disable-output-escaping="yes" />
								</div>
								
							<!--	<xsl:for-each select="document(concat('upage://', @id, '.spiker'))//value/page">
									<div class="predsbox predsbox_short">
										
										<img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{name}"/>
										
											<a href="{@link}"><span><xsl:value-of select="name"/></span></a>
										
										<p><xsl:value-of select="document(concat('upage://', @id, '.dolzhnost'))//value" disable-output-escaping="yes" /></p>
									</div>
								</xsl:for-each>-->
									
							</div><!-- #obrprog -->
					</xsl:for-each>
						
					


				</div><!-- #rightcol -->
			</div><!-- #row -->

		</div><!-- #middle -->
	</xsl:template> 
	<xsl:template match="result[@module = 'content'][@method = 'content'][page/@id = '264' or page/@id = '243']">
		<div class="middle">

		

			<div class="row">
				<div class="grid3 sidebar">
					<div class="leftmenu">
						<ul>
							<xsl:for-each select="document(concat('udata://content/menu///', page/@parentId , '//2'))/udata/items/item">
										<li>
											<xsl:if test="$request-uri = @link">
												<xsl:attribute name="class">
													active
												</xsl:attribute>
											</xsl:if>
											<a href="{@link}"><xsl:value-of select="@name" /></a>
										</li>
							</xsl:for-each>
							
						</ul>
					</div><!-- #leftmenu -->
				</div><!-- #sidebar -->
			<div class="showmb">
				<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>

				<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
			</div>

				<div class="grid9 rightcol">
			
					<div class="hidemb">
						<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>

						<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
					</div>
					
					<div class="row">
						<xsl:for-each select="document(concat('udata://content/menu///', page/@id , '//2'))/udata/items/item">
							<div class="grid4 progbox">
								<a href="{@link}">
									<div class="progbox_date">
										<xsl:value-of select="@name" />
										<small><xsl:if test="@id = 254 or @id = 275">&day-0;</xsl:if></small>
									</div>
								</a>
								<div class="progbox_text">
									<xsl:value-of select="document(concat('upage://', @id, '.anons'))//value" disable-output-escaping="yes" />
									<a href="{@link}" class="readmore">&read-more;</a>
								</div>
							</div><!-- #progbox -->
						</xsl:for-each>
						
					</div><!-- #row -->


				</div><!-- #rightcol -->
			</div><!-- #row -->

		</div><!-- #middle -->
	</xsl:template>
	<xsl:template match="result[@module = 'content'][@method = 'content'][page/@id = '237' or page/@id = '258']">
		<div class="middle">
		
			<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>

			<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
			<div class="row newsbox_row">
				<xsl:for-each select="document(concat('udata://content/menu///', page/@id , '//2'))/udata/items/item">
					<div class="grid4 newsbox">
						<div class="newsbox_in">
							<xsl:if test="@id != '264'">
							<a href="{@link}"><span class="newsbox_name"><xsl:value-of select="@name" /></span>
							<span class="newsbox_img"><img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{@name}"/></span></a>
							<div class="newsbox_txt"><p>
								<xsl:value-of select="document(concat('upage://', @id, '.anons'))//value" disable-output-escaping="yes" />
							</p></div>
							<a href="{@link}" class="readmore">&read-more;</a></xsl:if>
							<xsl:if test="@id = '264'">
							<span class="newsbox_name"><xsl:value-of select="@name" /></span>
							<span class="newsbox_img"><img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{@name}"/></span>
							<div class="newsbox_txt"><p>
								<xsl:value-of select="document(concat('upage://', @id, '.anons'))//value" disable-output-escaping="yes" />
							</p></div>
							</xsl:if>
						</div>
					</div><!-- #newsbox -->
				</xsl:for-each>
			
				
			</div><!-- #newsbox_row -->

			<div class="pagetitle">&summit-venue;</div>
			<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
			



		</div><!-- #middle -->
	
	</xsl:template>
	<xsl:template match="result[@module = 'content'][@method = 'content'][page/@parentId = '260' or page/@parentId = '239']">
		<div class="middle">
		
			
			
		
			<div class="row">
				<div class="grid3 sidebar">
					<div class="leftmenu">
						<ul>
							<xsl:for-each select="document(concat('udata://content/menu///', page/@parentId , '//2'))/udata/items/item">
								<li>
									<xsl:if test="$request-uri = @link">
										<xsl:attribute name="class">
											active
										</xsl:attribute>
									</xsl:if>
									<a href="{@link}"><xsl:value-of select="@name" /></a>
								</li>
							</xsl:for-each>
							
						</ul>
					</div><!-- #leftmenu -->

					<div class="usermenu">
						<ul>
							<li class="item_reg">&registration-is-open;</li>
							<xsl:if test="page/@id = '310' or page/@id = '311'">
								<li class="item_warn">&all-fields-are-mandatory;</li>
							</xsl:if>
						</ul>
					</div><!-- #usermenu -->
					<xsl:if test="page/@id = '310' or page/@id = '311'">
						<div class="usermenu">
							<ul>
								<li class="item_file"><a href="{.//property[@name = 'rules-of-accreditation-for-mass-media']/value}"><xsl:value-of select=".//property[@name = 'rules-of-accreditation-for-mass-media']/title" disable-output-escaping="yes" /></a></li>
								<li class="item_file"><a href="{.//property[@name = 'rules-of-accreditation-for-participants']/value}"><xsl:value-of select=".//property[@name = 'rules-of-accreditation-for-participants']/title" disable-output-escaping="yes" /></a></li>
							</ul>
						</div><!-- #usermenu -->
					</xsl:if>
				</div><!-- #sidebar -->
			<div class="showmb">
				<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
				<xsl:if test="not($lang-prefix)">
					<xsl:if test="page/@type-id = '147'">
							<p>		Обращаем внимание, что при бронировании номеров в отелях, представленных ниже, вы можете получить скидку на проживание, предоставляемую делегатам, если сообщите оператору полное название Саммита - Всемирный Коммуникационный Саммит. Скидка не действует при on-line бронировании.
						</p>
					</xsl:if>	
				</xsl:if>
				<div class="pagetitle">
					<xsl:choose>
						<xsl:when test="page/@type-id = '147'">
							&summit-hotel;
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" />
						</xsl:otherwise>
					</xsl:choose>
				</div>
			</div>

				<div class="grid9 rightcol">
			
					<div class="hidemb">
						<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
						<xsl:if test="not($lang-prefix)">
							<xsl:if test="page/@type-id = '147'">
								<p>		Обращаем внимание, что при бронировании номеров в отелях, представленных ниже, вы можете получить скидку на проживание, предоставляемую делегатам, если сообщите оператору полное название Саммита - Всемирный Коммуникационный Саммит. Скидка не действует при on-line бронировании.
								</p>
							</xsl:if>	
						</xsl:if>			
						<div class="pagetitle">
							<xsl:choose>
								<xsl:when test="page/@type-id = '147'">
									&official-hotel-of-the-summit;  
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" />
								</xsl:otherwise>
							</xsl:choose>
						
						
						</div>
			
					</div>
<!--<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />-->
					<xsl:choose>
						<xsl:when test="page/@id = '310' or page/@id = '311'">
							<xsl:choose>
								<xsl:when test="$user-type = 'guest'">
									<div class="pagetitle_warn">
										<p>
											<span><xsl:value-of select=".//property[@name = 'tekst_pod_zagolovkom']/value" disable-output-escaping="yes" />
											</span>
										</p>
									</div>
									<xsl:apply-templates select="document('udata://users/registrate/')/udata"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="page/@type-id = '147'">
							<div class="hotel_row">
							<xsl:variable name="so">
								<xsl:choose>
									<xsl:when test="$lang-prefix = '/en'">
										<xsl:value-of select="404"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="403"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
								
									
									<div class="hotel">
										<div class="hotel_inner">
											<div class="hotel_img"><a href="{@link}"><img src="{document(concat('upage://', $so, '.menu_pic_ua'))//value}" alt="{document(concat('upage://', $so, '.h1'))//value}"/></a></div>
											<div class="hotel_text">
												<div class="rating rating-{document(concat('upage://', $so, '.rejting'))//value}"></div>
												<div class="hotel_name"><a href="{document(concat('upage://', $so))/udata/page/@link}"><xsl:value-of select="document(concat('upage://', $so, '.h1'))//value" /></a></div>
												<xsl:value-of select="document(concat('upage://', $so, '.anons'))//value" disable-output-escaping="yes" />
											</div>
										</div>
									</div><!-- #hotel -->
								
							</div>
							<xsl:if test="document(concat('udata://content/menu///', page/@id , '//2'))/udata/items/item[@id != 404 and @id != 403]">
								<div class="pagetitle">&summit-hotel;</div>
							
								<div class="hotel_row">
						
									<xsl:for-each select="document(concat('udata://content/menu///', page/@id , '//2'))/udata/items/item[@id != 404 and @id != 403]">
										
										<div class="hotel">
											<div class="hotel_inner">
												<div class="hotel_img"><a href="{@link}"><img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="{@name}"/></a></div>
												<div class="hotel_text">
													<div class="rating rating-{document(concat('upage://', @id, '.rejting'))//value}"></div>
													<div class="hotel_name"><a href="{@link}"><xsl:value-of select="@name" /></a></div>
													<xsl:value-of select="document(concat('upage://', @id, '.anons'))//value" disable-output-escaping="yes" />
												</div>
											</div>
										</div><!-- #hotel -->
									</xsl:for-each>
								</div>
							</xsl:if>
							<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
						</xsl:when>
						<xsl:otherwise>
						
							<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />

						</xsl:otherwise>
					</xsl:choose>
				
				</div><!-- #rightcol -->
			</div><!-- #row -->

		</div><!-- #middle -->
	</xsl:template>
	
	
	<xsl:template match="result[@module = 'content'][@method = 'content'][parents/page/@id = '259' or parents/page/@id = '238']">
		<div class="middle">

		
		<div class="row">
			<div class="grid3 sidebar">
			
				<div class="leftmenu">
					<ul>
						<xsl:choose>
							<xsl:when test="$lang-prefix = '/en'">
								<xsl:for-each select="document('udata://menu/draw/press_en')/udata/item" >
									<xsl:if test="@is-active = '1'">
										<li>
											<xsl:if test="$request-uri = @link">
												<xsl:attribute name="class">
													active
												</xsl:attribute>
											</xsl:if>
											<a href="{@link}"><xsl:value-of select="@name"/></a></li>
									</xsl:if>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<xsl:for-each select="document('udata://menu/draw/press')/udata/item">
									<xsl:if test="@is-active = '1'">
										<li>
											<xsl:if test="$request-uri = @link">
												<xsl:attribute name="class">
													active
												</xsl:attribute>
											</xsl:if>
										<a href="{@link}"><xsl:value-of select="@name"/></a></li>
									</xsl:if>
								</xsl:for-each>
							</xsl:otherwise>
						</xsl:choose>
					</ul>
				</div><!-- #leftmenu -->
			</div><!-- #sidebar -->
<div class="showmb">
			<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
			<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
		</div>


			<div class="grid9 rightcol">
				<xsl:apply-templates select="document('udata://core/navibar/')/udata" mode="hidemb"/>
				<div class="pagetitle_float">
					<div class="pagetitle hidemb"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
					<!--<div class="pagination">
						<a href="#">&lt;</a>
						<a href="#">1</a>
						<span>2</span>
						<a href="#">3</a>
						<a href="#">4</a>
						<a href="#">5</a>
						<a href="#">&gt;</a>
					</div>-->
				</div><!-- #pagetitle_float -->
				<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
				
			</div><!-- #rightcol -->
		</div><!-- #row -->

	</div><!-- #middle -->
	</xsl:template>
	
	
	<xsl:include href="menu.xsl" />
	<xsl:include href="404.xsl" />
	<xsl:include href="sitemap.xsl" />
	<xsl:include href="recentPages.xsl" />

</xsl:stylesheet>