<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="/result[@module = 'news'][@method = 'rubric']">
		<div class="middle">

		
			<xsl:apply-templates select="document(concat('udata://news/lastlist/', $document-page-id,'?extProps=publish_time,anons,header_pic'))" />
		</div>
	</xsl:template>

	<xsl:template match="udata[@method = 'lastlist']">
		

		<div class="row">
			<div class="grid3 sidebar">
			
				<div class="leftmenu">
					<ul>
						<xsl:choose>
							<xsl:when test="$lang-prefix = '/en'">
								<xsl:for-each select="document('udata://menu/draw/press_en')/udata/item" >
									<li>
										<xsl:if test="$request-uri = @link">
											<xsl:attribute name="class">
												active
											</xsl:attribute>
										</xsl:if>
										<a href="{@link}"><xsl:value-of select="@name"/></a></li>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<xsl:for-each select="document('udata://menu/draw/press')/udata/item">
									<li>
										<xsl:if test="$request-uri = @link">
											<xsl:attribute name="class">
												active
											</xsl:attribute>
										</xsl:if>
									<a href="{@link}"><xsl:value-of select="@name"/></a></li>
								</xsl:for-each>
							</xsl:otherwise>
						</xsl:choose>
					</ul>
				</div><!-- #leftmenu -->
			</div><!-- #sidebar -->
	<div class="showmb">
				<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
				<div class="pagetitle"><xsl:value-of select="document(concat('upage://', $document-page-id, '.h1'))//value" disable-output-escaping="yes" /></div>
			</div>

			<div class="grid9 rightcol">
				<xsl:apply-templates select="document('udata://core/navibar/')/udata" mode="hidemb"/>
				<div class="pagetitle_float">
					<div class="pagetitle hidemb"><xsl:value-of select="document(concat('upage://', $document-page-id, '.h1'))//value" disable-output-escaping="yes" /></div>
					<xsl:apply-templates select="total" />
					<!--<div class="pagination">
						<a href="#">&lt;</a>
						<a href="#">1</a>
						<span>2</span>
						<a href="#">3</a>
						<a href="#">4</a>
						<a href="#">5</a>
						<a href="#">&gt;</a>
					</div>-->
				</div><!-- #pagetitle_float -->
				<xsl:apply-templates select="items/item" mode="news-list" />
						

			</div><!-- #rightcol -->
		</div><!-- #row -->


	
	
		
	</xsl:template>


	<xsl:template match="item" mode="news-list">
		<xsl:variable name="date" select=".//property[@name = 'publish_time']/value/@unix-timestamp"/>
		<div class="newsanonse">
			<a href="{@link}">
				<span class="newsanonse_date"><xsl:apply-templates select="document(concat('udata://system/convertDate/', $date,'/(d.m)'))" /></span>
				<span class="newsanonse_name"><xsl:value-of select="node()" /></span>
				<xsl:if test=".//property[@name = 'header_pic']/value">
					<img src="{.//property[@name = 'header_pic']/value}" alt=""/>
				</xsl:if>
				<xsl:value-of select=".//property[@name = 'anons']/value" disable-output-escaping="yes" />
			</a>
		</div>
	</xsl:template>
</xsl:stylesheet>