<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi"
	xmlns:xlink="http://www.w3.org/TR/xlink">

	<xsl:template match="/result[@module = 'news'][@method = 'item']">
		<xsl:variable name="date" select=".//property[@name = 'publish_time']/value/@unix-timestamp"/>
		<div class="middle">

		
		<xsl:variable name="parent" select="parents"/>
		<div class="row">
			<div class="grid3 sidebar">
			
				<div class="leftmenu">
					<ul>
						<xsl:choose>
							<xsl:when test="$lang-prefix = '/en'">
								<xsl:for-each select="document('udata://menu/draw/press_en')/udata/item" >
									<xsl:if test="@is-active = '1'">
										<li>
											<xsl:if test="@link = $parent/page[3]/@link">
												<xsl:attribute name="class">
													active
												</xsl:attribute>
											</xsl:if>
											<a href="{@link}"><xsl:value-of select="@name"/></a></li>
									</xsl:if>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<xsl:for-each select="document('udata://menu/draw/press')/udata/item">
									<xsl:if test="@is-active = '1'">
										<li>
											<xsl:if test="@link = $parent/page[3]/@link">
												<xsl:attribute name="class">
													active
												</xsl:attribute>
											</xsl:if>
										<a href="{@link}"><xsl:value-of select="@name"/></a></li>
									</xsl:if>
								</xsl:for-each>
							</xsl:otherwise>
						</xsl:choose>
					</ul>
				</div><!-- #leftmenu -->
			</div><!-- #sidebar -->
<div class="showmb">
			<xsl:apply-templates select="document('udata://core/navibar/')/udata"/>
			<div class="pagetitle"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
		</div>
			<div class="grid9 rightcol">
				<xsl:apply-templates select="document('udata://core/navibar/')/udata" mode="hidemb"/>
				<div class="pagetitle_float">
					<div class="pagetitle hidemb"><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></div>
					<!--<div class="pagination">
						<a href="#">&lt;</a>
						<a href="#">1</a>
						<span>2</span>
						<a href="#">3</a>
						<a href="#">4</a>
						<a href="#">5</a>
						<a href="#">&gt;</a>
					</div>-->
				</div><!-- #pagetitle_float -->
				<div class="datebox"><xsl:apply-templates select="document(concat('udata://system/convertDate/', $date,'/(d.m.Y)'))" /></div>
				<div class="greeting greeting_single">
					<xsl:if test=".//property[@name = 'menu_pic_ua']/value"><div class="greeting_img"><img src="{.//property[@name = 'menu_pic_ua']/value}" alt="{.//property[@name = 'h1']/value}"/></div>
						<div class="greeting_text">
							<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
						</div>
					</xsl:if>
					<xsl:if test="not(.//property[@name = 'menu_pic_ua']/value)">
						<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
					</xsl:if>
				</div><!-- #greeting -->
			</div><!-- #row -->
		</div>
	</div><!-- #middle -->
	
		<!--<div umi:element-id="{$document-page-id}" umi:field-name="content" umi:empty="&empty-page-content;">
			<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
		</div>
		
		<xsl:apply-templates select="document('udata://news/related_links')" />
		
		<a href="../">
			<xsl:text>&news-back;</xsl:text>
		</a>
		<div class="social">
			<div class="plusone">
				<div class="g-plusone" data-size="small" data-count="true"></div>
			</div>
			<script type="text/javascript">
				jQuery(document).ready(function(){ jQuery.getScript('//yandex.st/share/share.js', function() {
			new Ya.share({
				'element': 'ya_share1',
				'elementStyle': {
					'type': 'button',
					'linkIcon': true,
					'border': false,
					'quickServices': ['yaru', 'vkontakte', 'facebook', 'twitter', 'odnoklassniki', 'moimir', 'lj']
				},
				'popupStyle': {
					'copyPasteField': true
				}
			 });
					});
				});
			</script>
			<span id="ya_share1"></span>
		</div>-->
	</xsl:template>
	
	<xsl:template match="udata[@method = 'related_links']" />
	
	<xsl:template match="udata[@method = 'related_links' and count(items/item)]">
		<h4>
			<xsl:text>&news-realted;</xsl:text>
		</h4>

		<ul>
			<xsl:apply-templates select="items/item" mode="related" />
		</ul>
	</xsl:template>
	
	<xsl:template match="item" mode="related">
		<li umi:element-id="{@id}">
			<a href="{@link}" umi:field-name="news">
				<xsl:value-of select="." />
			</a>
		</li>
	</xsl:template>

</xsl:stylesheet>