<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet	version="1.0"
				xmlns="http://www.w3.org/1999/xhtml"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:date="http://exslt.org/dates-and-times"
				xmlns:udt="http://umi-cms.ru/2007/UData/templates"
				xmlns:xlink="http://www.w3.org/1999/xlink"
				exclude-result-prefixes="xsl date udt xlink">

	<xsl:param name="template" />

	<xsl:template match="result[@module = 'webforms'][@method = 'posted']">
		<div class="middle">
			<div class="pagetitle">
				<xsl:apply-templates select="document(concat('udata://webforms/posted/', $template,'/'))/udata" />
			</div>
		</div>
	</xsl:template>

	<xsl:template match="udata[@module = 'webforms'][@method = 'posted']">
		<div class="middle">
			<div class="pagetitle">
				<xsl:choose>
					<xsl:when test="$lang-prefix = '/en'">
						The request has been sent.The manager will contact you as soon as possible.
					</xsl:when>
					<xsl:otherwise>
						Заявка отправлена.Менеджер свяжется с вами в ближайшее время.
					</xsl:otherwise>
				</xsl:choose>
			</div>
			<xsl:choose>
				<xsl:when test="$lang-prefix = '/en'">
					<div class="btn" onclick="history.back();">back</div>
				</xsl:when>
				<xsl:otherwise>
					<div class="btn" onclick="history.back();">назад</div>
				</xsl:otherwise>
			</xsl:choose>
			
		</div>
	</xsl:template>

</xsl:stylesheet>