<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="udata[@method = 'getCreateForm' or @method = 'getEditForm']">
		<xsl:apply-templates select="group[@name != 'short_info']" mode="form" />
	</xsl:template>
	
	<xsl:template match="group" mode="form">
		<div class="formbox">
			<xsl:if test="@name = 'informaciya_dlya_bejdzha'">
				<h3 style="text-align:center">&BADGE;</h3>
				<div class="row">
					<xsl:apply-templates select="field" mode="form_up" />
				</div>
			</xsl:if>
			<xsl:if test="@name != 'informaciya_dlya_bejdzha'">
				<div class="row">
					<xsl:apply-templates select="field" mode="form" />
				</div>
			</xsl:if>
		</div>
	</xsl:template>


	<xsl:template match="field" mode="form">
		<input type="text" name="{@input_name}" class="styler" placeholder="{@title}" value="{.}" required="required">
			<xsl:if test="@required = 'required'">
				<xsl:attribute name="required">required</xsl:attribute>
			</xsl:if>
		</input>
	</xsl:template>
	<xsl:template match="field[@name = 'surname' or @name = 'namen']" mode="form">
		<input type="text" name="{@input_name}" class="styler up" placeholder="{@title}" value="{.}" required="required">
			<xsl:if test="@required = 'required'">
				<xsl:attribute name="required">required</xsl:attribute>
			</xsl:if>
		</input>
	</xsl:template>
	<xsl:template match="field" mode="form_up">
		<input type="text" name="{@input_name}" class="styler up" placeholder="{@title}" value="{.}" required="required">
			<xsl:if test="@required = 'required'">
				<xsl:attribute name="required">required</xsl:attribute>
			</xsl:if>
		</input>
	</xsl:template>
	<xsl:template match="field[@name = 'patronymic']" mode="form">
		<xsl:choose>
			<xsl:when test="$lang-prefix = '/en'">
				
			</xsl:when>
			<xsl:otherwise>
				<input type="text" name="{@input_name}" class="styler up"  placeholder="{@title}" value="{.}" required="required">
					<xsl:if test="@required = 'required'">
						<xsl:attribute name="required">required</xsl:attribute>
					</xsl:if>
				</input>
			</xsl:otherwise>
		</xsl:choose>
	
	</xsl:template>
	<xsl:template match="field[@name = 'passport-series' or @name = 'passport-id']" mode="form">
		<div class="grid3">
			<input type="text" name="{@input_name}" class="styler" placeholder="{@title}" value="{.}" required="required">
				<xsl:if test="@required = 'required'">
					<xsl:attribute name="required">required</xsl:attribute>
				</xsl:if>
			</input>
		</div>
	</xsl:template>
	<xsl:template match="field[@name = 'rek' or @name = 'kod_dlya_prilozheniya']" mode="form">
		
	</xsl:template>

	<xsl:template match="field[@type = 'relation']" mode="form">
	
				<select type="text" name="{@input_name}" class="selectbox {@name}" required="required">
					<xsl:if test="@name  = 'participant' or @name  = 'government-departments'" >
						<xsl:attribute name="class"><xsl:value-of select="@name"/></xsl:attribute>
					</xsl:if>
					<xsl:if test="@required = 'required'">
						<xsl:attribute name="required">required</xsl:attribute>
					</xsl:if>
					<xsl:if test="@multiple = 'multiple'">
						<xsl:attribute name="multiple">multiple</xsl:attribute>
					</xsl:if>
					<xsl:attribute name="data-placeholder"><xsl:value-of select="@title" /></xsl:attribute>
					<option value="" selected="selected"></option>
					<xsl:apply-templates select="values/item" mode="form" />
				</select>
		
	</xsl:template>
	
	<xsl:template match="item" mode="form">
		<option value="{@id}">
			<xsl:copy-of select="@selected" />
			<xsl:value-of select="." />
		</option>
	</xsl:template>


	<xsl:template match="field[@type = 'boolean']" mode="form">
		<div>
			<label title="{@tip}">
				<xsl:apply-templates select="@required" mode="form" />
				<span>
					<xsl:value-of select="concat(@title, ':')" />
				</span>
				<input type="hidden" name="{@input_name}" value="0" />
				<input type="checkbox" name="{@input_name}" value="1">
					<xsl:copy-of select="@checked" />
				</input>
			</label>
		</div>
	</xsl:template>


	<xsl:template match="field[@type = 'text' or @type = 'wysiwyg']" mode="form">
		
		<div>
			<label title="{@tip}">
				<xsl:apply-templates select="@required" mode="form" />
				<span>
					<xsl:value-of select="concat(@title, ':')" />
				</span>
				<textarea name="{@input_name}" class="textinputs">
					<xsl:value-of select="." />
				</textarea>
			</label>
		</div>
	</xsl:template>


	<xsl:template match="field[@type = 'file' or @type = 'img_file']" mode="form">
		<input type="file" name="{@input_name}" class="" data-browse="{@title}" data-placeholder="&no-file-selected;" required="required">
			<xsl:if test="@required = 'required'">
				<xsl:attribute name="required">required</xsl:attribute>
			</xsl:if>
		</input>
		
	</xsl:template>
	
	<xsl:template match="@required" mode="form">
		<xsl:attribute name="class">required</xsl:attribute>
	</xsl:template>
</xsl:stylesheet>