<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="result[@module = 'users'][@method = 'restore']">
		<xsl:apply-templates select="document(concat('udata://users/restore/', $param0 ,'/'))/udata" />
	</xsl:template>

	<xsl:template match="udata[@module = 'users'][@method = 'restore'][@status = 'success']">
		<div class="middle">
		
			<div class="pagetitle">
				<xsl:text>&forget-message;</xsl:text>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="udata[@module = 'users'][@method = 'restore'][@status = 'fail']">
		<div class="middle">
		
		 <div class="pagetitle">
			<xsl:text>&activation-error;</xsl:text>
			</div>
		</div>
	</xsl:template>

</xsl:stylesheet>
