<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xlink="http://www.w3.org/TR/xlink">

	<xsl:template match="result[@method = 'registrate']">
		<xsl:apply-templates select="document('udata://users/registrate')" />
	</xsl:template>
	
	<xsl:template match="udata[@method = 'registrate']">

	
	
		<form id="registrate" enctype="multipart/form-data" method="post" action="{$lang-prefix}/users/registrate_do/" >
			
			
			<input type="hidden" name="login" class="textinputs" />
			<input type="hidden" name="email" class="textinputs" />
			<input type="hidden" name="data[new][fname]" class="textinputs" />
			<input type="hidden" name="data[new][kod_dlya_prilozheniya]" id="kod_dlya_prilozheniya" class="textinputs" />
			<input type="hidden" name="password" class="textinputs" value="{document('udata://users/getRandomPassword')/udata}"/>
			<xsl:apply-templates select="document(@xlink:href)" />
			
			<xsl:apply-templates select="document('udata://system/captcha')" />

		
			<div class="row formbox_bottom">
					<div class="grid4">
						<label class="checkf"><input name="poltica" type="checkbox" class="checkbox" checked="checked"/>&consent-to-the-processing-of-data;</label>
						<input type="submit" class="btn" value="&send;"/>
					</div>
					<div class="grid8">
						<div class="formbox_bottom_text">
							<xsl:value-of select="document(concat('upage://', $document-page-id, '.tekst_v_forme'))//value" disable-output-escaping="yes"/>
							
						</div>
					</div>
				</div><!-- #formbox_bottom -->
			
			<xsl:apply-templates select="document('udata://system/listErrorMessages')" />
		</form>
	</xsl:template>

	<xsl:template match="result[@method = 'registrate_done']">		
		<xsl:apply-templates select="document('udata://users/registrate_done')/udata"/>
	</xsl:template>
	
	<xsl:template match="udata[@method = 'registrate_done']">
		<div class="middle">
			<xsl:choose>
				<xsl:when test="result = 'without_activation'">
					<div class="pagetitle">
						<xsl:text>&registration-done;</xsl:text>
					</div>
				</xsl:when>
				<xsl:when test="result = 'error'">
					<div class="pagetitle">
						<xsl:text>&registration-error;</xsl:text>
					</div>
				</xsl:when>
				<xsl:when test="result = 'error_user_exists'">
					<div class="pagetitle">
						<xsl:text>&registration-error-user-exists;</xsl:text>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<div class="pagetitle">
						<xsl:text>&registration-done;</xsl:text>
					</div>
					<p>
						<xsl:text>&registration-activation-note;</xsl:text>
					</p>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	
	
	<xsl:template match="result[@method = 'activate']">
		<xsl:variable name="activation-errors" select="document('udata://users/activate')/udata/error" />
		
		<xsl:choose>
			<xsl:when test="count($activation-errors)">
				<xsl:apply-templates select="$activation-errors" />
			</xsl:when>
			<xsl:otherwise>
				<p>
					<xsl:text>&account-activated;</xsl:text>
				</p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	
	<!-- User settings -->
	<xsl:template match="result[@method = 'settings']">
		<xsl:apply-templates select="document('udata://users/settings')" />
	</xsl:template>
	
	<xsl:template match="udata[@method = 'settings']">
		<xsl:variable name="csrf_token" select="/result/@csrf" />

		<form enctype="multipart/form-data" method="post" action="{$lang-prefix}/users/settings_do/" id="con_tab_profile">
			<input type="hidden" name="csrf" value="{$csrf_token}" />
			<div>
				<label>
					<span>
						<xsl:text>&login;:</xsl:text>
					</span>
					<input type="text" name="login" class="textinputs" disabled="disabled" value="{$user-info//property[@name = 'login']/value}" />
				</label>
			</div>
			<div>
				<label>
					<span>
						<xsl:text>&current-password;:</xsl:text>
					</span>
					<input type="password" name="current-password" class="textinputs" />
				</label>
			</div>
			<div>
				<label>
					<span>
						<xsl:text>&password;:</xsl:text>
					</span>
					<input type="password" name="password" class="textinputs" />
				</label>
			</div>
			<div>
				<label>
					<span>
						<xsl:text>&password-confirm;:</xsl:text>
					</span>
					<input type="password" name="password_confirm" class="textinputs" />
				</label>
			</div>
			<div>
				<label>
					<span>
						<xsl:text>&e-mail;:</xsl:text>
					</span>
					<input type="text" name="email" class="textinputs" value="{$user-info//property[@name = 'e-mail']/value}" />
				</label>
			</div>
			
			<xsl:apply-templates select="document(concat('udata://data/getEditForm/', $user-id))" />

			<div>
				<input type="submit" class="button" value="&save-changes;" />
			</div>
		</form>
	</xsl:template>
</xsl:stylesheet>
