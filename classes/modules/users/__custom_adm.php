<?php
	abstract class __custom_adm_users {
		public function users_activate_mail(iUmiEventPoint $eventPoint) {
			

			  if ($oEventPoint->getMode() === "after") {
				// берем необходимые параметры
				$user_id = $oEventPoint->getParam("user_id");

				// получаем объект "Пользователь"
				$oObject = umiHierarchy::getInstance()->getElement($user_id);
				if ($oObject instanceof umiHierarchyElement) {
				 
				 // формируем письмо
				 $oMyMail = new umiMail();
				 $oMyMail->setFrom("ivonina.alexandra@gmail.com", "mail");
				 $oMyMail->setSubject("Активация аккаунта");
				// $oMyMail->setContent("Ваш аккаунт на сайте <a href='http://iaa2017stpetersburg.com'>iaa2017stpetersburg.com</a> успешно активирован!<br/> Ваш логин: ".$oObject->getValue('login')."<br/> ");
				 $oMyMail->setContent("Ваш аккаунт успешно активирован!");
				 $oMyMail->addRecipient("ai@dpromo.su", "Admin");

				 // отправляем письмо
				 $oMyMail->commit(); 
				 $oMyMail->send();
				}
				return true;
			   }
		}
	};
?>
