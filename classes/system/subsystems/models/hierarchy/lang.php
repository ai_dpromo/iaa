<?php
	/**
	 * Класс языка
	 */
	class lang extends umiEntinty implements iLang {
		/**
		 * @var string $prefix префикс языка
		 */
		private $prefix;
		/**
		 * @var bool $isDefaultFlag является ли язык основным
		 */
		private $isDefaultFlag;
		/**
		 * @var string $title название языка
		 */
		private $title;
		/**
		 * @var string $store_type тип сохраняемой сущности для кеширования
		 */
		protected $store_type = "lang";

		/**
		 * @inheritdoc
		 * @return string
		 */
		public function getTitle() {
			return $this->title;
		}

		/**
		 * @inheritdoc
		 * @throws wrongParamException
		 */
		public function setTitle($title) {
			if (!is_string($title) || empty($title)) {
				throw new wrongParamException("Wrong language title given");
			}

			if ($this->getTitle() != $title) {
				$this->title = $title;
				$this->setIsUpdated();
			}
		}

		/**
		 * @inheritdoc
		 * @return string
		 */
		public function getPrefix() {
			return $this->prefix;
		}

		/**
		 * @inheritdoc
		 * @throws wrongParamException
		 */
		public function setPrefix($prefix) {
			if (!is_string($prefix) || empty($prefix)) {
				throw new wrongParamException("Wrong language prefix given");
			}

			$prefix = $this->filterPrefix($prefix);

			if ($this->getPrefix() != $prefix) {
				$this->prefix = $prefix;
				$this->setIsUpdated();
			}
		}

		/**
		 * @inheritdoc
		 * @return bool
		 */
		public function getIsDefault() {
			return $this->isDefaultFlag;
		}

		/**
		 * @inheritdoc
		 */
		public function setIsDefault($flag) {
			$flag = (bool) $flag;

			if ($this->getIsDefault() != $flag) {
				$this->isDefaultFlag = $flag;
				$this->setIsUpdated();
			}
		}

		/**
		 * @inheritdoc
		 * @return bool
		 * @throws Exception
		 */
		protected function save() {
			if (!$this->getIsUpdated()) {
				return true;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$title = $connection->escape($this->getTitle());
			$prefix = $connection->escape($this->getPrefix());
			$isDefaultFlag = (int) $this->getIsDefault();
			$escapedId = (int) $this->getId();

			$sql = <<<SQL
UPDATE `cms3_langs`
	SET `prefix` = '$prefix', `is_default` = $isDefaultFlag, `title` = '$title'
		WHERE `id` = $escapedId
SQL;

			$connection->query($sql);
			return true;
		}

		/**
		 * @inheritdoc
		 * @return bool
		 */
		protected function loadInfo($row = false) {
			if ($row === false) {
				$connection = ConnectionPool::getInstance()->getConnection();
				$escapedId = (int) $this->getId();
				$sql = <<<SQL
SELECT `id`, `prefix`, `is_default`, `title` FROM `cms3_langs` WHERE `id` = $escapedId
SQL;
				$result = $connection->queryResult($sql);
				$result->setFetchType(IQueryResult::FETCH_ROW);
				$row = $result->fetch();
			}

			if (list($id, $prefix, $isDefaultFlag, $title) = $row) {
				$this->prefix = (string) $prefix;
				$this->title = (string) $title;
				$this->isDefaultFlag = (bool) $isDefaultFlag;
				return true;
			}

			return false;
		}

		/**
		 * Удаляет неподдерживаемые символы из префикса языка
		 * @param string $prefix
		 * @return string
		 */
		private function filterPrefix($prefix) {
			return preg_replace("/[^A-z0-9_\-]+/", "", $prefix);
		}
	}
