<?php
	/**
	 * Предоставляет доступ к страницам сайта (класс umiHierarchyElement) и методы для управления структурой сайта.
	 * Синглтон, экземпляр коллекции можно получить через статический метод getInstance().
	 */
	class umiHierarchy extends singleton implements iSingleton, iUmiHierarchy {
		/**
		 * @var bool не обновлять карту сайта при сохранении страниц
		 * @see umiHierarchyElement::save()
		 */
		public static $ignoreSiteMap = false;

		/** экземпляры классов системы для внутреннего использования */
		public $cacheFrontend;
		public $umiObjectsCollection;
		public $domainsCollection;
		public $langsCollection;
		public $mainConfiguration;
		public $umiHierarchyTypesCollection;
		public $umiObjectTypesCollection;
		public $templatesCollection;
		public $regedit;
		public $umiTypesHelper;

		/** @var array(id => umiHierarchyElement, ...) загруженные страницы */
		private $loadedElements = [];

		/** @var int[] идентификаторы измененных страниц */
		private $updatedElements = [];

		/** @var int[] идентификаторы запрошенных страниц */
		private $collectedElementIds = [];

		/**
		 * @var bool отключить автокоррекцию адреса страниц
		 * @see $this->getIdByPath()
		 */
		private $isAutoCorrectionDisabled = false;

		/** @var int максимальное значение атрибута "дата последней модификации" среди запрошенных страниц */
		private $elementsLastUpdateTime = 0;

		/**
		 * @var bool генерировать абсолютные url для страниц
		 * @see $this->forceAbsolutePath()
		 */
		private $isAbsolutePathForced = false;

		/**
		 * @var array Список пар [elementId, parentSymlinkId], для которых будет сделана виртуальная копия
		 * @see $this->addElement()
		 * @see $this->__destruct()
		 */
		private $pendingSymlinkPairs = [];

		/**
		 * @var array [pseudoHash => elementPath, ...] кэш с полными адресами страниц
		 * @see $this->getPathById()
		 */
		private $pathCache = [];

		/**
		 * @var array [elementId => elementAltName, ...] кэш с псевдостатическими адресами страниц
		 * @see $this->getPathById()
		 */
		private $altNameCache = [];

		/**
		 * @var array [
		 *   langId => [
		 *     domainId => defaultElementId,
		 *   ],
		 * ]
		 * кэш с идентификаторами страниц по умолчанию для связки langId/domainId
		 * @see $this->getDefaultElementId()
		 */
		private $defaultIdCache = [];

		/**
		 * @var array [childId => parentIds[], ...] кэш с идентификаторами всех предков для дочерних страниц
		 * @see $this->getAllParents()
		 */
		private $parentIdCache = [];

		/**
		 * @var array [pathHash => elementId|false, ...] кэш с идентификаторами страниц, вычисленных по адресу
		 * @see $this->getIdByPath()
		 */
		private $idByPathCache = [];

		/** Конструктор */
		protected function __construct() {
			$this->regedit = regedit::getInstance();
			$this->mainConfiguration = mainConfiguration::getInstance();
			$this->domainsCollection = domainsCollection::getInstance();
			$this->langsCollection = langsCollection::getInstance();
			$this->templatesCollection = templatesCollection::getInstance();
			$this->cacheFrontend = cacheFrontend::getInstance();
			$this->umiObjectsCollection = umiObjectsCollection::getInstance();
			$this->templatesCollection = templatesCollection::getInstance();
			$this->umiHierarchyTypesCollection = umiHierarchyTypesCollection::getInstance();
			$this->umiObjectTypesCollection = umiObjectTypesCollection::getInstance();
			$this->umiTypesHelper = umiTypesHelper::getInstance();

			if ($this->regedit->getVal("//settings/disable_url_autocorrection")) {
				$this->isAutoCorrectionDisabled = true;
			}
		}

		/**
		 * Экземпляр коллекции
		 * @inheritdoc
		 * @return umiHierarchy
		 */
		public static function getInstance($c = null) {
			return parent::getInstance(__CLASS__);
		}

		/**
		 * Существует ли страница $elementId (класс umiHierarchyElement)?
		 * @param int $elementId идентификатор странциы
		 * @return bool
		 */
		public function isExists($elementId) {
			if (!is_numeric($elementId)) {
				return false;
			}

			$elementId = intval($elementId);
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT id FROM cms3_hierarchy WHERE id = '{$elementId}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$count = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$count = (int) array_shift($fetchResult);
			}

			return (bool) $count;
		}

		/**
		 * Загружена ли страница $elementId (класс umiHierarchyElement)?
		 * @param int $elementId идентификатор странциы
		 * @return bool
		 */
		public function isLoaded($elementId) {
			if ($elementId === false) {
				return false;
			}

			if (!is_array($elementId)) {
				return (bool) array_key_exists($elementId, $this->loadedElements);
			}

			$ids = $elementId;
			$isLoaded = true;

			foreach ($ids as $id) {
				if (!array_key_exists($id, $this->loadedElements)) {
					$isLoaded = false;
					break;
				}
			}

			return $isLoaded;
		}

		/**
		 * Страница $elementId
		 * @param int $elementId идентификатор страницы
		 * @param bool $ignorePermissions = false игнорировать права доступа
		 * @param bool $ignoreDeleted = false возможность получить удаленную страницу
		 * @param mixed $row параметры страницы, @see umiEntinty::__construct()
		 * @return umiHierarchyElement|false
		 */
		public function getElement($elementId, $ignorePermissions = false, $ignoreDeleted = false, $row = false) {
			if (!$elementId) {
				return false;
			}

			if (!$ignorePermissions && !$this->isAllowed($elementId)) {
				return false;
			}

			if ($this->isLoaded($elementId)) {
				return $this->loadedElements[$elementId];
			}

			$element = $this->cacheFrontend->load($elementId, "element");

			if (!$element instanceof iUmiHierarchyElement) {
				try {
					$element = new umiHierarchyElement($elementId, $row);
					$this->cacheFrontend->save($element, "element");
				} catch (privateException $e) {
					return false;
				}
			}

			$this->collectedElementIds[] = $elementId;

			if (!is_object($element)) {
				return false;
			}

			if ($element->getIsBroken()) {
				return false;
			}

			if ($element->getIsDeleted() && !$ignoreDeleted) {
				return false;
			}

			$this->pushElementsLastUpdateTime($element->getUpdateTime());
			$this->loadedElements[$elementId] = $element;
			return $element;
		}

		/**
		 * Возвращает список страниц
		 * @param int $limit количество страниц
		 * @param int $offset от какой позиции списка отсчитывать количество
		 * @return iUmiHierarchyElement[]
		 */
		public function getList($limit = 15, $offset = 0) {
			$escapedLimit = (int) $limit;
			$escapedOffset = (int) $offset;
			$connection = ConnectionPool::getInstance()
				->getConnection();
			$sql = <<<SQL
SELECT
	h.id,
	h.rel,
	h.type_id,
	h.lang_id,
	h.domain_id,
	h.tpl_id,
	h.obj_id,
	h.ord,
	h.alt_name,
	h.is_active,
	h.is_visible,
	h.is_deleted,
	h.updatetime,
	h.is_default,
	o.name,
	o.type_id as object_type_id
FROM cms3_hierarchy h, cms3_objects o
WHERE o.id = h.obj_id
ORDER BY h.id
LIMIT $escapedOffset, $escapedLimit;
SQL;
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			$elementList = [];

			if ($result->length() == 0) {
				return $elementList;
			}

			$ignorePermissions = true;
			$ignoreDeleted = true;

			foreach ($result as $row) {
				$id = array_shift($row);
				$element = $this->getElement($id, $ignorePermissions, $ignoreDeleted, $row);

				if ($element instanceof iUmiHierarchyElement) {
					$elementList[] = $element;
				}
			}

			return $elementList;
		}


		/**
		 * Загрузить страницы $elementIds
		 * @param array $elementIds массив идентификаторов страниц
		 * @return umiHierarchyElement[]
		 * @throws Exception
		 */
		public function loadElements($elementIds) {
			if (!is_array($elementIds)) {
				$elementIds = [$elementIds];
			}

			$allowedIds = [];

			foreach ($elementIds as $id) {
				if ($this->isAllowed($id)) {
					$allowedIds[] = $id;
				}
 			}

 			$elementIds = $allowedIds;

			if (count($elementIds) == 0) {
				return [];
			}

			$elementIds = implode(',', $elementIds);
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = <<<SQL
SELECT
  h.id,
  h.rel,
  h.type_id,
  h.lang_id,
  h.domain_id,
  h.tpl_id,
  h.obj_id,
  h.ord,
  h.alt_name,
  h.is_active,
  h.is_visible,
  h.is_deleted,
  h.updatetime,
  h.is_default,
  o.name,
  o.type_id AS object_type_id
FROM cms3_hierarchy h, cms3_objects o
WHERE h.id IN ({$elementIds}) AND o.id = h.obj_id;
SQL;
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			if ($result->length() == 0) {
				return [];
			}

			$elements = [];

			foreach ($result as $row) {
				$elementId = array_shift($row);
				$element = $this->getElement($elementId, true, false, $row);
				if ($element instanceof umiHierarchyElement) {
					$elements[] = $element;
				}
			}

			return $elements;
		}

		/**
		 * Помещает страницу и всех ее детей в корзину.
		 * Виртуальные копии удаляются из системы сразу, минуя корзину.
		 * @param int $elementId идентификатор страницы
		 * @return bool
		 */
		public function delElement($elementId) {
			$elementId = (int) $elementId;
			$permissions = permissionsCollection::getInstance();
			$auth = UmiCms\Service::Auth();

			if (!$permissions->isAllowedObject($auth->getUserId(), $elementId)) {
				return false;
			}

			$element = $this->getElement($elementId);

			if (!$element instanceof iUmiHierarchyElement) {
				return false;
			}

			if ($element->hasVirtualCopy()) {
				return $this->killElement($element->getId());
			}

			$this->addUpdatedElementId($elementId);
			$this->forceCacheCleanup();

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT id FROM cms3_hierarchy FORCE INDEX(rel) WHERE rel = '{$elementId}'";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			foreach ($result as $row) {
				list($childId) = $row;
				$this->delElement($childId);
				$this->cacheFrontend->del($childId, "element");
			}

			$event = new umiEventPoint("hierarchyDeleteElement");
			$event->setParam("element_id", $elementId);
			$event->setParam('user_id', $auth->getUserId());
			$event->setMode("after");
			$event->call();

			$element->setIsDeleted(true);
			$element->commit();

			unset($this->loadedElements[$elementId]);
			$this->cacheFrontend->del($elementId, "element");

			return true;
		}

		/**
		 * {@inheritdoc}
		 */
		public function getOriginalPage($objectId) {
			if (!is_numeric($objectId)) {
				return false;
			}

			$normalisedObjectId = (int) $objectId;

			$query = <<<SQL
SELECT `id` FROM `cms3_hierarchy` WHERE `obj_id` = $normalisedObjectId LIMIT 0, 1
SQL;
			$queryResult = ConnectionPool::getInstance()
				->getConnection()
				->queryResult($query)
				->setFetchType(IQueryResult::FETCH_ASSOC);

			if ($queryResult->length() === 0) {
				return false;
			}

			$queryResultRow = $queryResult->fetch();
			$originalPageId = array_shift($queryResultRow);

			return $this->getElement($originalPageId);
		}

		/**
		 * Создать виртуальную копию (подобие symlink в файловых системах) страницы $elementId
		 * @param int $elementId идентификатор копируемой страницы
		 * @param int $parentId идентификатор родителя созданной копии
		 * @param bool $copyChildren = false рекурсивно копировать дочерние страницы
		 * @return int|false идентификатор виртуальной копии
		 */
		public function copyElement($elementId, $parentId, $copyChildren = false) {
			$elementId = (int) $elementId;
			$this->cacheFrontend->flush();

			$this->collectedElementIds[] = $parentId;
			$this->collectedElementIds[] = $elementId;

			$this->forceCacheCleanup();

			if (!$this->isExists($elementId) || (!$this->isExists($parentId) && $parentId !== 0)) {
				return false;
			}

			$parentId = (int) $parentId;
			$timestamp = self::getTimeStamp();

			if ($newElement = $this->getElement($elementId)) {
				$this->collectedElementIds[] = $newElement->getParentId();
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$newPageOrd = (int) $this->getNewPageOrd($parentId);

			$sql = <<<SQL
INSERT INTO cms3_hierarchy
(rel, type_id, lang_id, domain_id, tpl_id, obj_id, alt_name, is_active, is_visible, is_deleted, updatetime, ord)
	SELECT '{$parentId}', type_id, lang_id, domain_id, tpl_id, obj_id,
	alt_name, is_active, is_visible, is_deleted, '{$timestamp}', $newPageOrd
	FROM cms3_hierarchy WHERE id = '{$elementId}' LIMIT 1
SQL;
			$connection->query($sql);
			$newElementId = $connection->insertId();

			$sql = <<<SQL
INSERT INTO cms3_permissions
(level, owner_id, rel_id)
	SELECT level, owner_id, '{$newElementId}' FROM cms3_permissions WHERE rel_id = '{$elementId}'

SQL;
			$connection->query($sql);
			$newElement = $this->getElement($newElementId, true);

			if (!$newElement instanceof iUmiHierarchyElement) {
				return false;
			}

			$newElement->setAltName($newElement->getAltName());
			$newElement->commit();
			$this->buildRelationNewNodes($newElementId);

			if ($copyChildren) {
				$domainId = $newElement->getDomainId();
				$children = $this->getChildrenTree($elementId, true, true, 0, false, $domainId);

				foreach ($children as $childId => $_dummy) {
					$this->getElement($childId, true, true);
					$this->copyElement($childId, $newElementId, true);
				}
			}

			$this->collectedElementIds[] = $newElementId;
			return $newElementId;
		}

		/**
		 * Создать копию страницы $elementId вместе со всеми данными
		 * @param int $elementId идентификатор копируемой страницы
		 * @param int $parentId идентификатор родителя созданной копии
		 * @param bool $copySubPages = false рекурсивно копировать дочерние страницы
		 * @return int|false идентификатор копии
		 */
		public function cloneElement($elementId, $parentId, $copySubPages = false) {
			$this->cacheFrontend->flush();
			$this->collectedElementIds[] = $parentId;
			$this->collectedElementIds[] = $elementId;
			$this->forceCacheCleanup();

			if (!$this->isExists($elementId) || (!$this->isExists($parentId) && $parentId != 0)) {
				return false;
			}

			$newElement = $this->getElement($elementId);

			if (!$newElement instanceof iUmiHierarchyElement) {
				return false;
			}

			$this->collectedElementIds[] = $newElement->getParentId();
			$connection = ConnectionPool::getInstance()->getConnection();

			$timestamp = (int) self::getTimeStamp();
			$objectId = $newElement->getObjectId();

			$sql = <<<SQL
INSERT INTO cms3_objects
(name, is_locked, type_id, owner_id, ord, updatetime)
	SELECT name, is_locked, type_id, owner_id, ord, $timestamp
		FROM cms3_objects
			WHERE id = '{$objectId}'
SQL;
			$connection->query($sql);
			$newObjectId = $connection->insertId();

			$objectTypeId = $this->umiObjectsCollection->getObject($objectId)->getTypeId();
			$contentTable = umiBranch::getBranchedTableByTypeId($objectTypeId);

			$sql = <<<SQL
INSERT INTO {$contentTable}
(field_id, int_val, varchar_val, text_val, rel_val, float_val, tree_val, obj_id)
	SELECT field_id, int_val, varchar_val, text_val, rel_val, float_val, tree_val, '{$newObjectId}'
		FROM {$contentTable}
			WHERE obj_id = '{$objectId}'
SQL;
			$connection->query($sql);

			$sql = <<<SQL
INSERT INTO cms3_object_images
(`obj_id`, `field_id`, `src`, `alt`, `ord`)
	SELECT '{$newObjectId}', `field_id`, `src`, `alt`, `ord`
		FROM cms3_object_images
			WHERE obj_id = '{$objectId}'
SQL;
			$connection->query($sql);

			$sql = <<<SQL
INSERT INTO cms3_object_content_cnt
(`obj_id`, `field_id`, `cnt`)
	SELECT '{$newObjectId}', `field_id`, `cnt`
		FROM cms3_object_content_cnt
			WHERE obj_id = '{$objectId}'
SQL;
			$connection->query($sql);


			$newPageOrd = (int) $this->getNewPageOrd($parentId);

			$sql = <<<SQL
INSERT INTO cms3_hierarchy
(rel, type_id, lang_id, domain_id, tpl_id, obj_id, alt_name, is_active, is_visible, is_deleted, updatetime, ord)
	SELECT '{$parentId}', type_id, lang_id, domain_id, tpl_id, '{$newObjectId}', alt_name, is_active,
		is_visible, is_deleted, '{$timestamp}', $newPageOrd
			FROM cms3_hierarchy WHERE id = '{$elementId}' LIMIT 1
SQL;
			$connection->query($sql);
			$newElementId = $connection->insertId();

			$sql = <<<SQL
INSERT INTO cms3_permissions
(level, owner_id, rel_id)
	SELECT level, owner_id, '{$newElementId}' FROM cms3_permissions WHERE rel_id = '{$elementId}'
SQL;
			$connection->query($sql);
			$newElement = $this->getElement($newElementId, true);

			if (!$newElement instanceof iUmiHierarchyElement) {
				return false;
			}

			$newElement->setAltName($newElement->getAltName());
			$newElement->commit();

			$this->buildRelationNewNodes($newElementId);

			if ($copySubPages) {
				$domainId = $newElement->getDomainId();
				$children = $this->getChildrenTree($elementId, true, true, 0, false, $domainId);

				foreach ($children as $childId => $_dummy) {
					$this->cloneElement($childId, $newElementId, true);
				}
			}

			$this->collectedElementIds[] = $newElementId;
			return $newElementId;
		}

		/**
		 * Удаленные в корзину страницы
		 * @param int &$total общее число страниц в корзине
		 * @param int $limit число запрошенных страниц
		 * @param int $page страница результатов
		 * @param string $searchName строка поиска по имени элементов
		 * @return int[] идентификаторы страниц
		 */
		public function getDeletedList(&$total = 0, $limit = 20, $page = 0, $searchName = '') {
			$limit = (int)$limit;
			$offset = (int)$page * $limit;
			$searchCondition = $searchName ? "AND objects.name LIKE '%{$searchName}%'" : '';

			$sql = <<<SQL
				SELECT 
					SQL_CALC_FOUND_ROWS hierarchy.id
				FROM
					cms3_hierarchy hierarchy
				LEFT JOIN cms3_objects as objects ON hierarchy.obj_id = objects.id
				WHERE
					hierarchy.is_deleted = '1' {$searchCondition}
				AND
					hierarchy.rel NOT IN (
						SELECT 
							id
						FROM
							cms3_hierarchy
						WHERE 
							is_deleted = '1'
						)
				ORDER BY
					hierarchy.updatetime DESC
				LIMIT 
					{$offset}, {$limit}
SQL;
			$connection = ConnectionPool::getInstance()->getConnection();
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			$deletedPageIds = [];

			foreach ($result as $row) {
				$deletedPageIds[] = array_shift($row);
			}

			$totalSql = <<<TOTALSQL
				SELECT FOUND_ROWS()
TOTALSQL;

			$totalResult = $connection->queryResult($totalSql);
			$totalResult->setFetchType(IQueryResult::FETCH_ROW);
			$totalRow = 0;

			if ($totalResult->length() > 0) {
				$fetchResult = $totalResult->fetch();
				$totalRow = (int) array_shift($fetchResult);
			}

			if ($totalRow) {
				$total = is_array($totalRow) ? (int)$totalRow[0] : (int)$totalRow;
			}


			return $deletedPageIds;
		}

		/**
		 * Восстановить страницу из корзины
		 * @param int $elementId идентификатор страницы
		 * @return bool
		 */
		public function restoreElement($elementId) {
			$elementId = (int) $elementId;
			$element = $this->getElement($elementId, false, true);

			if (!$element instanceof iUmiHierarchyElement) {
				return false;
			}

			$event = new umiEventPoint("systemRestoreElement");
			$event->addRef("element", $element);
			$event->setParam('user_id', UmiCms\Service::Auth()->getUserId());
			$event->setMode("before");
			$event->call();

			$element->setIsDeleted(false);
			$element->setAltName($element->getAltName());
			$element->commit();

			$event->setMode("after");
			$event->call();

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT id FROM cms3_hierarchy WHERE rel = '{$elementId}'";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			foreach ($result as $row) {
				list($childId) = $row;
				$this->getElement($childId, true, true);
				$this->restoreElement($childId);
			}

			return true;
		}

		/**
		 * Удаляет заданную страницу и всех ее детей
		 * @param int $parentId идентификатор страницы
		 * @return bool
		 */
		public function killElement($parentId) {
			$ignorePermissions = true;
			$ignoreDeletedStatus = true;
			$parent = $this->getElement($parentId, $ignorePermissions, $ignoreDeletedStatus);

			if (!$parent instanceof iUmiHierarchyElement) {
				return false;
			}

			$childrenIdList = $this->getChildrenList($parentId);

			array_map(function($childId) {
				$this->killElement($childId);
			}, $childrenIdList);

			$this->deleteElement($parent);
			return true;
		}

		/**
		 * Удаляет заданную страницу и всех ее детей из корзины
		 * @param int $parentId идентификатор страницы
		 * @param int $removedSoFar не передавать, используется для внутренних целей
		 * @return bool
		 */
		public function removeDeletedElement($parentId, &$removedSoFar = 0) {
			$ignorePermissions = true;
			$ignoreDeletedStatus = true;
			$parent = $this->getElement($parentId, $ignorePermissions, $ignoreDeletedStatus);

			if (!$parent instanceof iUmiHierarchyElement || !$parent->getIsDeleted()) {
				return false;
			}

			$childrenIdList = $this->getChildrenList($parentId);

			foreach ($childrenIdList as $childId) {
				$child = $this->getElement($childId, $ignorePermissions, $ignoreDeletedStatus);

				if (!$child instanceof iUmiHierarchyElement) {
					continue;
				}

				if (!$child->getIsDeleted()) {
					$child->setIsDeleted(true);
					$child->commit();
				}

				$this->removeDeletedElement($childId, $removedSoFar);
			}

			$this->deleteElement($parent);
			$removedSoFar += 1;

			return true;
		}

		/**
		 * Очистить корзину
		 * @return bool
		 */
		public function removeDeletedAll() {
			$connection = ConnectionPool::getInstance()->getConnection();
			$connection->query("START TRANSACTION /* umiHierarchy::removeDeletedAll() */");

			$sql = "SELECT id FROM cms3_hierarchy WHERE is_deleted = '1'";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			foreach ($result as $row) {
				list($elementId) = $row;
				$this->removeDeletedElement($elementId);
			}

			$connection->query("COMMIT");
			return true;
		}

		/**
		 * Очистить корзину с ограничением на $limit страниц
		 * @param mixed $limit число удаляемых страниц
		 * @return int число удаленных страниц
		 */
		public function removeDeletedWithLimit($limit = false) {
			if (!$limit) {
				$limit = 100;
			}

			$limit = (int) $limit;
			$connection = ConnectionPool::getInstance()->getConnection();
			$connection->query("START TRANSACTION /* umiHierarchy::removeDeletedWithLimit() */");

			$sql = "SELECT id FROM cms3_hierarchy WHERE is_deleted = '1' LIMIT {$limit}";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			$removedSoFar = 0;

			foreach ($result as $row) {
				list($elementId) = $row;
				$this->removeDeletedElement($elementId, $removedSoFar);
			}

			$connection->query("COMMIT");
			return $removedSoFar;
		}

		/**
		 * Идентификатор родительской страницы для $elementId
		 * @param int $elementId идентификатор дочерней страницы
		 * @return mixed
		 */
		public function getParent($elementId) {
			$elementId = (int) $elementId;

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT rel FROM cms3_hierarchy WHERE id = '{$elementId}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			if ($result->length() == 0) {
				return false;
			}

			$fetchResult = $result->fetch();
			$parentId = (int) array_shift($fetchResult);
			$this->collectedElementIds[] = $parentId;
			return (int) $parentId;
		}

		/**
		 * Идентификаторы всех родительских страниц для $elementId
		 * @param int $elementId идентификатор дочерней страницы
		 * @param bool $includeSelf = false включить в результат саму $elementId
		 * @param bool $ignoreCache = false не использовать микрокеширование
		 * @return int[]
		 */
		public function getAllParents($elementId, $includeSelf = false, $ignoreCache = false) {
			$cacheData = $this->cacheFrontend->loadSql('hierarchy_parents');
			if (is_array($cacheData) && sizeof($cacheData)) {
				$this->parentIdCache = $cacheData;
			}

			$elementId = (int) $elementId;
			$parents = [];

			if (!$ignoreCache && isset($this->parentIdCache[$elementId])) {
				$parents = $this->parentIdCache[$elementId];
			} else {
				$connection = ConnectionPool::getInstance()->getConnection();
				$sql = "SELECT rel_id FROM cms3_hierarchy_relations WHERE child_id = '{$elementId}' ORDER BY id";
				$result = $connection->queryResult($sql);
				$result->setFetchType(IQueryResult::FETCH_ROW);

				foreach ($result as $row) {
					list($parentId) = $row;
					$parents[] = (int) $parentId;
				}

				$this->parentIdCache[$elementId] = $parents;
				$this->cacheFrontend->saveSql('hierarchy_parents', $this->parentIdCache, 120);
			}

			if ($includeSelf) {
				$parents[] = (int) $elementId;
			}

			return $parents;
		}

		/**
		 * Список идентификаторов страниц, дочерних заданной на всю глубину вложенности, в виде дерева
		 * @param int $rootPageId идентификатор родительской страницы
		 * @param bool $allowInactive включить в результат неактивные дочерние страницы
		 * @param bool $allowInvisible включить в результат невидимые в меню дочерние страницы
		 * @param int $depth уровень вложенности, на котором искать дочерние страницы
		 * @param int|bool $hierarchyTypeId идентификатор иерархического типа данных,
		 * к которому должны принадлежать дочерние страницы
		 * @param int|bool $domainId включить в результат только страницы из текущего домена (работает если ищем от корня)
		 * @param int|bool $languageId включить в результат только страницы из текущего языка (работает если ищем от корня)
		 * @return array [
		 *          page_id =>  [
		 *                  page_id => [
		 *                          page_id => []
		 *                         ],
		 *                  page_id => []
		 *                ],
		 *          page_id => []
		 *         ]
		 */
		public function getChildrenTree($rootPageId, $allowInactive = true, $allowInvisible = true, $depth = 0,
				$hierarchyTypeId = false, $domainId = false, $languageId = false) {

			$disallowPermissions = permissionsCollection::getInstance()
				->isSv();

			$selectExpression = 'relations.`child_id`, relations.`level`, hierarchy.`ord`, hierarchy.`rel`';
			$whereExpression = $this->getChildrenWhereCondition(
					$rootPageId,
					$hierarchyTypeId,
					$allowInactive,
					$allowInvisible,
					$domainId,
					$languageId
			);

			$maxDepth = (int) $this->getMaxDepth($rootPageId, $depth);

			if ($maxDepth > 0 && $depth > 0) {
				$whereExpression .= " AND relations.`level` <= $maxDepth ";
			}

			$result = $this->runChildrenQuery($selectExpression, $whereExpression, $disallowPermissions, true);

			if ($result->length() == 0) {
				return [];
			}

			$rows = [];
			foreach ($result as $row) {
				$rows[$row['child_id']] = $row;
			}

			$childrenTree = [];
			$tempContainer = [];

			foreach ($rows as $row) {
				$pageId = $row['child_id'];
				$parentIds = $row['rel'];
				$tempContainer[$pageId] = [];

				if ($parentIds == $rootPageId) {
					$childrenTree[$pageId] = &$tempContainer[$pageId];
				}
				if (isset($tempContainer[$parentIds])) {
					$tempContainer[$parentIds][$pageId] = &$tempContainer[$pageId];
				}
			}

			return $childrenTree;
		}

		/**
		 * Абсолютный максимальный уровень вложенности
		 * @param int $rootPageId идентификатор корневой страницы
		 * @param int $maxDepth относительный максимальный уровень вложенности
		 * @return int
		 * @throws publicAdminException
		 */
		public function getMaxDepth($rootPageId, $maxDepth) {
			if (!is_numeric($rootPageId)) {
				throw new publicAdminException('Incorrect page id given');
			}

			$maxDepth = (int) $maxDepth;
			$maxDepth = ($maxDepth === 0) ? 1 : $maxDepth;

			$rootPageId = (int) $rootPageId;

			if ($rootPageId == 0) {
				return $maxDepth;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = <<<SQL
SELECT `level` FROM `cms3_hierarchy_relations` WHERE `child_id` = $rootPageId;
SQL;
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ARRAY);

			if ($result->length() == 0) {
				$level = 0;
			} else {
				$row = $result->fetch();
				$level = (isset($row['level'])) ? (int) $row['level'] : 0;
			}

			return $level + $maxDepth;
		}

		/**
		 * Плоский список идентификаторов страниц, дочерних заданной на всю глубину вложенности
		 * @param int $rootPageId идентификатор родительской страницы
		 * @param bool $allowInactive включить в результат неактивные дочерние страницы
		 * @param bool $allowInvisible включить в результат невидимые в меню дочерние страницы
		 * @param int|bool $hierarchyTypeId идентификатор иерархического типа данных,
		 * к которому должны принадлежать дочерние страницы
		 * @param int|bool $domainId включить в результат только страницы из текущего домена (работает если ищем от корня)
		 * @param bool $includeSelf включить в результат идентификатор родительской страницы
		 * @param int|bool $languageId включить в результат только страницы из текущего языка (работает если ищем от корня)
		 * @return array [
		 *          0 => page_id,
		 *          1 => page_id,
		 *          ...
		 *          n => page_id
		 *         ]
		 */
		public function getChildrenList($rootPageId, $allowInactive = true, $allowInvisible = true,
				$hierarchyTypeId = false, $domainId = false, $includeSelf = false, $languageId = false) {

			$disallowPermissions = permissionsCollection::getInstance()
				->isSv();

			$selectExpression = 'relations.`child_id`, relations.`level`, hierarchy.`ord`';
			$whereExpression = $this->getChildrenWhereCondition(
					$rootPageId,
					$hierarchyTypeId,
					$allowInactive,
					$allowInvisible,
					$domainId,
					$languageId
			);

			$result = $this->runChildrenQuery($selectExpression, $whereExpression, $disallowPermissions, true);

			if ($result->length() == 0) {
				return [];
			}

			$rows = [];

			foreach ($result as $row) {
				$rows[$row['child_id']] = $row;
			}

			$pageIds = [];

			foreach ($rows as $row) {
				$pageIds[] = $row['child_id'];
			}

			if ($includeSelf) {
				array_unshift($pageIds, $rootPageId);
			}

			return $pageIds;
		}

		/**
		 * Идентификатор текущего языка
		 * @return int
		 * @throws publicAdminException
		 */
		public function getCurrentLanguageId() {
			$currentLanguage = cmsController::getInstance()->getCurrentLang();
			if (!$currentLanguage instanceof lang) {
				throw new publicAdminException('Cannot detect current language');
			}
			return $currentLanguage->getId();
		}

		/**
		 * Идентификатор текущего домена
		 * @return int
		 * @throws publicAdminException
		 */
		public function getCurrentDomainId() {
			$currentDomain = cmsController::getInstance()->getCurrentDomain();
			if (!$currentDomain instanceof domain) {
				throw new publicAdminException('Cannot detect current domain');
			}
			return $currentDomain->getId();
		}

		/**
		 * Отсортированный по иерархиии массив с данными о страницах
		 * @param array $pageIds [
		 *              # => [
		 *                'level' => int уровень вложенности страницы,
		 *                'ord'  => int индекс сортировки страницы в рамках ее уровня вложенности
		 *              ]
		 *             ]
		 * @return array
		 */
		public function sortByHierarchy(array $pageIds) {
			usort($pageIds, function (array $first, array $second) {
				$firstItemLevel = $first['level'];
				$firstItemOrd = $first['ord'];
				$secondItemLevel = $second['level'];
				$secondItemOrd = $second['ord'];

				switch (true) {
					case ($firstItemLevel == $secondItemLevel) && ($firstItemOrd == $secondItemOrd): {
						return 0;
					}
					case ($firstItemLevel == $secondItemLevel) && ($firstItemOrd > $secondItemOrd): {
						return 1;
					}
					case ($firstItemLevel > $secondItemLevel): {
						return 1;
					}
					default: {
						return -1;
					}
				}
			});

			return $pageIds;
		}

		/**
		 * Количество страниц, дочерних заданной на всю глубину вложенности
		 * @param int $rootPageId идентификатор родительской страницы
		 * @param bool $allowInactive включить в результат неактивные дочерние страницы
		 * @param bool $allowInvisible включить в результат невидимые в меню дочерние страницы
		 * @param int $depth уровень вложенности, на котором искать дочерние страницы
		 * @param int|bool $hierarchyTypeId идентификатор иерархического типа данных,
		 * к которому должны принадлежать дочерние страницы
		 * @param int|bool $domainId включить в результат только страницы из текущего домена (работает если ищем от корня)
		 * @param int|bool $languageId включить в результат только страницы из текущего языка (работает если ищем от корня)
		 * @param bool $allowPermissions учитывать прав на просмотр дочерних страниц для текущего пользователя
		 * @return bool|int
		 */
		public function getChildrenCount($rootPageId, $allowInactive = true, $allowInvisible = true, $depth = 0,
				$hierarchyTypeId = false, $domainId = false, $languageId = false, $allowPermissions = false) {

			$selectExpression = 'count(relations.`child_id`) as count';
			$whereExpression = $this->getChildrenWhereCondition(
					$rootPageId,
					$hierarchyTypeId,
					$allowInactive,
					$allowInvisible,
					$domainId,
					$languageId
			);

			$maxDepth = (int) $this->getMaxDepth($rootPageId, $depth);

			if ($maxDepth > 0 && $depth > 0) {
				$whereExpression .= " AND relations.`level` <= $maxDepth ";
			}

			$result = $this->runChildrenQuery($selectExpression, $whereExpression, !$allowPermissions);

			if ($result->length() == 0) {
				return false;
			}

			$row = $result->fetch();
			return (isset($row['count'])) ? (int) $row['count'] : 0;
		}

		/**
		 * Переключить режим генерации адресов между относительным и абсолютным
		 * (в абсолютном режиме включается домен, даже если он совпадает с текущим доменом)
		 * @param bool $isForced = true (true - абсолютный режим, false - обычный режим)
		 * @return bool предыдущее значение
		 */
		public function forceAbsolutePath($isForced = true) {
			$oldForced = $this->isAbsolutePathForced;
			$this->isAbsolutePathForced = (bool) $isForced;
			return $oldForced;
		}

		/**
		 * Адрес страницы по ее идентификатору
		 * @param int $elementId = false идентификатор страницы
		 * @param bool $ignoreLang = false игнорировать языковой префикс к адресу страницы
		 * @param bool $ignoreIsDefaultStatus = false игнорировать статус страницы "по умолчанию" и сформировать для не полный путь
		 * @param bool $ignoreCache = false игнорировать кеш
		 * @param bool $ignoreUrlSuffix = false игнорировать суффикс ссылок
		 * @return string
		 */
		public function getPathById($elementId, $ignoreLang = false, $ignoreIsDefaultStatus = false,
				$ignoreCache = false, $ignoreUrlSuffix = false) {
			$elementId = (int) $elementId;

			$cachePath = $elementId . '#' . intval($ignoreLang) . intval($ignoreIsDefaultStatus) .
					intval($ignoreUrlSuffix) . '#' . intval($this->isAbsolutePathForced);

			if (!$ignoreCache && isset($this->pathCache[$cachePath])) {
				return $this->pathCache[$cachePath];
			}

			$cmsController = cmsController::getInstance();
			$urlPrefix = $cmsController->getUrlPrefix();
			$element = $this->getElement($elementId, true);

			if (!$element) {
				return $this->pathCache[$cachePath] = "";
			}

			$currentDomain = $cmsController->getCurrentDomain();
			$elementDomainId = $element->getDomainId();

			if (!$this->isAbsolutePathForced && $currentDomain->getId() == $elementDomainId) {
				$domainStr = "";
			} else {
				$domains = $this->domainsCollection;
				$domain = $domains->getDomain($elementDomainId);
				$domainStr = getSelectedServerProtocol($domain) . "://" . $domain->getHost();
			}

			$elementLangId = intval($element->getLangId());
			$elementLang = $this->langsCollection->getLang($elementLangId);
			$isLangDefault = ($elementLangId === intval($cmsController->getCurrentDomain()->getDefaultLangId()));
			$langStr = (!$elementLang || $isLangDefault || $ignoreLang == true) ? "" : "/" . $elementLang->getPrefix();

			if ($element->getIsDefault() && !$ignoreIsDefaultStatus) {
				return $this->pathCache[$cachePath] = $domainStr . $langStr . $urlPrefix . '/';
			}

			if (!$parents = $this->getAllParents($elementId, false, $ignoreCache)) {
				return $this->pathCache[$cachePath] = false;
			}

			$path = $domainStr . $langStr . $urlPrefix;
			$parents[] = $elementId;
			$toLoad = [];

			foreach ($parents as $parentId) {
				if ($parentId == 0) {
					continue;
				}
				if (!$ignoreCache && isset($this->altNameCache[$parentId])) {
					continue;
				}
				if ($this->isLoaded($parentId) && $parent = $this->getElement($parentId, true)) {
					$this->altNameCache[$parentId] = $parent->getAltName();
				} else {
					$toLoad[] = $parentId;
				}
			}

			if (count($toLoad)) {
				$sql = "SELECT id, alt_name FROM cms3_hierarchy WHERE id IN (" . implode(", ", $toLoad) . ")";
				$altNames = $ignoreCache ? null : $this->cacheFrontend->loadSql($sql);

				if (!is_array($altNames)) {
					$connection = ConnectionPool::getInstance()->getConnection();
					$result = $connection->queryResult($sql);
					$result->setFetchType(IQueryResult::FETCH_ROW);

					$altNames = [];

					foreach ($result as $row) {
						list($id, $altName) = $row;
						$altNames[$id] = $altName;
						$this->altNameCache[$id] = $altName;
					}

					$this->cacheFrontend->saveSql($sql, $altNames, 600);
				} else {
					$this->altNameCache = $this->altNameCache + $altNames;
				}
			}

			$parentCount = sizeof($parents);

			for ($i = 0; $i < $parentCount; $i++) {
				if (!$parents[$i]) {
					continue;
				}

				if (isset($this->altNameCache[$parents[$i]])) {
					$path .= "/" . $this->altNameCache[$parents[$i]];
				}
			}

			if ($this->mainConfiguration->get('seo', 'url-suffix.add') && !$ignoreUrlSuffix) {
				$path .= $this->mainConfiguration->get('seo', 'url-suffix');
			}

			return $this->pathCache[$cachePath] = $path;
		}

		/**
		 * Идентификатор страницы по ее адресу
		 * @param string $elementPath адрес страницы
		 * @param bool $showDisabled = false искать среди неактивных страниц
		 * @param int &$errorsCount = 0 количество несовпадений при разборе адреса
		 * @param mixed $domainId = false идентификатор домена
		 * @param mixed $langId = false идентификатор языка
		 * @return int|false
		 */
		public function getIdByPath($elementPath, $showDisabled = false, &$errorsCount = 0,
				$domainId = false, $langId = false) {
			$langId = (int) $langId;
			$domainId = (int) $domainId;

			if ($urlSuffix = $this->mainConfiguration->get('seo', 'url-suffix')) {
				$pos = strrpos($elementPath, $urlSuffix);
				if ($pos && ($pos + strlen($urlSuffix) == strlen($elementPath))) {
					$elementPath = substr($elementPath, 0, $pos);
				}
			}

			$elementPath = trim($elementPath, "\/ \n");
			$cmsController = cmsController::getInstance();

			if (empty($langId)) {
				$langId = $cmsController->getCurrentLang()->getId();
			}

			if (empty($domainId)) {
				$domainId = $cmsController->getCurrentDomain()->getId();
			}

			$elementHash = md5($domainId . ":" . $langId . ":" . $elementPath);

			if (isset($this->idByPathCache[$elementHash])) {
				return $this->idByPathCache[$elementHash];
			}

			if ($id = $this->cacheFrontend->loadSql($elementHash . "_path")) {
				return $id;
			}

			if ($elementPath == "") {
				return $this->idByPathCache[$elementHash] = $this->getDefaultElementId($langId, $domainId);
			}

			$paths = explode("/", $elementPath);
			$pathCount = sizeof($paths);
			$id = 0;
			$connection = ConnectionPool::getInstance()->getConnection();

			for ($i = 0; $i < $pathCount; $i++) {
				$altName = $paths[$i];
				$altName = $connection->escape($altName);

				if ($i == 0) {
					if ($elementDomainId = $this->domainsCollection->getDomainId($altName)) {
						$domainId = $elementDomainId;
						continue;
					}
				}

				$activeCondition = ($showDisabled) ? '' : "AND is_active = '1' AND is_deleted = '0'";
				$sql = <<<SQL
SELECT id FROM cms3_hierarchy
WHERE rel = '{$id}' AND alt_name = '{$altName}' {$activeCondition}
AND lang_id = '{$langId}' AND domain_id = '{$domainId}'
SQL;
				$result = $connection->queryResult($sql);

				if (!$result->length()) {
					$sql = <<<SQL
SELECT id, alt_name FROM cms3_hierarchy
WHERE rel = '{$id}' {$activeCondition} AND lang_id = '{$langId}' AND domain_id = '{$domainId}'
SQL;
					$result = $connection->queryResult($sql);
					$result->setFetchType(IQueryResult::FETCH_ROW);

					$max = 0;
					$currentId = 0;

					foreach ($result as $row) {
						list($nextId, $nextAltName) = $row;

						if ($this->isAutoCorrectionDisabled) {
							if ($altName == $nextAltName) {
								$currentId = $nextId;
							}
						} else {
							$similarity = self::compareStrings($altName, $nextAltName);
							if ($similarity > $max) {
								$max = $similarity;
								$currentId = $nextId;
								$errorsCount += 1;
							}
						}
					}

					if ($max > 75) {
						$id = $currentId;
					} else {
						return $this->idByPathCache[$elementHash] = false;
					}
				} else {
					$fetchResult = $result->fetch();
					if (!$id = array_shift($fetchResult)) {
						return $this->idByPathCache[$elementHash] = false;
					}
				}
			}

			$this->cacheFrontend->saveSql($elementHash . "_path", $id, 3600);
			return $this->idByPathCache[$elementHash] = $id;
		}

		/**
		 * Добавить новую страницу
		 * @param int $parentId идентификатор родительской страницы
		 * @param int $hierarchyTypeId идентификатор иерархического типа (класс umiHierarchyType)
		 * @param string $name название старницы
		 * @param string $altName псевдостатический адрес (если не передан, то будет вычислен из $name)
		 * @param bool $typeId идентификатор типа данных (если не передан, то будет вычислен из $hierarchyTypeId)
		 * @param bool|int $domainId идентификатор домена (имеет смысл только если $parentId = 0)
		 * @param bool|int $langId = false идентификатор языковой версии (имеет смысл только если $parentId = 0)
		 * @param bool|int $templateId = false идентификатор шаблона, по которому будет выводится страница
		 * @return int|false идентификатор созданной страницы
		 * @throws coreException
		 */
		public function addElement($parentId, $hierarchyTypeId, $name, $altName, $typeId = false,
				$domainId = false, $langId = false, $templateId = false) {

			$parentId = (int) $parentId;
			$domainId = (int) $domainId;
			$langId = (int) $langId;
			$templateId = (int) $templateId;
			$hierarchyType = null;

			if ($typeId === false) {
				if ($hierarchyType = $this->umiHierarchyTypesCollection->getType($hierarchyTypeId)) {
					$typeId = $this->umiObjectTypesCollection->getTypeIdByHierarchyTypeName(
							$hierarchyType->getName(), $hierarchyType->getExt()
					);

					if (!$typeId) {
						throw new coreException("There is no base object type for hierarchy type #{$hierarchyTypeId}");
					}
				}
			} else {
				$objectType = $this->umiObjectTypesCollection->getType($typeId);

				if (!$objectType) {
					throw new coreException("Wrong object type id given");
				}

				$hierarchyTypeId = $objectType->getHierarchyTypeId();
				$hierarchyType = $this->umiHierarchyTypesCollection->getType($hierarchyTypeId);
			}

			if (!$hierarchyType instanceof umiHierarchyType) {
				throw new coreException("Cannot detect hierarchy type");
			}

			$parent = null;
			$cmsController = cmsController::getInstance();

			if (!$domainId) {
				if ($parentId == 0) {
					$domainId = $cmsController->getCurrentDomain()->getId();
				} else {
					$parent = $this->getElement($parentId, true, true);
					$domainId = $parent->getDomainId();
				}
			}

			if (!$langId) {
				if ($parentId == 0) {
					$langId = $cmsController->getCurrentLang()->getId();
				} else {
					if (!$parent) {
						$parent = $this->getElement($parentId, true, true);
					}
					$langId = $parent->getLangId();
				}
			}

			if (!$templateId) {
				$templateId = $this->templatesCollection->getHierarchyTypeTemplate(
						$hierarchyType->getName(), $hierarchyType->getExt()
				);

				if ($templateId === false) {
					$templateId = $this->getDominantTplId($parentId);

					if (!$templateId) {
						$template = $this->templatesCollection->getDefaultTemplate($domainId, $langId);

						if (!$template instanceof template) {
							throw new coreException("Failed to detect default template");
						}
						$templateId = $template->getId();
					}
				}
			}

			if ($parentId) {
				$this->addUpdatedElementId($parentId);
			} else {
				$this->addUpdatedElementId($this->getDefaultElementId());
			}

			$objectId = $this->umiObjectsCollection->addObject($name, $typeId);

			if (!$objectId) {
				throw new coreException("Failed to create new object for hierarchy element");
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = <<<SQL
INSERT INTO cms3_hierarchy (rel, type_id, domain_id, lang_id, tpl_id, obj_id) VALUES(
	'{$parentId}', '{$hierarchyTypeId}', '{$domainId}', '{$langId}', '{$templateId}', '{$objectId}'
)
SQL;
			$connection->query($sql);

			$elementId = $connection->insertId();

			$element = $this->getElement($elementId, true);
			$element->setAltName($altName);

			$element->setOrd(
				$this->getNewPageOrd($parentId)
			);

			$element->commit();
			$this->loadedElements[$elementId] = $element;
			$this->addUpdatedElementId($parentId);
			$this->addUpdatedElementId($elementId);

			if ($parentId) {
				$parentElement = $this->getElement($parentId);

				if ($parentElement instanceof umiHierarchyElement) {
					$symlinks = $this->getObjectInstances($parentElement->getObject()->getId());

					if (sizeof($symlinks) > 1) {
						foreach ($symlinks as $symlinkId) {
							if ($symlinkId == $parentId) {
								continue;
							}
							$this->pendingSymlinkPairs[] = [$elementId, $symlinkId];
						}
					}
				}
			}

			$this->collectedElementIds[] = $elementId;
			$this->buildRelationNewNodes($elementId);
			return $elementId;
		}

		/**
		 * Возвращает страницу со статусом "по умолчанию" (главная страница)
		 * @param null|int $langId = false идентификатор языковой версии, если не указан, берется текущий язык
		 * @param null|int $domainId = false идентификатор домена, если не указан, берется текущий домен
		 * @return iUmiHierarchyElement|false
		 */
		public function getDefaultElement($langId = null, $domainId = null) {
			$id = $this->getDefaultElementId($langId, $domainId);
			return $this->getElement($id);
		}

		/**
		 * Возвращает идентификатор страницы со статусом "по умолчанию" (главная страница)
		 * @param bool|int $langId = false идентификатор языковой версии, если не указан, берется текущий язык
		 * @param bool|int $domainId = false идентификатор домена, если не указан, берется текущий домен
		 * @return int|false
		 */
		public function getDefaultElementId($langId = false, $domainId = false) {
			$langId = (int) $langId;
			$domainId = (int) $domainId;
			$cmsController = cmsController::getInstance();

			if (empty($this->defaultIdCache)) {
				$cacheData = $this->cacheFrontend->loadData('default_pages');
				if (is_array($cacheData)) {
					$this->defaultIdCache = $cacheData;
				}
			}

			if (!$langId) {
				$langId = $cmsController->getCurrentLang()->getId();
			}

			if (!$domainId) {
				$domainId = $cmsController->getCurrentDomain()->getId();
			}

			if (isset($this->defaultIdCache[$langId][$domainId])) {
				return $this->defaultIdCache[$langId][$domainId];
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = <<<SQL
SELECT id FROM cms3_hierarchy
WHERE is_default = '1' AND is_deleted='0' AND is_active='1' AND lang_id = '{$langId}' AND domain_id = '{$domainId}'
SQL;
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			if ($result->length() == 0) {
				return false;
			}

			$fetchResult = $result->fetch();
			$elementId = (int) array_shift($fetchResult);
			$this->defaultIdCache[$langId][$domainId] = $elementId;
			$this->cacheFrontend->saveData('default_pages', $this->defaultIdCache, 3600);

			return $elementId;
		}

		/**
		 * Включен ли режим генерации абсолютных адресов?
		 * @return bool
		 */
		public function isPathAbsolute() {
			return (bool) $this->isAbsolutePathForced;
		}

		/**
		 * Коэффициент похожести двух строк
		 * Возвращает число от 0 до 100, где 0 - ничего общего, 100 - одинаковые строки
		 * @param string $str1 первая строка
		 * @param string $str2 вторая строка
		 * @return int
		 */
		public static function compareStrings($str1, $str2) {
			return 100 * (
					similar_text($str1, $str2) / (
							(strlen($str1) + strlen($str2))
							/ 2)
			);
		}

		/**
		 * Конвертировать псевдостатический адрес в транслит и убрать недопустимые символы
		 * @param string $altName псевдостатический url
		 * @param bool|string $separator разделитель слов
		 * @return string
		 */
		public static function convertAltName($altName, $separator = false) {
			$config = mainConfiguration::getInstance();

			if (!$separator) {
				$separator = $config->get('seo', 'alt-name-separator');
				$separator = ($separator) ? $separator : "_";
			}

			$altName = translit::convert($altName, $separator);
			$altName = preg_replace("/[\?\\\\&=]+/", "_", $altName);
			$altName = preg_replace("/[_\/]+/", "_", $altName);

			return $altName;
		}

		/**
		 * Текущий UNIX TIMESTAMP
		 * @return int
		 */
		public static function getTimeStamp() {
			return time();
		}

		/**
		 * Переместить страницу $elementId в страницу $parentId перед страницей $previousElementId
		 * @param int $elementId идентификатор перемещаемой страницы
		 * @param int $parentId идентификатор новой родительской страницы
		 * @param int|bool $previousElementId = false идентификатор страницы, перед которой
		 * нужно разместить $elementId. Если не указан, страница помещается в конец списка.
		 * @return bool
		 */
		public function moveBefore($elementId, $parentId, $previousElementId = false) {
			if (!$this->isExists($elementId)) {
				return false;
			}

			$element = $this->getElement($elementId);

			$elementId = (int) $elementId;
			$parentId = (int) $parentId;
			$langId = $element->getLangId();
			$domainId = $element->getDomainId();

			try {
				$element->setRel($parentId);
			} catch	(coreException $e) {
				return false;
			}

			$currentTemplateId = $element->getTplId();
			$availableTemplates = $this->templatesCollection->getTemplatesList($domainId, $langId);
			$templateWillChange = true;

			/** @var template $template */
			foreach ($availableTemplates as $template) {
				if ($template->getId() == $currentTemplateId) {
					$templateWillChange = false;
					break;
				}
			}

			$connection = ConnectionPool::getInstance()->getConnection();

			if ($templateWillChange) {
				$defaultTemplate = $this->templatesCollection->getDefaultTemplate($domainId, $langId);

				if ($defaultTemplate) {
					$defaultTemplateId = $defaultTemplate->getId();

					$sel = new selector('pages');
					$sel->where('hierarchy')->page($elementId)->level(100);
					$sel->option('return')->value('id');
					$result = $sel->result();

					$descendantIds = array_map(function($info) { return $info['id']; }, $result);
					$descendantIds[] = $elementId;

					$idCondition = implode(",", $descendantIds);
					$sql = "UPDATE cms3_hierarchy SET tpl_id = '{$defaultTemplateId}' WHERE id IN ({$idCondition})";
					$connection->query($sql);
				}
			}

			if ($previousElementId) {
				$previousElementId = (int) $previousElementId;
				$sql = "SELECT ord FROM cms3_hierarchy WHERE id = '{$previousElementId}'";
				$result = $connection->queryResult($sql, true);
				$result->setFetchType(IQueryResult::FETCH_ROW);

				if ($result->length() == 0) {
					return false;
				}

				$fetchResult = $result->fetch();
				$ord = (int) array_shift($fetchResult);
				$sql = <<<SQL
UPDATE cms3_hierarchy
SET ord = (ord + 1)
WHERE rel = '{$parentId}' AND lang_id = '{$langId}' AND domain_id = '{$domainId}' AND ord >= {$ord}
SQL;
				$connection->query($sql);

				$element->setOrd($ord);
				$this->rewriteElementAltName($elementId);
				$this->rebuildRelationNodes($elementId);
				$this->addUpdatedElementId($elementId);
				$element->commit();
				return true;

			} else {
				$sql = <<<SQL
SELECT MAX(ord)
FROM cms3_hierarchy
WHERE rel = '{$parentId}' AND lang_id = '{$langId}' AND domain_id = '{$domainId}'
SQL;
				$result = $connection->queryResult($sql);
				$result->setFetchType(IQueryResult::FETCH_ROW);
				$ord = 1;

				if ($result->length() > 0) {
					$fetchResult = $result->fetch();
					$ord = 1 + (int) array_shift($fetchResult);
				}

				$element->setOrd($ord);
				$this->rewriteElementAltName($elementId);
				$this->rebuildRelationNodes($elementId);
				$this->addUpdatedElementId($elementId);
				$element->commit();

				return true;
			}
		}

		/**
		 * Переместить $elementId внутрь $parentId в начало списка детей
		 * @param int $elementId идентификатор перемещаемой страницы
		 * @param int $parentId идентификатор новой родительской страницы
		 * @return bool
		 */
		public function moveFirst($elementId, $parentId) {
			$elementId = (int) $elementId;
			$parentId = (int) $parentId;

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT id FROM cms3_hierarchy WHERE rel = '{$parentId}' ORDER BY ord ASC";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$previousElementId = null;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$previousElementId = (int) array_shift($fetchResult);
			}

			return $this->moveBefore($elementId, $parentId, $previousElementId);
		}

		/**
		 * Есть ли у текущего пользователя права на чтение $elementId?
		 * @param int $elementId идентификатор страницы
		 * @return bool
		 */
		public function isAllowed($elementId) {
			if (!is_numeric($elementId)) {
				return false;
			}
			$permissions = permissionsCollection::getInstance();
			$readStatus = $permissions->isAllowedObject(UmiCms\Service::Auth()->getUserId(), $elementId);
			return $readStatus[0];
		}

		/**
		 * Идентификатор объектного типа данных, которому принадлежит больше всего страниц внутри $elementId
		 * @param int $elementId идентификатор страницы
		 * @param int $depth = 1 глубина поиска
		 * @param int $hierarchyTypeId = null идентификатор иерархического типа страниц
		 * @return int
		 */
		public function getDominantTypeId($elementId, $depth = 1, $hierarchyTypeId = null) {
			if (!$this->isExists($elementId) && $elementId !== 0) {
				return false;
			}

			if ($elementId === 0) {
				$cmsController = cmsController::getInstance();
				$langId = $cmsController->getCurrentLang()->getId();
				$domainId = $cmsController->getCurrentDomain()->getId();
			} else {
				$elementId = (int) $elementId;
				$element = $this->getElement($elementId, true);

				if (!$element instanceof iUmiHierarchyElement) {
					return false;
				}

				$langId = $element->getLangId();
				$domainId = $element->getDomainId();
			}

			$depth = (int) $depth;
			$hierarchyTypeId = (int) $hierarchyTypeId;
			$typeCondition = ($hierarchyTypeId) ? "AND h.type_id = '{$hierarchyTypeId}'" : '';

			if ($depth > 1) {
				$sql = <<<SQL
SELECT o.type_id, COUNT(*) AS c
FROM cms3_hierarchy h, cms3_objects o, cms3_hierarchy_relations hr
WHERE hr.rel_id = '{$elementId}' AND h.id = hr.child_id AND h.is_deleted = '0'
	AND o.id = h.obj_id AND h.lang_id = '{$langId}' AND h.domain_id = '{$domainId}'
	{$typeCondition}
GROUP BY o.type_id
ORDER BY c DESC
LIMIT 1
SQL;
			} else {
				$sql = <<<SQL
SELECT o.type_id, COUNT(*) AS c
FROM cms3_hierarchy h, cms3_objects o
WHERE h.rel = '{$elementId}' AND h.is_deleted = '0' AND o.id = h.obj_id
	AND h.lang_id = '{$langId}' AND h.domain_id = '{$domainId}'
	{$typeCondition}
GROUP BY o.type_id
ORDER BY c DESC
LIMIT 1
SQL;
			}

			if ($typeId = (int) $this->cacheFrontend->loadSql($sql)) {
				return $typeId;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			if ($result->length() == 0) {
				return null;
			}

			$fetchResult = $result->fetch();
			$typeId = (int) array_shift($fetchResult);
			$this->cacheFrontend->saveSql($sql, $typeId);

			return $typeId;
		}

		/**
		 * Пометить страницу $elementId как измененную
		 * @param int $elementId идентификатор страницы
		 */
		public function addUpdatedElementId($elementId) {
			if (!in_array($elementId, $this->updatedElements)) {
				$this->updatedElements[] = $elementId;
			}
		}

		/**
		 * Список идентификаторов измененных страниц
		 * @return int[]
		 */
		public function getUpdatedElements() {
			return $this->updatedElements;
		}

		/** Очистить кэш измененных страниц */
		protected function forceCacheCleanup() {
			if (sizeof($this->updatedElements)) {
				$enabled = (bool) $this->mainConfiguration->get('cache', 'static.enabled');
				if ($enabled) {
					$staticCache = new staticCache();
					$staticCache->cleanup();
				}
			}
		}

		/** Деструктор */
		public function __destruct() {
			if (defined('SMU_PROCESS') && SMU_PROCESS) {
				return;
			}

			$this->forceCacheCleanup();

			if (sizeof($this->pendingSymlinkPairs)) {
				foreach ($this->pendingSymlinkPairs as $pair) {
					list($elementId, $symlinkId) = $pair;
					$this->copyElement($elementId, $symlinkId);
				}
				$this->pendingSymlinkPairs = [];
			}
		}

		/**
		 * Список идентификаторов запрошенных страниц
		 * @return int[]
		 */
		public function getCollectedElements() {
			return array_merge(array_keys($this->loadedElements), $this->collectedElementIds);
		}

		/**
		 * Выгрузить страницу $elementId
		 * @param int $elementId идентификатор страницы
		 * @return bool
		 */
		public function unloadElement($elementId) {
			static $currentElementId;

			if ($currentElementId === null) {
				$currentElementId = cmsController::getInstance()->getCurrentElementId();
			}

			if ($currentElementId == $elementId) {
				return false;
			}

			if (array_key_exists($elementId, $this->loadedElements)) {
				unset($this->loadedElements[$elementId]);
			} else {
				return false;
			}
		}

		/** Выгрузить все страницы */
		public function unloadAllElements() {
			static $currentElementId;

			if ($currentElementId === null) {
				$currentElementId = cmsController::getInstance()->getCurrentElementId();
			}

			foreach ($this->loadedElements as $elementId => $_dummy) {
				if ($currentElementId == $elementId) {
					continue;
				}
				unset($this->loadedElements[$elementId]);
			}
		}

		/**
		 * Добавить время последней модификации страницы максимальное для текущей сессии
		 * @param int $updateTime = 0 время в формате UNIX TIMESTAMP
		 */
		private function pushElementsLastUpdateTime($updateTime = 0) {
			if ($updateTime > $this->elementsLastUpdateTime) {
				$this->elementsLastUpdateTime = $updateTime;
			}
		}

		/**
		 * Максимальное значение атрибута "дата последней модификации" среди загруженных страниц
		 * @return int дата в формате UNIX TIMESTAMP
		 */
		public function getElementsLastUpdateTime() {
			return $this->elementsLastUpdateTime;
		}

		/**
		 * Максимальный уровень вложенности потомков страницы $elementId
		 * @param int $elementId идентификатор страницы
		 * @return int|false
		 */
		public function getMaxNestingLevel($elementId) {
			$elementId = (int) $elementId;

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT max(level) FROM `cms3_hierarchy_relations` WHERE rel_id = '{$elementId}'";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$level = false;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$level = (int) array_shift($fetchResult);
			}

			return $level;
		}

		/**
		 * Идентификаторы страниц, использующие $objectId в качестве источника данных
		 * @param int $objectId идентификатор объекта
		 * @param bool $ignoreDomain искать независимо от домена
		 * @param bool $ignoreLang искать независимо от языковой версии
		 * @param bool $ignoreDeleted игнорировать страницы в корзине
		 * @return int[]
		 */
		public function getObjectInstances($objectId, $ignoreDomain = false, $ignoreLang = false, $ignoreDeleted = false) {
			$objectId = (int) $objectId;
			$sql = "SELECT id FROM cms3_hierarchy WHERE obj_id = '{$objectId}'";
			$cmsController = cmsController::getInstance();

			if (!$ignoreDomain) {
				$domainId = $cmsController->getCurrentDomain()->getId();
				$sql .= " AND domain_id = '{$domainId}'";
			}

			if (!$ignoreLang) {
				$langId = $cmsController->getCurrentLang()->getId();
				$sql .= " AND lang_id = '{$langId}'";
			}

			if ($ignoreDeleted) {
				$sql .= " AND is_deleted = 0";
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$instanceIds = [];

			foreach ($result as $row) {
				$instanceIds[] = array_shift($row);
			}

			return $instanceIds;
		}

		/**
		 * Идентификатор шаблона, который выставлен у большинства страниц внутри $elementId
		 * @param int $parentId идентификатор родительской страницы
		 * @return int|bool
		 */
		public function getDominantTplId($parentId) {
			$parentId = (int) $parentId;
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = <<<SQL
SELECT `tpl_id`, COUNT(*) AS `cnt`
FROM cms3_hierarchy
WHERE rel = '{$parentId}' AND is_deleted = '0' GROUP BY tpl_id ORDER BY `cnt` DESC
SQL;
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				return (int) array_shift($fetchResult);
			}

			$element = $this->getElement($parentId);

			if ($element instanceof umiHierarchyElement) {
				return $element->getTplId();
			}

			return false;
		}

		/**
		 * Список идентификаторов страниц, измененных с даты $timestamp
		 * @param int $limit ограничение на количество результатов
		 * @param int $timestamp = 0 дата в формате UNIX TIMESTAMP
		 * @return int[]
		 */
		public function getLastUpdatedElements($limit, $timestamp = 0) {
			$limit = (int) $limit;
			$timestamp = (int) $timestamp;

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT id FROM cms3_hierarchy WHERE updatetime >= {$timestamp} LIMIT {$limit}";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$ids = [];

			foreach ($result as $row) {
				$ids[] = array_shift($row);
			}

			return $ids;
		}

		/**
		 * Проверить список страниц на предмет того, имеют ли они виртуальные копии
		 * @param array $elements [$pageId => $_dummy, ...]
		 * @param bool $includeDeleted учитывать ли все (удаленные и неудаленные) страницы
		 * @return array измененный массив со значениями для запрошенных страниц:
		 * true - если виртуальные копии есть, false - если нет
		 */
		public function checkIsVirtual($elements, $includeDeleted = false) {
			if (sizeof($elements) == 0) {
				return $elements;
			}

			foreach ($elements as $elementId => $_dummy) {
				$elementId = (int) $elementId;
				$element = $this->getElement($elementId);
				$elements[$elementId] = (string) $element->getObjectId();
			}

			$deletedCondition = ($includeDeleted) ? '' : "AND is_deleted = '0'";
			$objectsCondition = implode(", ", $elements);

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = <<<SQL
SELECT obj_id, COUNT(*) FROM cms3_hierarchy
WHERE obj_id IN ({$objectsCondition}) {$deletedCondition}
GROUP BY obj_id
SQL;
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			foreach ($result as $row) {
				list($objectId, $count) = $row;
				$isVirtual = $count > 1;

				foreach ($elements as $elementId => $originalObjectId) {
					if ($originalObjectId === $objectId) {
						$elements[$elementId] = $isVirtual;
					}
				}
			}

			return $elements;
		}

		/**
		 * Перепроверить псевдостатический URL страницы $elementId на предмет коллизий
		 * @param int $elementId идентификатор страницы
		 * @return bool
		 */
		protected function rewriteElementAltName($elementId) {
			$element = $this->getElement($elementId, true, true);

			if (!$element instanceof iUmiHierarchyElement) {
				return false;
			}

			$element->setAltName($element->getAltName());
			$element->commit();
			return true;
		}

		/**
		 * Стереть все записи, связанные со страницой $elementId из таблицы cms3_hierarchy_relations
		 * @param int $elementId идентификатор страницы
		 */
		protected function eraseRelationNodes($elementId) {
			$elementId = (int) $elementId;
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "DELETE FROM cms3_hierarchy_relations WHERE rel_id = '{$elementId}' OR child_id = '{$elementId}'";
			$connection->query($sql);
		}

		/**
		 * Перестроить дерево связей для страницы $elementId
		 * @param int $elementId идентификатор страницы
		 */
		public function rebuildRelationNodes($elementId) {
			$elementId = (int) $elementId;

			$this->eraseRelationNodes($elementId);
			$this->buildRelationNewNodes($elementId);
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT id FROM cms3_hierarchy WHERE rel = '{$elementId}'";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			foreach ($result as $row) {
				$this->rebuildRelationNodes(array_shift($row));
			}
		}

		/**
		 * Построить дерево связей для страницы $elementId относительно родителей
		 * @param int $elementId идентификатор страницы
		 * @return bool
		 */
		public function buildRelationNewNodes($elementId) {
			$elementId = (int) $elementId;
			$this->eraseRelationNodes($elementId);
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT rel FROM cms3_hierarchy WHERE id = '{$elementId}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			if ($result->length() == 0) {
				return false;
			}

			$fetchResult = $result->fetch();
			$parentId = array_shift($fetchResult);
			$parentIdCondition = ($parentId > 0) ? " = '{$parentId}'" : " IS NULL";

			$sql = <<<SQL
INSERT INTO cms3_hierarchy_relations (rel_id, child_id, level)
	SELECT rel_id, '{$elementId}', (level + 1)
	FROM cms3_hierarchy_relations
	WHERE child_id {$parentIdCondition}
SQL;
			$connection->query($sql);
			$parents = $this->getAllParents($parentId, true, true);

			$parents = array_extract_values($parents);
			$level = sizeof($parents);
			$parentIdValue = ($parentId > 0) ? "'{$parentId}'" : "NULL";

			$sql = <<<SQL
INSERT INTO cms3_hierarchy_relations (rel_id, child_id, level)
VALUES ({$parentIdValue}, '{$elementId}', '{$level}')
SQL;
			$connection->query($sql);
			return true;
		}

		/**
		 * Является ли $child потомком $parent?
		 * @param int|iUmiHierarchyElement $child страница-потомок
		 * @param int|iUmiHierarchyElement $parent страница-предок
		 * @return bool
		 */
		public function hasParent($child, $parent) {
			if (!$child) {
				return false;
			}

			if (is_numeric($child)) {
				$child = $this->getElement($child);
			}

			if (is_numeric($parent)) {
				$parent = $this->getElement($parent);
			}

			if (!$child instanceof umiHierarchyElement) {
				return false;
			}

			if (!$parent instanceof umiHierarchyElement) {
				return false;
			}

			if ($child->getRel() == $parent->getId()) {
				return true;
			}

			return $this->hasParent($child->getRel(), $parent);
		}

		/** Очистить внутренний кэш класса */
		public function clearCache() {
			$this->loadedElements = [];
			$this->pendingSymlinkPairs = [];
			$this->collectedElementIds = [];
			$this->pathCache = [];
			$this->altNameCache = [];
			$this->defaultIdCache = [];
			$this->parentIdCache = [];
			$this->idByPathCache = [];
		}

		/** Очистить внутренний кэш класса для страниц по умолчанию */
		public function clearDefaultElementCache() {
			$this->defaultIdCache = [];
		}

		/**
		 * Скорректированный псевдостатический адрес страницы
		 * В случае конфликта с уже существующим адресом к адресу добавляется цифра от 1 до 9, чтобы адрес был уникальным.
		 * Учитываются конфликты с названиями модулей, языками и адресами других страниц.
		 *
		 * @param string $altName псевдостатический адрес
		 * @param umiHierarchyElement $element страница
		 * @param bool $denseNumbering = false использовать все свободные цифры по порядку
		 * @param bool $ignoreCurrentElement = false не учитывать адрес страницы $element как конфликт
		 * @return string
		 */
		public function getRightAltName($altName, $element, $denseNumbering = false, $ignoreCurrentElement = false) {
			if (empty($altName)) {
				$altName = '1';
			}

			$parentId = $element->getRel();

			if ($parentId == 0 && !IGNORE_MODULE_NAMES_OVERWRITE) {
				$moduleNames = $this->regedit->getList("//modules");

				foreach ($moduleNames as $moduleName) {
					if ($altName == $moduleName[0]) {
						$altName .= '1';
						break;
					}
				}

				if ($this->langsCollection->getLangId($altName)) {
					$altName .= '1';
				}
			}

			$regex = "/^([a-z0-9_.-]*)(\d*?)$/U";
			preg_match($regex, $altName, $matches);
			$altString = isset($matches[1]) ? $matches[1] : null;
			$altDigit = isset($matches[2]) ? $matches[2] : null;

			$langId = $element->getLangId();
			$domainId = $element->getDomainId();

			$idCondition = ($ignoreCurrentElement) ? '' : "AND id <> {$element->getId()}";
			$sql = <<<SQL
SELECT alt_name
FROM cms3_hierarchy
WHERE rel = {$parentId} {$idCondition} AND is_deleted = '0' AND lang_id = '{$langId}'
	AND domain_id = '{$domainId}' AND alt_name LIKE '{$altString}%';
SQL;
			$connection = ConnectionPool::getInstance()->getConnection();
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			$existingAltNames = [];

			foreach ($result as $row) {
				$existingAltNames[] = array_shift($row);
			}

			if (in_array($altName, $existingAltNames)) {
				foreach ($existingAltNames as $nextAltName) {
					preg_match($regex, $nextAltName, $matches);
					if (!empty($matches[2])) {
						$altDigit = max($altDigit, $matches[2]);
					}
				}

				$altDigit += 1;

				if ($denseNumbering) {
					for ($nextDigit = 1; $nextDigit < $altDigit; $nextDigit += 1) {
						if (!in_array($altString . $nextDigit, $existingAltNames)) {
							$altDigit = $nextDigit;
							break;
						}
					}
				}
			}

			return $altString . $altDigit;
		}

		/**
		 * Возвращает максимальное значений порядка вывода среди страниц, дочерних родительской
		 * @param int $parentId идентификатор родителькой страницы
		 * @return int
		 */
		protected function getMaxOrdAmongChildren($parentId) {
			$parentId = (int) $parentId;
			$sql = "SELECT MAX(ord) FROM cms3_hierarchy WHERE rel = $parentId";

			$connection = ConnectionPool::getInstance()
				->getConnection();
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				return (int) array_shift($fetchResult);
			}

			return 0;
		}

		/**
		 * Возвращает порядок вывода для новой страницы
		 * @param int $parentId идентификатор страницы, родительской для новой
		 * @return int
		 */
		protected function getNewPageOrd($parentId) {
			return (int) $this->getMaxOrdAmongChildren($parentId) + self::INCREMENT_NEW_PAGE_ORDER;
		}

		/**
		 * Собрать запрос на получение дочерних элементов, выполнить запрос и вернуть результат
		 * @param string $selectCondition что выбирается из базы данных
		 * @param string $whereCondition условия выборки
		 * @param bool $disallowPermissions не учитывать права дочерних элементов в выборке
		 * @param bool $useHierarchySort использовать сортировку по иерархии
		 * @return IQueryResult
		 */
		private function runChildrenQuery($selectCondition, $whereCondition,
				$disallowPermissions, $useHierarchySort = false) {
			if ($disallowPermissions) {
				$permissionsJoin = '';
				$permissionsCondition = '';
			} else {
				$umiPermissions = permissionsCollection::getInstance();
				$userId = UmiCms\Service::Auth()->getUserId();
				$permissionsJoin = 'LEFT JOIN `cms3_permissions` as cp ON cp.`rel_id` = hierarchy.`id`';
				$permissionsCondition = 'AND ' . $umiPermissions->makeSqlWhere($userId) . ' AND cp.`level`&1 = 1';
			}

			$orderCondition = ($useHierarchySort) ? 'ORDER BY relations.`level`, hierarchy.`ord`' : '';
			$connection = ConnectionPool::getInstance()->getConnection();
			$getChildrenIds = <<<SQL
SELECT
$selectCondition
FROM `cms3_hierarchy_relations` as relations
LEFT JOIN `cms3_hierarchy` as hierarchy ON relations.`child_id` = hierarchy.`id`
$permissionsJoin
WHERE
$whereCondition
AND hierarchy.`is_deleted` = 0
$permissionsCondition
$orderCondition
SQL;
			$result = $connection->queryResult($getChildrenIds);
			$result->setFetchType(IQueryResult::FETCH_ASSOC);

			return $result;
		}

		/**
		 * Собрать и вернуть условие на получение дочерних элементов
		 * @param int $rootPageId идентификатор родительской страницы
		 * @param int $hierarchyTypeId идентификатор иерархического типа данных,
		 * к которому должны принадлежать дочерние страницы
		 * @param bool $allowInactive включить в результат неактивные дочерние страницы
		 * @param bool $allowInvisible включить в результат невидимые в меню дочерние страницы
		 * @param int $domainId включить в результат только страницы из этого домена (работает если ищем от корня)
		 * @param int $languageId включить в результат только страницы из этого языка (работает если ищем от корня)
		 * @return string
		 */
		private function getChildrenWhereCondition($rootPageId, $hierarchyTypeId, $allowInactive,
				$allowInvisible, $domainId, $languageId) {
			$rootPageId = (int) $rootPageId;
			$relationCondition = ($rootPageId === 0) ? ' relations.`rel_id` IS NULL ' : " relations.`rel_id` = {$rootPageId}";

			$hierarchyTypeId = (int) $hierarchyTypeId;
			$hierarchyTypeCondition = ($hierarchyTypeId === 0) ? '' : " AND hierarchy.`type_id` = {$hierarchyTypeId} ";

			$activeCondition = ($allowInactive) ? '' : ' AND hierarchy.`is_active` = 1 ';
			$visibleCondition = ($allowInvisible) ? '' : ' AND hierarchy.`is_visible` = 1 ';

			$languageId = (!is_numeric($languageId)) ? (int) $this->getCurrentLanguageId() : (int) $languageId;
			$languageCondition = ($rootPageId !== 0) ? '' : " AND hierarchy.`lang_id` = {$languageId} ";

			$domainId = (!is_numeric($domainId)) ? (int) $this->getCurrentDomainId() : (int) $domainId;
			$domainCondition = ($rootPageId !== 0) ? '' : " AND hierarchy.`domain_id` = {$domainId} ";

			return ($relationCondition . $hierarchyTypeCondition . $activeCondition .
					$visibleCondition . $languageCondition . $domainCondition);
		}

		/**
		 * Удаляет страницу из системы.
		 * Если страница не имеет виртуальных копий - так же будет
		 * вызвано удаления объекта, который является источником данных для страницы
		 * @param iUmiHierarchyElement $element удаляемая страница
		 * @return $this
		 */
		private function deleteElement(iUmiHierarchyElement $element) {
			$userId = UmiCms\Service::Auth()->getUserId();

			$event = new umiEventPoint('systemKillElement');
			$event->addRef('element', $element);
			$event->setParam('user_id', $userId);
			$event->setMode("before");
			$event->call();

			$elementHasVirtualCopies = $element->hasVirtualCopy();
			$elementId = (int) $element->getId();
			$sql = "DELETE FROM cms3_hierarchy WHERE id = $elementId";
			ConnectionPool::getInstance()
				->getConnection()
				->query($sql);

			if (!$elementHasVirtualCopies) {
				$objectId = (int) $element->getObjectId();
				$this->umiObjectsCollection
					->delObject($objectId);
			}

			unset($element);
			$this->unloadElement($elementId);

			return $this;
		}

		/** @deprecated */
		public function getChilds($element_id, $allow_unactive = true, $allow_unvisible = true, $depth = 0,
				$hierarchy_type_id = false, $domainId = false, $langId = false) {
			return $this->getChildrenTree(
					$element_id,
					$allow_unactive,
					$allow_unvisible,
					$depth,
					$hierarchy_type_id,
					$domainId,
					$langId
			);
		}

		/** @deprecated */
		public function getChildIds($element_id, $allow_unactive = true, $allow_unvisible = true,
				$hierarchy_type_id = false, $domainId = false, $include_self = false, $langId = false) {
			return $this->getChildrenList(
					$element_id,
					$allow_unactive,
					$allow_unvisible,
					$hierarchy_type_id,
					$domainId,
					$include_self,
					$langId
			);
		}

		/** @deprecated */
		public function getChildsCount($element_id, $allow_unactive = true, $allow_unvisible = true,
				$depth = 0, $hierarchy_type_id = false, $domainId = false) {
			return $this->getChildrenCount(
					$element_id,
					$allow_unactive,
					$allow_unvisible,
					$depth,
					$hierarchy_type_id,
					$domainId
			);
		}

		/** @deprecated */
		public function getElementsCount($module, $method = "") {
			$hierarchy_type_id = $this->umiHierarchyTypesCollection->getTypeByName($module, $method)->getId();

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT COUNT(*) FROM cms3_hierarchy WHERE type_id = '{$hierarchy_type_id}'";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$count = false;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$count = (int) array_shift($fetchResult);
			}

			return $count;
		}
	}
