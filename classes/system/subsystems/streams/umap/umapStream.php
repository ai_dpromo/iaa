<?php
	class umapStream extends umiBaseStream {
		protected $scheme = "umap";


		public function stream_open($path, $mode, $options, $opened_path) {
			$cacheFrontend = cacheFrontend::getInstance();
			$path = $this->removeHash($path);
			$path = $this->parsePath($path);
			
			if($data = $cacheFrontend->loadData($path)) {
				return $this->setData($data);
			}
			
			if($path) {
				$matches = new matches("sitemap.xml");
				$matches->setCurrentURI($path);
				$data = $matches->execute(false);
				
				if($this->expire) $cacheFrontend->saveObject($path, $data, $this->expire);
				return $this->setData($data);
			} else {
				return $this->setDataError('not-found');
			}
		}
		
		
		protected function parsePath($path) {
			$path = parent::parsePath($path);
			if($path) {
				return $this->path = $path;
			} else {
				return $this->path = false;
			}
		}
	};
?>