<?php
	class custom extends def_module {
		public function cms_callMethod($method_name, $args) {
			return call_user_func_array(Array($this, $method_name), $args);
		}
		
		public function __call($method, $args) {
			throw new publicException("Method " . get_class($this) . "::" . $method . " doesn't exist");
		}
			public function getUserIp() {
		  //получаем HTTP_X_FORWARDED_FOR
		  $forward_ip = @getenv('HTTP_X_FORWARDED_FOR');
		  if(empty($forward_ip)){
			$user_ip = @getenv('REMOTE_ADDR');
			$proxy_ip = '';
		  } else{
			$user_ip = $forward_ip;
			$proxy_ip = @getenv('REMOTE_ADDR');
		  }
		  return $user_ip;
		}	
	}