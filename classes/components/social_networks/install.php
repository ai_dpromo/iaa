<?php
	/**
	 * Установщик модуля
	 */

	/**
	 * @var array $INFO реестр модуля
	 */
	$INFO = Array();
	$INFO['name'] = "social_networks";
	$INFO['config'] = "0";
	$INFO['default_method'] = "vkontakte";
	$INFO['default_method_admin'] = "vkontakte";
	$INFO['per_page'] = 10;

	/**
	 * @var array $COMPONENTS файлы модуля
	 */
	$COMPONENTS = array();
	$COMPONENTS[] = "./classes/components/social_network/admin.php";
	$COMPONENTS[] = "./classes/components/social_network/autoload.php";
	$COMPONENTS[] = "./classes/components/social_network/class.php";
	$COMPONENTS[] = "./classes/components/social_network/customAdmin.php";
	$COMPONENTS[] = "./classes/components/social_network/customMacros.php";
	$COMPONENTS[] = "./classes/components/social_network/handlers.php";
	$COMPONENTS[] = "./classes/components/social_network/i18n.en.php";
	$COMPONENTS[] = "./classes/components/social_network/i18n.php";
	$COMPONENTS[] = "./classes/components/social_network/includes.php";
	$COMPONENTS[] = "./classes/components/social_network/install.php";
	$COMPONENTS[] = "./classes/components/social_network/lang.en.php";
	$COMPONENTS[] = "./classes/components/social_network/lang.php";
	$COMPONENTS[] = "./classes/components/social_network/permissions.php";
	$COMPONENTS[] = "./classes/components/social_network/classes/network.php";
	$COMPONENTS[] = "./classes/components/social_network/classes/networks/vkontakte.php";
