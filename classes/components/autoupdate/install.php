<?php
	/**
	 * Установщик модуля
	 */

	/**
	 * @var array $INFO реестр модуля
	 */
	$INFO = Array();
	$INFO['name'] = "autoupdate";
	$INFO['config'] = "0";
	$INFO['default_method'] = "empty";
	$INFO['default_method_admin'] = "versions";

	/**
	 * @var array $COMPONENTS файлы модуля
	 */
	$COMPONENTS = array();
	$COMPONENTS[] = "./classes/components/autoupdate/admin.php";
	$COMPONENTS[] = "./classes/components/autoupdate/class.php";
	$COMPONENTS[] = "./classes/components/autoupdate/customAdmin.php";
	$COMPONENTS[] = "./classes/components/autoupdate/customMacros.php";
	$COMPONENTS[] = "./classes/components/autoupdate/handlers.php";
	$COMPONENTS[] = "./classes/components/autoupdate/i18n.en.php";
	$COMPONENTS[] = "./classes/components/autoupdate/i18n.php";
	$COMPONENTS[] = "./classes/components/autoupdate/includes.php";
	$COMPONENTS[] = "./classes/components/autoupdate/install.php";
	$COMPONENTS[] = "./classes/components/autoupdate/lang.en.php";
	$COMPONENTS[] = "./classes/components/autoupdate/lang.php";
	$COMPONENTS[] = "./classes/components/autoupdate/permissions.php";
	$COMPONENTS[] = "./classes/components/autoupdate/service.php";
?>
