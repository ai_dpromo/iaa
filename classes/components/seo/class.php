<?php
	/**
	 * Базовый класс модуля "SEO".
	 *
	 * Модуль отвечает за:
	 *
	 * 1) Интеграцию с Megaindex;
	 * 2) Интеграцию с Яндекс.Вебмастер;
	 * 3) Работу с seo настройками доменов;
	 * @link http://help.docs.umi-cms.ru/rabota_s_modulyami/modul_seo/
	 */
	class seo extends def_module {

		/**
		 * Конструктор
		 */
		public function __construct() {
			parent::__construct();
			$cmsController = cmsController::getInstance();

			if ($cmsController->getCurrentMode() == "admin") {
				$configTabs = $this->getConfigTabs();

				if ($configTabs) {
					$configTabs->add("config");
					$configTabs->add("megaindex");
					$configTabs->add("yandex");
				}

				$commonTabs = $this->getCommonTabs();

				if ($commonTabs) {
					$commonTabs->add('seo');
					$commonTabs->add('links');
					$commonTabs->add('webmaster');
					$commonTabs->add('getBrokenLinks');
					$commonTabs->add("emptyMetaTags");
				}

				$this->__loadLib("admin.php");
				$this->__implement("SeoAdmin");

				$this->__loadLib("megaIndex.php");
				$this->__implement("SeoMegaIndex");

				$this->__loadLib("yandexWebMaster.php");
				$this->__implement("SeoYandexWebMaster");

				$this->loadAdminExtension();

				$this->__loadLib("customAdmin.php");
				$this->__implement("SeoCustomAdmin", true);
			}

			$this->__loadLib("macros.php");
			$this->__implement("SeoMacros");

			$this->loadSiteExtension();

			$this->__loadLib("customMacros.php");
			$this->__implement("SeoCustomMacros", true);

			$this->loadCommonExtension();
			$this->loadTemplateCustoms();
		}

	}
