<?php
	namespace UmiCms\Manifest\Seo;
	/**
	 * Команда удаления индекса карты сайта
	 */
	class DeleteIndexAction extends \Action {

		/**
		 * @var \iSiteMapUpdater $siteMapUpdater экземпляр класс обновления карты сайта
		 */
		private $siteMapUpdater;

		/**
		 * @inheritdoc
		 * @param string $name
		 * @param array $params
		 */
		public function __construct($name, array $params = []) {
			parent::__construct($name, $params);
			$this->siteMapUpdater = \UmiCms\Service::SiteMapUpdater();
		}

		/**
		 * @inheritdoc
		 * @return $this
		 */
		public function execute() {
			$this->getUpdater()
				->deleteAll();
			return $this;
		}

		/**
		 * @inheritdoc
		 * @return $this
		 */
		public function rollback() {
			return $this;
		}

		/**
		 * Возвращает экземпляр класс обновления карты сайта
		 * @return \iSiteMapUpdater
		 */
		private function getUpdater() {
			return $this->siteMapUpdater;
		}
	}