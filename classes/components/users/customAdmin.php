<?php
	/**
	 * Класс пользовательских методов административной панели
	 */
	class UsersCustomAdmin {
		/**
		 * @var users $module
		 */
		public $module;
		
		public function users_activate_mail2($e) {
			$objects = umiObjectsCollection::getInstance();
			$object = $e->getRef('object');
			$object_id = $object->getId();

			  if ($e->getMode() == "after") {
				// берем необходимые параметры
				//$user_id = $oEventPoint->getParam("user_id");

				// получаем объект "Пользователь"
				$oObject = umiHierarchy::getInstance()->getElement($object_id);
				if ($oObject instanceof umiHierarchyElement) {
				 
				 // формируем письмо
				 $oMyMail = new umiMail();
				 $oMyMail->setFrom("ivonina.alexandra@gmail.com", "mail");
				 $oMyMail->setSubject("Активация аккаунта");
				// $oMyMail->setContent("Ваш аккаунт на сайте <a href='http://iaa2017stpetersburg.com'>iaa2017stpetersburg.com</a> успешно активирован!<br/> Ваш логин: ".$oObject->getValue('login')."<br/> ");
				 $oMyMail->setContent("Ваш аккаунт успешно активирован!");
				 $oMyMail->addRecipient("ai@dpromo.su", "Admin");

				 // отправляем письмо
				 $oMyMail->commit();
				 $oMyMail->send();
				}
				return true;
			   }
		}
		public function users_activate_mail1($e) {
		$cor = cmsController::getInstance(); 
		$cur_lang_prefix = $cor->getLang()->getPrefix();
		include 'i18n.'.$cur_lang_prefix.'.php';
		
		$objects = umiObjectsCollection::getInstance();
		$object = $e->getRef('object');
		$object_id = $object->getId();
		$type_id = ($object instanceof umiHierarchyElement) ? $object->getObjectTypeId() : $object->getTypeId();
		$template = "default";
		//list($template_mail, $template_mail_subject) = def_module::loadTemplatesForMail("users/register/".$template, "mail_admin_changes", "onObjectEdit_subject");
		
				
		if($type_id == '51'){
			if($e->getMode() == "before") {
				
				
					$property1 = $object->getValue('is_activated');
					$property_d = $object->getValue('dostup_v_mobilnoe_prilozhenie');
					
					$e->setParam('property', $property1);
					$e->setParam('propertyn', $property_d);
					return true;
			}
			if($e->getMode() == "after") {
				$regedit = regedit::getInstance();
				$property1 = $e->getParam('property');
				$property_d = $e->getParam('propertyn');
				$user_mail = $object->getValue("e-mail");
				$user_login = $object->getValue("login");
                
				$otklonit_aktivaciyu = $object->getValue('otklonit_aktivaciyu');
				
               // $is_activated = $object->getValue('is_activated');
				$manager_mail = 'ai@dpromo.su';
				$mail_arr = Array();
				$mail_arr['user_id'] = $object_id;
				if($user_login) {
					$mail_arr['login'] = $user_login;
				}
				if($manager_mail) {
						$mail_arr['manager_mail_adress'] = $manager_mail;
				}
				if($otklonit_aktivaciyu == '1'){
					$object->setValue('is_activated', '0');
				}
				$object->commit();
				$property2 = $object->getValue('is_activated');
				$property_d2 = $object->getValue('dostup_v_mobilnoe_prilozhenie');
				
					if($property2 != $property1 || $otklonit_aktivaciyu == '1') {

                        $email_from = $regedit->getVal("//settings/email_from");
                        $fio_from = $regedit->getVal("//settings/fio_from");

                        $oMyMail = new umiMail();
                        $oMyMail->setFrom($email_from, $fio_from);
                        $oMyMail->setSubject($i18n['account-activation']);
						if ($property2 == '1') {
							$password = users::getRandomPassword();
							$encodedPassword = UmiCms\Service::PasswordHashAlgorithm()->hash($password);
							$object->setValue("password", $encodedPassword);
							$object->commit();
							
                            $oMyMail->setContent('

						'.$i18n['hello'].'<span style="text-transform:uppercase">'.$object->getValue('surname').' '.$object->getValue('namen').'</span>!<br/>'.$i18n['mail-actiation-body1'].'

							

							'.$i18n['mail-actiation-body2']);

                        } else {
							   $oMyMail->setContent($i18n['mail-deactiation-body']);
                         }
						
						//$oMyMail->setContent($property2 . "За вами не закреплен личный менеджер.");
                        $oMyMail->addRecipient($user_mail, $user_login);
						//<p>
						//		'.$i18n['mail-template-variable-login'].': '.$user_login.'<br />
						//		'.$i18n['mail-template-variable-password'].': '.$password.'<br />
							
						//	</p>

                        $oMyMail->commit();
						$oMyMail->send();

                       
                    }
					/*if($property_d != $property_d2 && $property_d2 == '1') {
						
					}*/
					
			 return true;
			}
			
			
			
			
		} 
		else{return true;}
		
	}
	/*	public function onObjectEdit($e) {
		
		$objects = umiObjectsCollection::getInstance();
		$object = $e->getRef('object');
		$object_id = $object->getId();
		$type_id = ($object instanceof umiHierarchyElement) ? $object->getObjectTypeId() : $object->getTypeId();
		$template = "default";
		list($template_mail, $template_mail_subject) = def_module::loadTemplatesForMail("users/register/".$template, "mail_admin_changes", "onObjectEdit_subject");
		
				
		if($type_id == '51'){
			if($e->getMode() == "before") {
				
				
					$is_activated = $object->getValue('is_activated');
					
					umiEventPoint::setParam($property, $paramValue = $is_activated);
					
					
					 return true;
			}
			if($e->getMode() == "after") {
				$regedit = regedit::getInstance();
				$property1 = umiEventPoint::getParam($property);
				$user_mail = $object->getValue("e-mail");
				$user_login = $object->getValue("login");
				$user_login = $object->getValue("password");
                $property2 = '';
               	$is_activated = $object->getValue('is_activated');
				$property2 = $is_activated;
			
				$mail_arr = Array();
				$mail_arr['user_id'] = $object_id;
				if($user_login) {
					$mail_arr['login'] = $user_login;
				}
				if($manager_mail) {
						$mail_arr['manager_mail_adress'] = $manager_mail;
				}


				
					if($property2 != $property1 ) {

                        $email_from = $regedit->getVal("//settings/email_from");
                        $fio_from = $regedit->getVal("//settings/fio_from");

                        $oMyMail = new umiMail();
                        $oMyMail->setFrom($email_from, $fio_from);
                        $oMyMail->setSubject("Изменение личного менеджера");

                       	if ($property2 != '') {

                            $oMyMail->setContent("Вам назначен личный менеджер.<br/>Подробная информация:<br/>" . $property2 . "<br/>" . $manager_mail . "<br/>" . $manager_phone);

                        } else {
                            $oMyMail->setContent($property2 . "За вами не закреплен личный менеджер.");
                         }
                        $oMyMail->addRecipient($user_mail, $user_login);


                        $oMyMail->commit();

                        $oMyMail = new umiMail();
                        $oMyMail->setFrom($email_from, $fio_from);
                        $oMyMail->setSubject("За вами закреплен новый клиент");
                        $mail_content = "За вами закреплен новый клиент.<br/>";
                        $mail_content = $mail_content . def_module::parseTemplateForMail($template_mail, $mail_arr, false, $object_id);
                        $oMyMail->setContent($mail_content);
                        $oMyMail->addRecipient($manager_mail, $property2);

                        $oMyMail->commit();

                    }
					
			 return true;
			}
			
			
			
			
		} 
		else{return true;}
		
	}*/

	}
?>