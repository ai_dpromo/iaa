<?php
	/**
	 * Класс пользовательских макросов
	 */
	class UsersCustomMacros {
		/**
		 * @var users $module
		 */
		public $module;
		
		public function create_csv()
		{
		$min = $_GET["minDate"];
		$max = $_GET["maxDate"];
		//$hierarchy = umiHierarchy::getInstance();
		$fieldsCollection = umiFieldsCollection::getInstance();
		$objectsCollection = umiObjectsCollection::getInstance();
 
		$users = new selector('objects');
		$users->types('object-type')->name('users', 'user');
		if($min != null && $max != null){
			$users->where('register_date')->between($min, $max);
		}
		
		//$file = '/sample.csv';
		//header('Content-Type: text/csv; charset=windows-1251');
		$fp = fopen('users.csv', 'w');

		$items_arr = array();
			$item_arr = array();
			$item_arr['attribute:active'] = iconv( "utf-8", "windows-1251", 'Дата регистрации');
			$item_arr['attribute:number'] = iconv( "utf-8", "windows-1251", 'Номер п/п');
			
			$item_arr['attribute:surname'] = iconv( "utf-8", "windows-1251", 'Фамилия');
			$item_arr['attribute:id'] = iconv( "utf-8", "windows-1251", 'Имя');
			$item_arr['attribute:patronymic'] = iconv( "utf-8", "windows-1251", 'Отчество');
			$item_arr['attribute:yearofbirth'] = iconv( "utf-8", "windows-1251", 'Дата рождения');
			$item_arr['attribute:placeofbirth'] = iconv( "utf-8", "windows-1251", 'Место рождения');
			$item_arr['attribute:passportseries'] = iconv( "utf-8", "windows-1251", 'Серия паспорта');
			$item_arr['attribute:passportid'] = iconv( "utf-8", "windows-1251", 'Номер паспорта');
			$item_arr['attribute:country'] = iconv( "utf-8", "windows-1251", 'Страна проживания');
			$item_arr['attribute:registration'] = iconv( "utf-8", "windows-1251", 'Адрес регистрации');
			$item_arr['attribute:primechanie'] = iconv( "utf-8", "windows-1251", 'Примечание');
			
			$item_arr['attribute:is_activated'] = iconv( "utf-8", "windows-1251", 'Аккредитован');
			$item_arr['attribute:city'] = iconv( "utf-8", "windows-1251", 'город');
			$item_arr['attribute:phonenumber'] = iconv( "utf-8", "windows-1251", 'номер телефона');
			$item_arr['attribute:email'] = iconv( "utf-8", "windows-1251", 'e-mail');
			$item_arr['attribute:i18nfieldavatar'] = iconv( "utf-8", "windows-1251", 'фото');
			$item_arr['attribute:fullcompanyname'] = iconv( "utf-8", "windows-1251", 'полное наименование компании');
			$item_arr['attribute:shortcompanyname'] = iconv( "utf-8", "windows-1251", 'краткое наименование компании');
			$item_arr['attribute:companylocation'] = iconv( "utf-8", "windows-1251", 'город, где расположена компания');
			$item_arr['attribute:position'] = iconv( "utf-8", "windows-1251", 'должность');
			$item_arr['attribute:participantcategory'] = iconv( "utf-8", "windows-1251", 'категория участника');
			$item_arr['attribute:participant'] = iconv( "utf-8", "windows-1251", 'Участник');
			$item_arr['attribute:governmentdepartments'] = iconv( "utf-8", "windows-1251", 'органы государственной власти');
			$item_arr['attribute:nameb'] = iconv( "utf-8", "windows-1251", 'name');
			$item_arr['attribute:surnameb'] = iconv( "utf-8", "windows-1251", 'surname');
			$item_arr['attribute:companyb'] = iconv( "utf-8", "windows-1251", 'company');
			$item_arr['attribute:cityb'] = iconv( "utf-8", "windows-1251", 'city');
			$item_arr['attribute:countryb'] = iconv( "utf-8", "windows-1251", 'country');
			$item_arr['attribute:issuingcountry'] = iconv( "utf-8", "windows-1251", 'страна выдачи');
			$item_arr['attribute:dateofissueandauthority'] = iconv( "utf-8", "windows-1251", 'дата и место выдачи');
			$item_arr['attribute:bestcases'] = iconv( "utf-8", "windows-1251", 'лучшие кейсы/работы за последние 3 года');
		
			$items_arr[] = $item_arr;
			$i = 1;
		foreach ($users as  $item) {
		
			$item_arr = array();
			$number = $i;
			$surname = ($item->getValue('surname') ? $item->getValue('surname') : "");
            $namen = ($item->getValue('namen') ? $item->getValue('namen') : "");
			$patronymic = ($item->getValue('patronymic') ? $item->getValue('patronymic') : "");
			$yearofbirth = ($item->getValue('year-of-birth') ? $item->getValue('year-of-birth') : "");
			$placeofbirth = ($item->getValue('place-of-birth') ? $item->getValue('place-of-birth') : "");
			$passportseries = ($item->getValue('passport-series') ? $item->getValue('passport-series') : "");
			$passportid = ($item->getValue('passport-id') ? $item->getValue('passport-id') : "");
			$country = ($item->getValue('country') ? $item->getValue('country') : "");
			$registration = ($item->getValue('registration') ? $item->getValue('registration') : "");
			$primechanie = ($item->getValue('primechanie') ? $item->getValue('primechanie') : "");
			$active = ($item->getValue('register_date') ? $item->getValue('register_date') : "0");
			$is_activated = ($item->getValue('is_activated') ? " + " : " - ");
			
			$city = ($item->getValue('city') ? $item->getValue('city') : "");
			$phonenumber = ($item->getValue('phone-number') ? $item->getValue('phone-number') : "");
			$email = ($item->getValue('email') ? $item->getValue('email') : "");
			$i18nfieldavatar = ($item->getValue('18nfieldavatar') ? " + " : "");
			$fullcompanyname = ($item->getValue('full-company-name') ? $item->getValue('full-company-name') : "");
			$shortcompanyname = ($item->getValue('short-company-name') ? $item->getValue('short-company-name') : "");
			$companylocation = ($item->getValue('company-location') ? $item->getValue('company-location') : "");
			
			$position = ($item->getValue('position') ? $item->getValue('position') : "");
			
			if($item->getValue('participant-category')){
				
				$value_field = $objectsCollection->getObject($item->getValue('participant-category'));
				$participantcategory = $value_field->getName();
				
			}
			else{
				$participantcategory = "";
			}
			
			if($item->getValue('participant')){
				
				$value_field = $objectsCollection->getObject($item->getValue('participant'));
				$participant = $value_field->getName();
				
			}
			else{
				$participant = "";
			}
			
			if($item->getValue('government-department')){
				
				$value_field = $objectsCollection->getObject($item->getValue('government-department'));
				$governmentdepartments = $value_field->getName();
				
			}
			else{
				$governmentdepartments = "";
			}
			//$participant = ($item->getValue('participant') ? $item->getValue('participant') : "");
			//$governmentdepartments = ($item->getValue('government-departments') ? $item->getValue('government-departments') : "");
			$nameb = ($item->getValue('nameb') ? $item->getValue('nameb') : "");
			$surnameb = ($item->getValue('surnameb') ? $item->getValue('surnameb') : "");
			$companyb = ($item->getValue('companyb') ? $item->getValue('companyb') : "");
			$cityb = ($item->getValue('cityb') ? $item->getValue('cityb') : "");
			$countryb = ($item->getValue('countryb') ? $item->getValue('countryb') : "");
			$issuingcountry = ($item->getValue('issuing-country') ? $item->getValue('issuing-country') : "");
			$dateofissueandauthority = ($item->getValue('date-of-issue-and-authority') ? $item->getValue('date-of-issue-and-authority') : "");
			$bestcases = ($item->getValue('best-cases') ? " + " : "");
			$kod_dlya_prilozheniya = ($item->getValue('kod_dlya_prilozheniya') ? $item->getValue('kod_dlya_prilozheniya') : "");
			$dostup_v_mobilnoe_prilozhenie = ($item->getValue('dostup_v_mobilnoe_prilozhenie') ? $item->getValue('dostup_v_mobilnoe_prilozhenie') : "");
		
			$item_arr['attribute:active'] = iconv( "utf-8", "windows-1251", $active);
			$item_arr['attribute:number'] = iconv( "utf-8", "windows-1251", $i);
			$item_arr['attribute:surname'] = iconv( "utf-8", "windows-1251", $surname);
			$item_arr['attribute:id'] = iconv( "utf-8", "windows-1251", $namen);
			$item_arr['attribute:patronymic'] = iconv( "utf-8", "windows-1251", $patronymic);
			$item_arr['attribute:yearofbirth'] = iconv( "utf-8", "windows-1251", $yearofbirth);
			$item_arr['attribute:placeofbirth'] = iconv( "utf-8", "windows-1251", $placeofbirth);
			$item_arr['attribute:passportseries'] = iconv( "utf-8", "windows-1251",  '="'.$passportseries.'"');
			$item_arr['attribute:passportid'] = iconv( "utf-8", "windows-1251", '="'.$passportid.'"');
			$item_arr['attribute:country'] = iconv( "utf-8", "windows-1251", $country);
			$item_arr['attribute:registration'] = iconv( "utf-8", "windows-1251", $registration);
			$item_arr['attribute:primechanie'] = iconv( "utf-8", "windows-1251", $primechanie);
			
			$item_arr['attribute:is_activated'] = iconv( "utf-8", "windows-1251", $is_activated);
			$item_arr['attribute:city'] = iconv( "utf-8", "windows-1251", $city);
			$item_arr['attribute:phonenumber'] = iconv( "utf-8", "windows-1251", '="'.$phonenumber.'"');
			$item_arr['attribute:email'] = iconv("utf-8", "windows-1251", $email);
			$item_arr['attribute:i18nfieldavatar'] = iconv( "utf-8", "windows-1251", $i18nfieldavatar);
			$item_arr['attribute:fullcompanyname'] = iconv( "utf-8", "windows-1251", $fullcompanyname);
			$item_arr['attribute:shortcompanyname'] = iconv( "utf-8", "windows-1251", $shortcompanyname);
			$item_arr['attribute:companylocation'] = iconv( "utf-8", "windows-1251", $companylocation);
			$item_arr['attribute:position'] = iconv( "utf-8", "windows-1251", $position);
			$item_arr['attribute:participantcategory'] = iconv( "utf-8", "windows-1251", $participantcategory);
			$item_arr['attribute:participant'] = iconv( "utf-8", "windows-1251", $participant);
			$item_arr['attribute:governmentdepartments'] = iconv( "utf-8", "windows-1251", $governmentdepartments);
			$item_arr['attribute:nameb'] = iconv( "utf-8", "windows-1251", $nameb);
			$item_arr['attribute:surnameb'] = iconv( "utf-8", "windows-1251", $surnameb);
			$item_arr['attribute:companyb'] = iconv( "utf-8", "windows-1251", $companyb);
			$item_arr['attribute:cityb'] = iconv( "utf-8", "windows-1251", $cityb);
			$item_arr['attribute:countryb'] = iconv( "utf-8", "windows-1251", $countryb);
			$item_arr['attribute:issuingcountry'] = iconv( "utf-8", "windows-1251", $issuingcountry);
			$item_arr['attribute:dateofissueandauthority'] = iconv( "utf-8", "windows-1251",$dateofissueandauthority);
		
			$item_arr['attribute:bestcases'] = iconv( "utf-8", "windows-1251", $bestcases);
		
		
		
		
		
		
			$items_arr[] = $item_arr;
			$i++;
        }
	
			
		
		foreach ($items_arr as $fields) {
				fputcsv($fp, $fields, ';', '"');
			}
			
		fclose($fp);
		//return array('items' => array('nodes:item' => $items_arr));
		return 'http://'.$_SERVER['HTTP_HOST'].'/users.csv';
	}
	
	
	}
?>