<?php
 
$INFO = Array();

$INFO['verison'] = "1.0.0.0";

$INFO['name'] = "mobile";
$INFO['title'] = "Приложение";
$INFO['description'] = "Модуль Мобильного приложения";
$INFO['filename'] = "modules/mobile/class.php";
$INFO['config'] = "1";
$INFO['ico'] = "ico_mobile";
$INFO['default_method'] = "show";
$INFO['default_method_admin'] = "lists";
$INFO['per_page'] = "10";
 
$INFO['func_perms'] = "";
 
$COMPONENTS = array();
 
$COMPONENTS[0] = "./classes/modules/news/__admin.php";
$COMPONENTS[1] = "./classes/modules/news/class.php";
$COMPONENTS[2] = "./classes/modules/news/i18n.php";
$COMPONENTS[3] = "./classes/modules/news/lang.php";
$COMPONENTS[4] = "./classes/modules/news/permissions.php";


/*Создание типа данных для Обратная связь мобильное приложение*/
$hierarchyTypes = umiHierarchyTypesCollection::getInstance();
$hierarchyType = $hierarchyTypes->getTypeByName('mobile', 'communi');
if(!$hierarchyType) {
    $type_id = $hierarchyTypes->addType('mobile', 'Обратная связь мобильное приложение', 'communi');
} else {
    $type_id = $hierarchyType->getId();
}
$objectTypes = umiObjectTypesCollection::getInstance();
$objectTypeId = $objectTypes->getTypeByHierarchyTypeId($type_id);
if(!$objectTypeId) {
    $objectTypeId = $objectTypes->addType(0, 'Обратная связь мобильного приложения');
    $objectType = $objectTypes->getType($objectTypeId);
    $objectType->setHierarchyTypeId($type_id);
    $objectType->setIsGuidable(true);
    $objectType->commit();
} else {
    $objectType = $objectTypes->getType($objectTypeId);
}
$group = $objectType->getFieldsGroupByName('dannye', true);
if(!$group) {
    $group_id = $objectType->addFieldsGroup('dannye', 'Данные', true, false);
    $group = $objectType->getFieldsGroupByName('dannye');
}
$fields = $group->getFields();
$fieldDomain = false;
$fieldLang = false;

$fieldsCollection = umiFieldsCollection::getInstance();

$fieldTypesCollection = umiFieldTypesCollection::getInstance();
$typeInt = $fieldTypesCollection->getFieldTypeByDataType('string');
$typeDate = $fieldTypesCollection->getFieldTypeByDataType('date');
$typeOpt = $fieldTypesCollection->getFieldTypeByDataType('relation');
$typeBool = $fieldTypesCollection->getFieldTypeByDataType('boolean');

$fieldTypeId = $fieldsCollection->addField('tovar', 'Товар', $typeInt->getId(), true, true);
$group->attachField($fieldTypeId);

$fieldTypeId = $fieldsCollection->addField('imya', 'Имя', $typeInt->getId(), true, true);
$group->attachField($fieldTypeId);

$fieldTypeId = $fieldsCollection->addField('telefon', 'Телефон', $typeInt->getId(), true, true);
$group->attachField($fieldTypeId);

$fieldTypeId = $fieldsCollection->addField('email', 'E-mail', $typeInt->getId(), true, true);
$group->attachField($fieldTypeId);

$fieldTypeId = $fieldsCollection->addField('adres', 'Адрес доставки', $typeInt->getId(), true, true);
$group->attachField($fieldTypeId);

/*$fieldTypeId = $fieldsCollection->addField('ydata', 'Дата', $typeInt->getId(), true, true);
$group->attachField($fieldTypeId);*/

/*$fieldTypeId = $fieldsCollection->addField('imgname', 'Изображение', $typeInt->getId(), true, true);
$group->attachField($fieldTypeId);*/

/*$fieldTypeId = $fieldsCollection->addField('usluga', 'Услуга', $typeInt->getId(), true, true);
$group->attachField($fieldTypeId);*/

$fieldTypeId = $fieldsCollection->addField('kommentarij', 'Комментарий', $typeInt->getId(), true, true);
$group->attachField($fieldTypeId);

$fieldTypeId = $fieldsCollection->addField('publish_time', 'Дата отправки', $typeDate->getId(), true, true);
$group->attachField($fieldTypeId);

/*$fieldTypeId = $fieldsCollection->addField('polzovatel', 'Пользователь', $typeOpt->getId(), true, true);
$group->attachField($fieldTypeId);*/

$fieldTypeId = $fieldsCollection->addField('zayavka_obrabotana', 'Заявка обработана?', $typeBool->getId(), true, true);
$group->attachField($fieldTypeId);

/*$fieldTypeId = $fieldsCollection->addField('pageid', 'Страница с которой пришел пользователь', $typeInt->getId(), true, true);
$group->attachField($fieldTypeId);*/



/*Создание типа данных для Группа страниц*/
$hierarchyTypes = umiHierarchyTypesCollection::getInstance();
$hierarchyType = $hierarchyTypes->getTypeByName('mobile', 'groupelements');
if(!$hierarchyType) {
    $type_id = $hierarchyTypes->addType('mobile', 'Группа страниц', 'groupelements');
} else {
    $type_id = $hierarchyType->getId();
}
$objectTypes = umiObjectTypesCollection::getInstance();
$objectTypeId = $objectTypes->getTypeByHierarchyTypeId($type_id);
if(!$objectTypeId) {
    $objectTypeId = $objectTypes->addType(0, 'Группа страниц');
    $objectType = $objectTypes->getType($objectTypeId);
    $objectType->setHierarchyTypeId($type_id);
    $objectType->setIsGuidable(true);
    $objectType->commit();
} else {
    $objectType = $objectTypes->getType($objectTypeId);
}
$group = $objectType->getFieldsGroupByName('osnovnye', true);
if(!$group) {
    $group_id = $objectType->addFieldsGroup('osnovnye', 'Основные', true, false);
    $group = $objectType->getFieldsGroupByName('osnovnye');
}
$fields = $group->getFields();

/*Создание типа данных для Мобильное приложение группа отзывов*/
$hierarchyTypes = umiHierarchyTypesCollection::getInstance();
$hierarchyType = $hierarchyTypes->getTypeByName('mobile', 'group_feedback');
if(!$hierarchyType) {
    $type_id = $hierarchyTypes->addType('mobile', 'Мобильное приложение группа отзывов', 'group_feedback');
} else {
    $type_id = $hierarchyType->getId();
}
$objectTypes = umiObjectTypesCollection::getInstance();
$objectTypeId = $objectTypes->getTypeByHierarchyTypeId($type_id);
if(!$objectTypeId) {
    $objectTypeId = $objectTypes->addType(0, 'Мобильное приложение группа отзывов');
    $objectType = $objectTypes->getType($objectTypeId);
    $objectType->setHierarchyTypeId($type_id);
    $objectType->setIsGuidable(true);
    $objectType->commit();
} else {
    $objectType = $objectTypes->getType($objectTypeId);
}
$group = $objectType->getFieldsGroupByName('osnovnye', true);
if(!$group) {
    $group_id = $objectType->addFieldsGroup('osnovnye', 'Основные', true, false);
    $group = $objectType->getFieldsGroupByName('osnovnye');
}
$fields = $group->getFields();


/*Создание типа данных для Обратная связь мобильное приложение*/
$hierarchyTypes = umiHierarchyTypesCollection::getInstance();
$hierarchyType = $hierarchyTypes->getTypeByName('mobile', 'item_element');
if(!$hierarchyType) {
    $type_id = $hierarchyTypes->addType('mobile', 'Новость или акция', 'item_element');
} else {
    $type_id = $hierarchyType->getId();
}
$objectTypes = umiObjectTypesCollection::getInstance();
$objectTypeId = $objectTypes->getTypeByHierarchyTypeId($type_id);
if(!$objectTypeId) {
    $objectTypeId = $objectTypes->addType(0, 'Новость или акция');
    $objectType = $objectTypes->getType($objectTypeId);
    $objectType->setHierarchyTypeId($type_id);
    $objectType->setIsGuidable(true);
    $objectType->commit();
} else {
    $objectType = $objectTypes->getType($objectTypeId);
}
$group = $objectType->getFieldsGroupByName('osnovnaya_gruppa', true);
if(!$group) {
    $group_id = $objectType->addFieldsGroup('osnovnaya_gruppa', 'Основная группа', true, false);
    $group = $objectType->getFieldsGroupByName('osnovnaya_gruppa');
}
$group2 = $objectType->getFieldsGroupByName('dopolnitelnye_parametry', true);
if(!$group2) {
    $group_id = $objectType->addFieldsGroup('dopolnitelnye_parametry', 'Дополнительные параметры', true, false);
    $group2 = $objectType->getFieldsGroupByName('dopolnitelnye_parametry');
}
$fields = $group->getFields();
$fieldDomain = false;
$fieldLang = false;

$fieldsCollection = umiFieldsCollection::getInstance();

$fieldTypesCollection = umiFieldTypesCollection::getInstance();
$typeTxt = $fieldTypesCollection->getFieldTypeByDataType('text');
$typeHtml = $fieldTypesCollection->getFieldTypeByDataType('wysiwyg');
$typeStr = $fieldTypesCollection->getFieldTypeByDataType('string');
$typeInt = $fieldTypesCollection->getFieldTypeByDataType('int');
$typeImg = $fieldTypesCollection->getFieldTypeByDataType('img_file');
$typeDate = $fieldTypesCollection->getFieldTypeByDataType('date');
$typeOpt = $fieldTypesCollection->getFieldTypeByDataType('relation');
$typeBool = $fieldTypesCollection->getFieldTypeByDataType('boolean');

$fieldTypeId = $fieldsCollection->addField('anons', 'Анонс', $typeTxt->getId(), true, true);
$group->attachField($fieldTypeId);

$fieldTypeId = $fieldsCollection->addField('kontent_dlya_mobilnogo', 'Контент', $typeHtml->getId(), true, true);
$group->attachField($fieldTypeId);

$fieldTypeId = $fieldsCollection->addField('anons_pic', 'Картинка', $typeImg->getId(), true, true);
$group2->attachField($fieldTypeId);

$fieldTypeId = $fieldsCollection->addField('end_time', 'Дата завершения скидки', $typeDate->getId(), true, true);
$group2->attachField($fieldTypeId);

$fieldTypeId = $fieldsCollection->addField('kolichestvo_kuponov', 'Сколько купонов осталось', $typeInt->getId(), true, true);
$group2->attachField($fieldTypeId);

$fieldTypeId = $fieldsCollection->addField('publish_time', 'Дата публикации (заполняется автоматически)', $typeDate->getId(), true, true);
$group2->attachField($fieldTypeId);


/*Создание типа данных для Мобильное приложение отзывы*/
$hierarchyTypes = umiHierarchyTypesCollection::getInstance();
$hierarchyType = $hierarchyTypes->getTypeByName('mobile', 'item_feedback');
if(!$hierarchyType) {
    $type_id = $hierarchyTypes->addType('mobile', 'Мобильное приложение отзывы', 'item_feedback');
} else {
    $type_id = $hierarchyType->getId();
}
$objectTypes = umiObjectTypesCollection::getInstance();
$objectTypeId = $objectTypes->getTypeByHierarchyTypeId($type_id);
if(!$objectTypeId) {
    $objectTypeId = $objectTypes->addType(0, 'Мобильное приложение отзывы');
    $objectType = $objectTypes->getType($objectTypeId);
    $objectType->setHierarchyTypeId($type_id);
    $objectType->setIsGuidable(true);
    $objectType->commit();
} else {
    $objectType = $objectTypes->getType($objectTypeId);
}
$group = $objectType->getFieldsGroupByName('osnovnaya_gruppa', true);
if(!$group) {
    $group_id = $objectType->addFieldsGroup('osnovnaya_gruppa', 'Основная группа', true, false);
    $group = $objectType->getFieldsGroupByName('osnovnaya_gruppa');
}
$group2 = $objectType->getFieldsGroupByName('dopolnitelnye_parametry', true);
if(!$group2) {
    $group_id = $objectType->addFieldsGroup('dopolnitelnye_parametry', 'Дополнительные параметры', true, false);
    $group2 = $objectType->getFieldsGroupByName('dopolnitelnye_parametry');
}
$fields = $group->getFields();
$fieldDomain = false;
$fieldLang = false;

$fieldsCollection = umiFieldsCollection::getInstance();

$fieldTypesCollection = umiFieldTypesCollection::getInstance();
$typeTxt = $fieldTypesCollection->getFieldTypeByDataType('text');
$typeHtml = $fieldTypesCollection->getFieldTypeByDataType('wysiwyg');
$typeStr = $fieldTypesCollection->getFieldTypeByDataType('string');
$typeInt = $fieldTypesCollection->getFieldTypeByDataType('int');
$typeImg = $fieldTypesCollection->getFieldTypeByDataType('img_file');
$typeDate = $fieldTypesCollection->getFieldTypeByDataType('date');
$typeOpt = $fieldTypesCollection->getFieldTypeByDataType('relation');
$typeBool = $fieldTypesCollection->getFieldTypeByDataType('boolean');

$fieldTypeId = $fieldsCollection->addField('otzyv', 'Отзыв', $typeTxt->getId(), true, true);
$group->attachField($fieldTypeId);

$fieldTypeId = $fieldsCollection->addField('usluga', 'Услуга', $typeStr->getId(), true, true);
$group2->attachField($fieldTypeId);

$fieldTypeId = $fieldsCollection->addField('publish_time', 'Дата публикации (заполняется автоматически)', $typeDate->getId(), true, true);
$group2->attachField($fieldTypeId);

$fieldTypeId = $fieldsCollection->addField('imya_otpravitelya', 'Имя отправителя', $typeStr->getId(), true, true);
$group2->attachField($fieldTypeId);

$fieldTypeId = $fieldsCollection->addField('polzovatel', 'Пользователь', $typeOpt->getId(), true, true);
$group2->attachField($fieldTypeId);





/*Устанавливаем права по умолчанию для гостя*/
$permissions = permissionsCollection::getInstance();
$guestId = $permissions->getGuestId();
if(!$permissions->isAllowedMethod($guestId, 'mobile', 'view')) {
    $permissions->setModulesPermissions($guestId, 'mobile', 'view');
}
?>

?>