<?php
 
class mobile extends def_module {
public $per_page;
 
public function __construct() {
parent::__construct();
 
if(cmsController::getInstance()->getCurrentMode() == "admin") {

$commonTabs = $this->getCommonTabs();
if($commonTabs) {
    $commonTabs->add('lists');
    $commonTabs->add('communication');
    $commonTabs->add('feedback');
}
 
$configTabs = $this->getConfigTabs();
if ($configTabs) {
$configTabs->add("config");
}
 
$this->__loadLib("__admin.php");
$this->__implement("__mobile");
 
} else {
 

}
 
}




    /* ------------------  Отображение отзыва  -----------------*/
    public function feedshow($parentElementId) {
        $hierarchyTypes = umiHierarchyTypesCollection::getInstance();
        $hierarchyType = $hierarchyTypes->getTypeByName("mobile", "item_feedback");
        $hierarchyTypeId = $hierarchyType->getId();

        $objectTypes = umiObjectTypesCollection::getInstance();
        $objectTypeId = $objectTypes->getTypeByHierarchyTypeId($hierarchyTypeId);

        $hierarchy = umiHierarchy::getInstance();

        //Создаем и подготавливаем выборку
        $sel = new umiSelection;
        $sel->addElementType($hierarchyTypeId); //Добавляет поиск по иерархическому типу
        $sel->addPermissions(); //Говорим, что обязательно нужно учитывать права доступа

        $sel->setOrderByObjectId(false);
        //Получаем результаты
        $result = umiSelectionsParser::runSelection($sel); //Массив id объектов
        $total = umiSelectionsParser::runSelectionCounts($sel); //Количество записей

        $datenow = date(U);

        //Выводим список  на экран
        if(($sz = sizeof($result)) > 0) {
            $block_arr = Array();
            $lines = Array();
            for($i = 00; $i < $sz; $i++) {
                $element_id = $result[$i];
                $element = umiHierarchy::getInstance()->getElement($element_id);
                if(!$element) continue;
                $line_arr = Array();
                $line_arr['attribute:id'] = $element_id;
                if($i < 10) {
                    $line_arr['i'] = '0'.$i;
                    $impr = '0'.$i;
                }
                else {
                    $line_arr['i'] = $i;
                    $impr = $i;
                }
                $line_arr['name'] = $element->getName();
                $line_arr['otzyv'] = $element->getValue('otzyv');
                $line_arr['usluga'] = $element->getValue('usluga');
                $line_arr['imya_otpravitelya'] = $element->getValue('imya_otpravitelya');
                $ptime = $element->getValue("publish_time");
                $formatedptime = $ptime->getFormattedDate("d.m.Y");
                $line_arr['publish_time'] = $formatedptime;

                $lines['nodes:item'][$impr] = $line_arr;
            }
            $block_arr['lines'] = $lines;


            return $block_arr;
        }
        return $block_arr;


    }

    /* ------------------  Голосование  -----------------*/
    public function voteplus($pElementId) {
		 
		 $hierarchy = umiHierarchy::getInstance();
		 $elementId = $pElementId;
		 
		 if($elementId === false) {
		  return "error";
		 }
		 
		 //Получим экземпляр страницы через ее id
		 $element = $hierarchy->getElement($elementId);
		 
		 //Проверим, что мы получили правильный результат
		 if($element instanceof umiHierarchyElement) {
		  //Изменим название и заголовок страницы
		  $rate_voters = (int) $element->getValue("rate_voters");
		  $rate_sum = $rate_voters + 1;
		  $element->setValue("rate_voters", $rate_sum);
		  $element->commit(); //Подтвердим внесенные изменения
		  return $rate_sum;
		}
    }

    public function voteminus($pElementId) {
		 
		 $hierarchy = umiHierarchy::getInstance();
		 $elementId = $pElementId;
		 
		 if($elementId === false) {
		  return "error";
		 }
		 
		 //Получим экземпляр страницы через ее id
		 $element = $hierarchy->getElement($elementId);
		 
		 //Проверим, что мы получили правильный результат
		 if($element instanceof umiHierarchyElement) {
		  //Изменим название и заголовок страницы
		  $rate_voters = (int) $element->getValue("rate_voters");
		  $rate_sum = $rate_voters - 1;
		  $element->setValue("rate_voters", $rate_sum);
		  $element->commit(); //Подтвердим внесенные изменения
		  return $rate_sum;
		}


    }



    /* ------------------  Отправка отзыва  -----------------*/
    public function sendfeed($parentElementId) {
        //Данные о сообщении
        $title = getRequest('title');
        $imya = getRequest('imya_otpravitelya');
        $email = getRequest('author_email');
        $usluga = getRequest('usluga');
        $kommentarij = getRequest('comment');
        $permissions = permissionsCollection::getInstance();
        $currentUserId = "31";
        $nowtime = date("U");


        $hierarchyTypes = umiHierarchyTypesCollection::getInstance();
        $hierarchyType = $hierarchyTypes->getTypeByName("mobile", "item_feedback");
        $hierarchyTypeId = $hierarchyType->getId();

        $hierarchy = umiHierarchy::getInstance();


        $newElementId = $hierarchy->addElement($parentElementId, $hierarchyTypeId, $title, $newElementAltName);
        if($newElementId === false) {
            return "Ne udalos sozdat stranicu";
        }

        //Установим права на страницу в состояние "по умолчанию"
        $permissions = permissionsCollection::getInstance();
        $permissions->setElementPermissions('31', $newElementId, '1');

        //Получим экземпляр страницы
        $newElement = $hierarchy->getElement($newElementId, true);

        if($newElement instanceof umiHierarchyElement) {
            $newElement->setValue("imya_otpravitelya", $imya);
            $newElement->setValue("usluga", $usluga);
            $newElement->setValue("otzyv", $kommentarij);
            $newElement->setValue("publish_time", $nowtime);
            $newElement->setValue("polzovatel", $currentUserId);

            //Укажем, что страница является активной
            $newElement->setIsActive(true);

            //Подтвердим внесенные изменения
            $newElement->commit();

            //Покажем адрес новой страницы
            return "Success";
        } else {
            return "Ne udalos poluchit ekzempliar stranici #{$newElementId}.";
        }



/*
        //куда отправлять сообщение
        $to = regedit::getInstance()->getVal("//modules/mobile/email");
        $from = 'noreaply@mobileapps.your';

        $headers = 'Content-type: text/html; charset=utf-8 ' . "\r\n";
        $headers .= 'From: Отзывы Мобильного приложения <noreaply@mobileapps.your>  ' . "\r\n";

        //Формируем сообщение

        //Отправляем сообщение
        mail($to, 'Оставлен новый отзыв с мобильного приложения ',
            'С мобильного приложения поступил новый заказ, данные заказа следующие: <br/> Имя: ' . $imya . '<br/> Услуга: ' . $usluga . ' <br/> Телефон: ' . $telefon . ' <br/> Удобная дата: ' . $ydata . '<br/> Комментарий: ' . $kommentarij . '<br/> Прикрепленное Изображение: ' . $host . '/upload/' . $imgname . '<br/> Промо страница:' . $promoname ,
            $headers);
        return('Сообщение отправлено');
*/
    }


    /* ------------------  Отправка сообщения в техподдержку  -----------------*/
    public function supportmessage() {
        //Данные о сообщении
        
        $imya = getRequest('imya');
        $company = getRequest('company');
        $dolj = getRequest('dolj'); 
        $email = getRequest('email');    
        $tema = getRequest('tema');
        $telefon = getRequest('telefon'); 
        $kommentarij = getRequest('kommentarij');

        $permissions = permissionsCollection::getInstance();
        $currentUserId = $permissions->getUserId();
        $nowtime = date("U");
        $collection = domainsCollection::getInstance(); // получаем экземпляр коллекции
        $domain = $collection->getDefaultDomain();
        $host = $domain->getHost();



        //куда отправлять сообщение
        $to = 'andrey@dpromo.su, m.mitrofanova@tmg-russia.ru, a.mitrofanova@tmg-russia.ru, jane.baranova@tmg-russia.ru, anna.smirnova@tmg-russia.ru, info@dpromo.su';
        $from = 'noreaply@mobileeapps.your';

        $headers = 'Content-type: text/html; charset=utf-8 ' . "\r\n";
        $headers .= 'From: Mobile Apps Dpromo <noreaply@mobileeapps.your>  ' . "\r\n";

        //Формируем сообщение

        //Отправляем сообщение
        mail($to, $tema,
            'С мобильного приложения поступил новый запрос, данные запроса следующие: <br/> Имя: ' . $imya . '<br/> Компания: ' . $company . ' <br/> Должность: ' . $dolj . ' <br/> Телефон: ' . $telefon . ' <br/> Email: ' . $email . ' <br/> Комментарий: ' . $kommentarij . '<br/> Домен: ' . $host ,
            $headers);
        return('Сообщение отправлено');
    }


    /* ------------------  Отправка сообщений  -----------------*/
    public function sendmessage() {
        //Данные о сообщении
        $telefon = getRequest('telefon');
        $imya = getRequest('imya');
        $adres = getRequest('adres');
        $email = getRequest('email');
        $kommentarij = getRequest('kommentarij');
        $tovar = getRequest('tovar');
        $permissions = permissionsCollection::getInstance();
        $currentUserId = $permissions->getUserId();
        $nowtime = date("U");
        $collection = domainsCollection::getInstance(); // получаем экземпляр коллекции
        $domain = $collection->getDefaultDomain();
        $host = $domain->getHost();


        $hierarchy = umiHierarchy::getInstance();
        $elementId = $pid;
        $element = $hierarchy->getElement($elementId);
        if($element instanceof umiHierarchyElement) {
            $promoname = $element->getName();
        }


        $objectTypes = umiObjectTypesCollection::getInstance();
        $mObjectTypeId = $objectTypes->getBaseType("mobile", "communi");
        $mname = "Заявка";
        $objects = umiObjectsCollection::getInstance();
        $new = $objects->addObject($mname, $mObjectTypeId);
        $newCommuni = $objects->getObject($new);

        if($newCommuni instanceof umiObject) {
            $newCommuni->setValue("imya", $imya);
            $newCommuni->setValue("telefon", $telefon);
            $newCommuni->setValue("adres", $adres);
            $newCommuni->setValue("email", $email);
            $newCommuni->setValue("kommentarij", $kommentarij);
            $newCommuni->setValue("publish_time", $nowtime);
            $newCommuni->setValue("tovar", $tovar);


            //Подтверждаем внесенные изменения
            $newCommuni->commit();

        }


        //куда отправлять сообщение
        $to = regedit::getInstance()->getVal("//modules/mobile/email");
        $from = 'noreaply@mobileapps.your';

        $headers = 'Content-type: text/html; charset=utf-8 ' . "\r\n";
        $headers .= 'From: Mamuas.ru <noreaply@mobileapps.your>  ' . "\r\n";

        //Формируем сообщение

        //Отправляем сообщение
        mail($to, 'Новая заявка с мобильного приложения ',
            'С мобильного приложения поступил новый заказ, данные заказа следующие: <br/> Имя: ' . $imya . '<br/> Телефон: ' . $telefon . ' <br/> email: ' . $email . ' <br/> Адрес: ' . $adres . '<br/> Комментарий: ' . $kommentarij . '<br/> Товар: <ul>' . $tovar . '</ul>' ,
            $headers);
        return('Сообщение отправлено');
    }

    /* ------------------  Menu  -----------------*/

    public function catitems($parentElementId, $width = '49', $height = '49'){
        $hierarchyTypes = umiHierarchyTypesCollection::getInstance();
        $hierarchyType = $hierarchyTypes->getTypeByName("catalog", "category");
        $hierarchyTypeId = $hierarchyType->getId();

        //Получаем базовый тип данных для новостей
        $objectTypes = umiObjectTypesCollection::getInstance();
        $objectTypeId = $objectTypes->getTypeByHierarchyTypeId($hierarchyTypeId);
        $objectType = $objectTypes->getType($objectTypeId);

        $hierarchy = umiHierarchy::getInstance();

        //Создаем и подготавливаем выборку
        $sel = new umiSelection;
        $sel->addElementType($hierarchyTypeId); //Добавляет поиск по иерархическому типу
        $sel->addHierarchyFilter($parentElementId); //Устанавливаем поиск по разделу
        $sel->addPermissions(); //Говорим, что обязательно нужно учитывать права доступа


        //Получаем результаты
        $result = umiSelectionsParser::runSelection($sel); //Массив id объектов
        $total = umiSelectionsParser::runSelectionCounts($sel); //Количество записей

        $datenow = date(U);

        //Выводим список  на экран
        if(($sz = sizeof($result)) > 0) {
            $block_arr = Array();
            $lines = Array();
            for($i = 0; $i < $sz; $i++) {
                $element_id = $result[$i];
                $element = umiHierarchy::getInstance()->getElement($element_id);
                if(!$element) continue;
                $line_arr = Array();
                $line_arr['attribute:id'] = $element_id;
                if($i < 10) {
                    $line_arr['i'] = '00'.$i;
                    $impr = '00'.$i;
                }
                else {
                    $line_arr['i'] = '0'.$i;
                    $impr = '0'.$i;
                }
                $line_arr['name'] = $element->getName();
                $elh = $element->getValue("header_pic");

                if ($elh) {
                    $imgsrc = $elh->getFilePath();
                    $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
                    $line_arr['thumb'] = $sourcthmb;
                }

                $pages = new selector('pages');
                $pages->types('hierarchy-type')->name('catalog', 'category');
                $pages->where('hierarchy')->page($element_id)->childs(1);
                $countcat = $pages->length;
                $resultp = $pages->result();
                $line_arr2 = Array();
                $line_arr3 = Array();
                if ($countcat > 0) {
                    for($k = 0; $k < $countcat; $k++) {
                        $line_arr2['names'] = $resultp[$k]->getName();
                        $newelid = $resultp[$k]->id;
                        $line_arr2['id'] = $newelid;
                        $pagesn = new selector('pages');
                        $pagesn->types('hierarchy-type')->name('catalog', 'category');
                        $pagesn->where('hierarchy')->page($newelid)->childs(1);
                        $countcatn = $pagesn->length;
                        if ($countcatn > 0) {
                        $line_arr2['moremenu'] = 1;
                        }
                        else {$line_arr2['moremenu'] = 0;}

                        if($k < 10) {
                            $nk = '0'.$k;
                        }
                        else {
                            $nk = $k;
                        }

                        $line_arr3['nodes:itemmenu'][$nk] = $line_arr2;
                    }
                $line_arr['nodes:menu'][$impr] = $line_arr3;    
                }
                else {
                 $line_arr['nodes:nomenu'][$impr] = $line_arr3; 
                }
                
                $lines['nodes:item'][$impr] = $line_arr;
            }
            $block_arr['lines'] = $lines;
            $block_arr['numpages'] = umiPagenum::generateNumPage($total, $per_page);
            $block_arr['total'] = $total;
            $block_arr['per_page'] = $per_page;
            $block_arr['category_id'] = $category_id;

            if($type_id) {
                $block_arr['type_id'] = $type_id;
            }
            return $block_arr;
        } else {
            $block_arr['numpages'] = umiPagenum::generateNumPage(0, 0);
            $block_arr['lines'] = "";
            $block_arr['total'] = 0;
            $block_arr['per_page'] = 0;
            $block_arr['category_id'] = $category_id;
        }
        return $block_arr;

    }
 
    /* ------------------  Работа со страницами  -----------------*/

    public function pageitems($parentElementId, $curpage, $width = 'auto', $height = '250'){
        $perpage = 50;
        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('content');
        $pages->limit($curpage, $perpage);
        $pages->where('hierarchy')->page($parentElementId)->childs(3);
        $pages->order(ord);
        $countcat = $pages->length;
        if ($countcat < $perpage) 
        {
            $rescount = $countcat;
        }
        else if($perpage*$curpage > $countcat)
        {
            $rescount = $countcat - ($perpage*($curpage));
        }
        else {
            $rescount = $perpage;
        }
        $resultp = $pages->result();
        $line_arr2 = Array();
        $line_arr3 = Array();
        $objects = umiObjectsCollection::getInstance();
        for($k = 0; $k < $rescount; $k++) {
           

                $line_arr2['names'] = $resultp[$k]->name;
                $line_arr2['id'] = $resultp[$k]->id;
                $line_arr2['description'] = $resultp[$k]->getValue("content");
                $line_arr2['vremya'] = $resultp[$k]->getValue("vremya");

                $line_arr2['fio'] = $resultp[$k]->getValue("fio");
                $line_arr2['dolzhnost'] = $resultp[$k]->getValue("dolzhnost");
                $line_arr2['informaciya_o_spikere'] = $resultp[$k]->getValue("informaciya_o_spikere");
                $elh = $resultp[$k]->getValue("fotografiya_spikera");
                $elh2 = $resultp[$k]->getValue("logotip_kompanii");

                if ($elh) {
                    $imgsrc = $elh->getFilePath();
                    $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
                    $line_arr2['fotografiya_spikera'] = $sourcthmb;
                } else {

                    $line_arr2['fotografiya_spikera'] = "noimage";
                }

                if ($elh2) {
                    $imgsrc = $elh2->getFilePath();
                    $sourcthmb = $this->makeThumbnailFullmy($imgsrc, auto, 100);
                    $line_arr2['logotip_kompanii'] = $sourcthmb;
                } else {

                    $line_arr2['logotip_kompanii'] = null;
                }

                if ($k < 10) {
                    $nk = '00' . $k;
                } else {
                    $nk = '0' . $k;
                }
                $line_arr2['i'] = $nk;
                $lines['nodes:item'][$nk] = $line_arr2;

        }

        $block_arr['lines'] = $lines;
        $block_arr['numpages'] = $curpage;
        $block_arr['total'] = $countcat;

        return $block_arr;

    }

    public function partners($parentElementId, $curpage, $width = 'auto', $height = '250'){
        $perpage = 50;
        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('content');
        $pages->limit($curpage, $perpage);
        $pages->where('hierarchy')->page($parentElementId)->childs(2);
        $pages->order(ord);
        $countcat = $pages->length;
        if ($countcat < $perpage) 
        {
            $rescount = $countcat;
        }
        else if($perpage*$curpage > $countcat)
        {
            $rescount = $countcat - ($perpage*($curpage));
        }
        else {
            $rescount = $perpage;
        }
        $resultp = $pages->result();
        $line_arr2 = Array();
        $line_arr3 = Array();
        $objects = umiObjectsCollection::getInstance();
        for($k = 0; $k < $rescount; $k++) {
           

                $line_arr2['names'] = $resultp[$k]->name;
                $line_arr2['id'] = $resultp[$k]->id;
                $line_arr2['description'] = $resultp[$k]->getValue("content");
                $line_arr2['url'] = $resultp[$k]->getValue("url");
                $elh = $resultp[$k]->getValue("izobr");
                $elh2 = $resultp[$k]->getValue("logotip_kompanii");

                if ($elh) {
                    $imgsrc = $elh->getFilePath();
                    $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
                    $line_arr2['fotografiya_spikera'] = $sourcthmb;
                } else {

                    $line_arr2['fotografiya_spikera'] = "noimage";
                }

                if ($elh2) {
                    $imgsrc = $elh2->getFilePath();
                    $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
                    $line_arr2['logotip_kompanii'] = $sourcthmb;
                } else {

                    $line_arr2['logotip_kompanii'] = "noimage";
                }

                if ($k < 10) {
                    $nk = '00' . $k;
                } else {
                    $nk = '0' . $k;
                }
                $line_arr2['i'] = $nk;
                $lines['nodes:item'][$nk] = $line_arr2;

        }

        $block_arr['lines'] = $lines;
        $block_arr['numpages'] = $curpage;
        $block_arr['total'] = $countcat;

        return $block_arr;

    }

    public function places($parentElementId, $curpage, $width = 'auto', $height = '250'){
        $perpage = 50;
        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('content');
        $pages->limit($curpage, $perpage);
        $pages->where('hierarchy')->page($parentElementId)->childs(3);
        $countcat = $pages->length;
        if ($countcat < $perpage) 
        {
            $rescount = $countcat;
        }
        else if($perpage*$curpage > $countcat)
        {
            $rescount = $countcat - ($perpage*($curpage));
        }
        else {
            $rescount = $perpage;
        }
        $resultp = $pages->result();
        $line_arr2 = Array();
        $line_arr3 = Array();
        $objects = umiObjectsCollection::getInstance();
        for($k = 0; $k < $rescount; $k++) {
           

                $line_arr2['names'] = $resultp[$k]->name;
                $line_arr2['id'] = $resultp[$k]->id;
                $line_arr2['opcii_predlozheniya'] = $resultp[$k]->getValue("opcii_predlozheniya");
                $line_arr2['shirota'] = $resultp[$k]->getValue("shirota");
                $line_arr2['dolgota'] = $resultp[$k]->getValue("dolgota");
                $line_arr2['adres'] = $resultp[$k]->getValue("adres");
                $line_arr2['content'] = $resultp[$k]->getValue("content");
                $line_arr2['desti'] = "0";

                if ($k < 10) {
                    $nk = '0' . $k;
                } else {
                    $nk = $k;
                }
                $line_arr2['i'] = $nk;
                $lines['nodes:item'][$nk] = $line_arr2;

        }

        $block_arr['lines'] = $lines;
        $block_arr['numpages'] = $curpage;
        $block_arr['total'] = $countcat;

        return $block_arr;

    }

     public function newsitm($parentElementId, $curpage, $width = 'auto', $height = '250'){
        $perpage = 50;
        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('news', 'item');
        $pages->limit($curpage, $perpage);
        $pages->where('hierarchy')->page($parentElementId)->childs(3);
        $pages->order(ord);
        $countcat = $pages->length; 
        if ($countcat < $perpage) 
        {
            $rescount = $countcat;
        }
        else if($perpage*$curpage > $countcat)
        {
            $rescount = $countcat - ($perpage*($curpage));
        }
        else {
            $rescount = $perpage;
        }
        $resultp = $pages->result();
        $line_arr2 = Array();
        $line_arr3 = Array();
        $objects = umiObjectsCollection::getInstance();
        for($k = 0; $k < $rescount; $k++) {
           

                $line_arr2['names'] = $resultp[$k]->name;
                $line_arr2['id'] = $resultp[$k]->id;
                $line_arr2['description'] = $resultp[$k]->getValue("anons");
                $line_arr2['vremya'] = $resultp[$k]->getValue("publish_time");

                if ($elh) {
                    $imgsrc = $elh->getFilePath();
                    $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
                    $line_arr2['fotografiya_spikera'] = $sourcthmb;
                } else {

                    $line_arr2['fotografiya_spikera'] = "noimage";
                }

                if ($elh2) {
                    $imgsrc = $elh2->getFilePath();
                    $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
                    $line_arr2['logotip_kompanii'] = $sourcthmb;
                } else {

                    $line_arr2['logotip_kompanii'] = "noimage";
                }

                if ($k < 10) {
                    $nk = '0' . $k;
                } else {
                    $nk = $k;
                }
                $line_arr2['i'] = $nk;
                $lines['nodes:item'][$nk] = $line_arr2;

        }

        $block_arr['lines'] = $lines;
        $block_arr['numpages'] = $curpage;
        $block_arr['total'] = $countcat;

        return $block_arr;

    }   

    public function newitems($parentElementId, $curpage='0', $width = '150', $height = '150', $where = 'news'){
        $perpage = 12;
        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('catalog', 'object');
        $pages->limit($curpage, $perpage);
        $pages->where('hierarchy')->page($parentElementId)->childs(3);
        if ($where == 'news') {
            $pages->order('publish_time')->desc();
        }
        else if ($where == 'lider') {
            $pages->where('lidery_prodazh')->equals(1);
        }
        else if ($where == 'skidki') {
            $pages->where('skidki')->equals(1);
        }
        else if ($where == 'vibor') {
            $pages->where('vybor_redakcii')->equals(1);
        }

        $countcat = $pages->length;
        if ($countcat < $perpage) 
        {
            $rescount = $countcat;
        }
        else if($perpage*$curpage > $countcat)
        {
            $rescount = $countcat - ($perpage*($curpage));
        }
        else {
            $rescount = $perpage;
        }
        $resultp = $pages->result();
        $line_arr2 = Array();
        $line_arr3 = Array();
        $objects = umiObjectsCollection::getInstance();
        for($k = 0; $k < $rescount; $k++) {
            $elh = $resultp[$k]->getValue("photo");

            

                $line_arr2['name'] = $resultp[$k]->getValue("nazvanie_knigi");
                $line_arr2['id'] = $resultp[$k]->id;

                $avtor = $resultp[$k]->getValue("avtor");
                $avtorObject = $objects->getObject($avtor);
                $line_arr2['avtor'] = $avtorObject->name;

                $chtec = $resultp[$k]->getValue("chtec");
                $chtecObject = $objects->getObject($chtec);
                $line_arr2['chtec'] = $chtecObject->name;

                $izdatel = $resultp[$k]->getValue("izdatel");
                $izdatelObject = $objects->getObject($izdatel);
                $line_arr2['izdatel'] = $izdatelObject->name; 
                
                $line_arr2['price'] = $resultp[$k]->getValue("price");              
                 
                if ($elh) {
                    $imgsrc = $elh->getFilePath();
                    $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
                    $line_arr2['thumb'] = $sourcthmb;
                } else {

                    $line_arr2['thumb'] = "/images/nofoto_260_auto_5_80.jpg";
                }
                if ($k < 10) {
                    $nk = $k;
                } else {
                    $nk = $k;
                }

                $lines['nodes:item'][$nk] = $line_arr2;

        }

        $block_arr['lines'] = $lines;
        $block_arr['numpages'] = $curpage;
        $block_arr['total'] = $countcat;

        return $block_arr;

    }

    public function lideritems($parentElementId, $curpage='0', $width = '150', $height = '150'){
        $perpage = 12;
        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('catalog', 'object');
        $pages->limit($curpage, $perpage);
        $pages->where('hierarchy')->page($parentElementId)->childs(3);
        $pages->where('lidery_prodazh')->equals(1);

        $countcat = $pages->length;
        if ($countcat < $perpage) 
        {
            $rescount = $countcat;
        }
        else if($perpage*$curpage > $countcat)
        {
            $rescount = $countcat - ($perpage*($curpage));
        }
        else {
            $rescount = $perpage;
        }
        $resultp = $pages->result();
        $line_arr2 = Array();
        $line_arr3 = Array();
        $objects = umiObjectsCollection::getInstance();
        for($k = 0; $k < $rescount; $k++) {
            $elh = $resultp[$k]->getValue("photo");

                $line_arr2['name'] = $resultp[$k]->getValue("nazvanie_knigi");
                $line_arr2['id'] = $resultp[$k]->id;
                 
                if ($elh) {
                    $imgsrc = $elh->getFilePath();
                    $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
                    $line_arr2['thumb'] = $sourcthmb;
                } else {

                    $line_arr2['thumb'] = "/images/nofoto_260_auto_5_80.jpg";
                }
                if ($k < 10) {
                    $nk = $k;
                } else {
                    $nk = $k;
                }

                $lines['nodes:item'][$nk] = $line_arr2;

        }

        $block_arr['lines'] = $lines;
        $block_arr['numpages'] = $curpage;
        $block_arr['total'] = $countcat;

        return $block_arr;

    }

    public function skidki($parentElementId, $curpage='0', $width = '150', $height = '150'){
        $perpage = 12;
        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('catalog', 'object');
        $pages->limit($curpage, $perpage);
        $pages->where('hierarchy')->page($parentElementId)->childs(3);
        $pages->where('skidki')->equals(1);

        $countcat = $pages->length;
        if ($countcat < $perpage) 
        {
            $rescount = $countcat;
        }
        else if($perpage*$curpage > $countcat)
        {
            $rescount = $countcat - ($perpage*($curpage));
        }
        else {
            $rescount = $perpage;
        }
        $resultp = $pages->result();
        $line_arr2 = Array();
        $line_arr3 = Array();
        $objects = umiObjectsCollection::getInstance();
        for($k = 0; $k < $rescount; $k++) {
            $elh = $resultp[$k]->getValue("photo");

                $line_arr2['name'] = $resultp[$k]->getValue("nazvanie_knigi");
                $line_arr2['id'] = $resultp[$k]->id;
                 
                if ($elh) {
                    $imgsrc = $elh->getFilePath();
                    $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
                    $line_arr2['thumb'] = $sourcthmb;
                } else {

                    $line_arr2['thumb'] = "/images/nofoto_260_auto_5_80.jpg";
                }
                if ($k < 10) {
                    $nk = $k;
                } else {
                    $nk = $k;
                }

                $lines['nodes:item'][$nk] = $line_arr2;

        }

        $block_arr['lines'] = $lines;
        $block_arr['numpages'] = $curpage;
        $block_arr['total'] = $countcat;

        return $block_arr;

    }

    public function soon($parentElementId, $curpage='0', $width = '150', $height = '150'){
        $perpage = 12;
        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('catalog', 'object');
        $pages->limit($curpage, $perpage);
        $pages->where('hierarchy')->page($parentElementId)->childs(3);

        $countcat = $pages->length;
        if ($countcat < $perpage) 
        {
            $rescount = $countcat;
        }
        else if($perpage*$curpage > $countcat)
        {
            $rescount = $countcat - ($perpage*($curpage));
        }
        else {
            $rescount = $perpage;
        }
        $resultp = $pages->result();
        $line_arr2 = Array();
        $line_arr3 = Array();
        $objects = umiObjectsCollection::getInstance();
        for($k = 0; $k < $rescount; $k++) {
            $elh = $resultp[$k]->getValue("photo");

                $line_arr2['name'] = $resultp[$k]->getValue("nazvanie_knigi");
                $line_arr2['id'] = $resultp[$k]->id;

                $avtor = $resultp[$k]->getValue("avtor");
                $avtorObject = $objects->getObject($avtor);
                $line_arr2['avtor'] = $avtorObject->name;

                $chtec = $resultp[$k]->getValue("chtec");
                $chtecObject = $objects->getObject($chtec);
                $line_arr2['chtec'] = $chtecObject->name;

                $izdatel = $resultp[$k]->getValue("izdatel");
                $izdatelObject = $objects->getObject($izdatel);
                $line_arr2['izdatel'] = $izdatelObject->name;

                $line_arr2['price'] = $resultp[$k]->getValue("price");
                 
                if ($elh) {
                    $imgsrc = $elh->getFilePath();
                    $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
                    $line_arr2['thumb'] = $sourcthmb;
                } else {

                    $line_arr2['thumb'] = "/images/nofoto_260_auto_5_80.jpg";
                }
                if ($k < 10) {
                    $nk = $k;
                } else {
                    $nk = $k;
                }

                $lines['nodes:item'][$nk] = $line_arr2;

        }

        $block_arr['lines'] = $lines;
        $block_arr['numpages'] = $curpage;
        $block_arr['total'] = $countcat;

        return $block_arr;

    }    

    public function rchoise($parentElementId, $curpage='0', $width = '150', $height = '150'){
        $perpage = 12;
        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('catalog', 'object');
        $pages->limit($curpage, $perpage);
        $pages->where('hierarchy')->page($parentElementId)->childs(3);
        $pages->where('vybor_redakcii')->equals(1);

        $countcat = $pages->length;
        if ($countcat < $perpage) 
        {
            $rescount = $countcat;
        }
        else if($perpage*$curpage > $countcat)
        {
            $rescount = $countcat - ($perpage*($curpage));
        }
        else {
            $rescount = $perpage;
        }
        $resultp = $pages->result();
        $line_arr2 = Array();
        $line_arr3 = Array();
        $objects = umiObjectsCollection::getInstance();
        for($k = 0; $k < $rescount; $k++) {
            $elh = $resultp[$k]->getValue("photo");

                $line_arr2['name'] = $resultp[$k]->getValue("nazvanie_knigi");
                $line_arr2['id'] = $resultp[$k]->id;
                 
                if ($elh) {
                    $imgsrc = $elh->getFilePath();
                    $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
                    $line_arr2['thumb'] = $sourcthmb;
                } else {

                    $line_arr2['thumb'] = "/images/nofoto_260_auto_5_80.jpg";
                }
                if ($k < 10) {
                    $nk = $k;
                } else {
                    $nk = $k;
                }

                $lines['nodes:item'][$nk] = $line_arr2;

        }

        $block_arr['lines'] = $lines;
        $block_arr['numpages'] = $curpage;
        $block_arr['total'] = $countcat;

        return $block_arr;

    }

    public function rselect($parentElementId, $curpage='0', $width = '150', $height = '150', $tip, $znach){
        $perpage = 12;
        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('catalog', 'object');
        $pages->limit($curpage, $perpage);
        $pages->where('hierarchy')->page($parentElementId)->childs(3);
        $pages->where($tip)->equals($znach);

        $countcat = $pages->length;
        if ($countcat < $perpage) 
        {
            $rescount = $countcat;
        }
        else if($perpage*$curpage > $countcat)
        {
            $rescount = $countcat - ($perpage*($curpage));
        }
        else {
            $rescount = $perpage;
        }
        $resultp = $pages->result();
        $line_arr2 = Array();
        $line_arr3 = Array();
        $objects = umiObjectsCollection::getInstance();
        for($k = 0; $k < $rescount; $k++) {
            $elh = $resultp[$k]->getValue("photo");

                $line_arr2['name'] = $resultp[$k]->getValue("nazvanie_knigi");
                $line_arr2['id'] = $resultp[$k]->id;

                $avtor = $resultp[$k]->getValue("avtor");
                $avtorObject = $objects->getObject($avtor);
                $line_arr2['avtor'] = $avtorObject->name;

                $chtec = $resultp[$k]->getValue("chtec");
                $chtecObject = $objects->getObject($chtec);
                $line_arr2['chtec'] = $chtecObject->name;

                $izdatel = $resultp[$k]->getValue("izdatel");
                $izdatelObject = $objects->getObject($izdatel);
                $line_arr2['izdatel'] = $izdatelObject->name;

                $line_arr2['price'] = $resultp[$k]->getValue("price");
                 
                if ($elh) {
                    $imgsrc = $elh->getFilePath();
                    $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
                    $line_arr2['thumb'] = $sourcthmb;
                } else {

                    $line_arr2['thumb'] = "/images/nofoto_260_auto_5_80.jpg";
                }
                if ($k < 10) {
                    $nk = $k;
                } else {
                    $nk = $k;
                }

                $lines['nodes:item'][$nk] = $line_arr2;

        }

        $block_arr['lines'] = $lines;
        $block_arr['numpages'] = $curpage;
        $block_arr['total'] = $countcat;

        return $block_arr;

    }

   public function pageitemsmin($parentElementId, $width = '49', $height = '49'){
        $hierarchyTypes = umiHierarchyTypesCollection::getInstance();
        $hierarchyType = $hierarchyTypes->getTypeByName("catalog", "category");
        $hierarchyTypeId = $hierarchyType->getId();

        //Получаем базовый тип данных для новостей
        $objectTypes = umiObjectTypesCollection::getInstance();
        $objectTypeId = $objectTypes->getTypeByHierarchyTypeId($hierarchyTypeId);
        $objectType = $objectTypes->getType($objectTypeId);

        $hierarchy = umiHierarchy::getInstance();

        //Создаем и подготавливаем выборку
        $sel = new umiSelection;
        $sel->addElementType($hierarchyTypeId); //Добавляет поиск по иерархическому типу
        $sel->addHierarchyFilter($parentElementId); //Устанавливаем поиск по разделу
        $sel->addPermissions(); //Говорим, что обязательно нужно учитывать права доступа


        //Получаем результаты
        $result = umiSelectionsParser::runSelection($sel); //Массив id объектов
        $total = umiSelectionsParser::runSelectionCounts($sel); //Количество записей

        $datenow = date(U);

        //Выводим список  на экран
        if(($sz = sizeof($result)) > 0) {
            $block_arr = Array();
            $lines = Array();
            for($i = 0; $i < $sz; $i++) {
                $element_id = $result[$i];
                $element = umiHierarchy::getInstance()->getElement($element_id);
                if(!$element) continue;
                $line_arr = Array();
                $line_arr['attribute:id'] = $element_id;
                if($i < 10) {
                    $line_arr['i'] = $i;
                    $impr = $i;
                }
                else {
                    $line_arr['i'] = 'n'.$i;
                    $impr = 'n'.$i;
                }
                $line_arr['name'] = $element->getName();
                $elh = $element->getValue("header_pic");

                if ($elh) {
                    $imgsrc = $elh->getFilePath();
                    $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
                    $line_arr['thumb'] = $sourcthmb;
                }

                $pages = new selector('pages');
                $pages->types('hierarchy-type')->name('catalog', 'category');
                $pages->where('hierarchy')->page($element_id)->childs(1);
                $countcat = $pages->length;
                $resultp = $pages->result();
                $line_arr2 = Array();
                $line_arr3 = Array();
                if ($countcat > 0) {
                    for($k = 0; $k < $countcat; $k++) {
                        $line_arr2['names'] = $resultp[$k]->getName();
                        $newelid = $resultp[$k]->id;
                        $line_arr2['id'] = $newelid;
                        $pagesn = new selector('pages');
                        $pagesn->types('hierarchy-type')->name('catalog', 'category');
                        $pagesn->where('hierarchy')->page($newelid)->childs(1);
                        $countcatn = $pagesn->length;
                        if ($countcatn > 0) {
                        $line_arr2['moremenu'] = 1;
                        }
                        else {$line_arr2['moremenu'] = 0;}

                        if($k < 10) {
                            $nk = '0'.$k;
                        }
                        else {
                            $nk = $k;
                        }

                        $line_arr3['nodes:itemmenu'][$nk] = $line_arr2;
                    }
                $line_arr['nodes:menu'][$impr] = $line_arr3;    
                }
                else {
                 $line_arr['nodes:nomenu'][$impr] = $line_arr3; 
                }
                
                $lines['nodes:item'][$impr] = $line_arr;
            }
            $block_arr['lines'] = $lines;
            $block_arr['numpages'] = umiPagenum::generateNumPage($total, $per_page);
            $block_arr['total'] = $total;
            $block_arr['per_page'] = $per_page;
            $block_arr['category_id'] = $category_id;

            if($type_id) {
                $block_arr['type_id'] = $type_id;
            }
            return $block_arr;
        } else {
            $block_arr['numpages'] = umiPagenum::generateNumPage(0, 0);
            $block_arr['lines'] = "";
            $block_arr['total'] = 0;
            $block_arr['per_page'] = 0;
            $block_arr['category_id'] = $category_id;
        }
        return $block_arr;

    }


        public function banners($parentElementId, $curpage, $width = '618', $height = '273'){
        $perpage = 20;
        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('content');
        $pages->limit($curpage, $perpage);
        $pages->where('hierarchy')->page($parentElementId)->childs(3);
        $countcat = $pages->length;
        if ($countcat < $perpage) 
        {
            $rescount = $countcat;
        }
        else if($perpage*$curpage > $countcat)
        {
            $rescount = $countcat - ($perpage*($curpage));
        }
        else {
            $rescount = $perpage;
        }
        $resultp = $pages->result();
        $line_arr2 = Array();
        $line_arr3 = Array();
        for($k = 0; $k < $rescount; $k++) {
            $elh = $resultp[$k]->getValue("image");

                $line_arr2['names'] = $resultp[$k]->getName();
                $line_arr2['id'] = $resultp[$k]->id;
                $nalichie = $resultp[$k]->getValue('est_v_nalichii');                   
                if ($elh) {
                    $imgsrc = $elh->getFilePath();
                    $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
                    $line_arr2['thumb'] = $sourcthmb;
                } else {

                    $line_arr2['thumb'] = "/images/nofoto_260_auto_5_80.jpg";
                }
                if ($k < 10) {
                    $nk = '0' . $k;
                } else {
                    $nk = $k;
                }

                $lines['nodes:item'][$nk] = $line_arr2;

        }

        $block_arr['lines'] = $lines;
        $block_arr['numpages'] = $curpage;
        $block_arr['total'] = $countcat;

        return $block_arr;

    }


        public function brandmoreitems($parentElementId, $brand, $curpage, $width = '350', $height = '350'){
        $perpage = 50;
        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('catalog', 'object');
        $pages->where('manufacturer')->equals(array('int' => $brand));
        $pages->limit($curpage, $perpage);
        $pages->where('hierarchy')->page($parentElementId)->childs(3);
        $countcat = $pages->length;
        if ($countcat < $perpage) 
        {
            $rescount = $countcat;
        }
        else if($perpage*$curpage > $countcat)
        {
            $rescount = $countcat - ($perpage*($curpage));
        }
        else {
            $rescount = $perpage;
        }
        $resultp = $pages->result();
        $line_arr2 = Array();
        $line_arr3 = Array();
        for($k = 0; $k < $rescount; $k++) {
            $line_arr2['names'] = $resultp[$k]->getName();
            $line_arr2['id'] = $resultp[$k]->id;
            $line_arr2['price'] = $resultp[$k]->getValue("price");
            $elh = $resultp[$k]->getValue("photo");
            if ($elh) {
                $imgsrc = $elh->getFilePath();
                $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
                $line_arr2['thumb'] = $sourcthmb;
            }
            else {
                $line_arr2['thumb'] = "/images/nofoto_260_auto_5_80.jpg";
            }
            if($k < 10) {
                $nk = '0'.$k;
            }
            else {
                $nk = $k;
            }

            $lines['nodes:item'][$nk] = $line_arr2;
        }

        $block_arr['lines'] = $lines;
        $block_arr['numpages'] = $curpage;
        $block_arr['total'] = $countcat;

        return $block_arr;

    }


    public function branditems($parentElementId, $brand, $width = '350', $height = '350'){
        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('catalog', 'object');
        $pages->where('hierarchy')->page($parentElementId)->childs(6);
        $pages->where('manufacturer')->equals(array('int' => $brand));

        $countcat = $pages->length;
        $resultp = $pages->result();

        foreach($pages as $page) 
        {
            $item_arr = Array();
            $firstb = Array();
            $id_parent = $page->getParentId();
            $items[] = $item_arr;
            $firstbs[] = $id_parent;
        }

        $firstbs_array = array_unique($firstbs);

            //«аписываем в массив буквы
            $itembs_array = Array();
            foreach($firstbs_array as $itembs) {
                $itembs_arr = Array();
                $item_arr['attribute:id'] = $itembs;
                $hierarchy = umiHierarchy::getInstance();
                $element_parent = $hierarchy->getElement($itembs);
                $item_arr['attribute:name'] = $element_parent->getName();
                $itembs_array[] = $item_arr;
            }
            
            $itogarray = Array("items" => Array('nodes:item' => $itembs_array));
            
            return $itogarray;

    }

        public function otzitems($parentElementId, $curpage, $width = '350', $height = '350'){
        $perpage = 50;
        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('comments', 'comment');
        $pages->limit($curpage, $perpage);
        $pages->where('hierarchy')->page($parentElementId)->childs(3);
        $pages->order('publish_time')->desc();
        $countcat = $pages->length;
        if ($countcat < $perpage) 
        {
            $rescount = $countcat;
        }
        else if($perpage*$curpage > $countcat)
        {
            $rescount = $countcat - ($perpage*($curpage));
        }
        else {
            $rescount = $perpage;
        }
        $resultp = $pages->result();
        $line_arr2 = Array();
        $line_arr3 = Array();
        for($k = 0; $k < $rescount; $k++) {
            $line_arr2['id'] = $resultp[$k]->id;
            $line_arr2['message'] = $resultp[$k]->getValue("message");
            $author_id = $resultp[$k]->getValue("author_id");
             $objects2 = umiObjectsCollection::getInstance();
             $userObject = $objects2->getObject($author_id);
             $line_arr2['author'] = $userObject->getValue("nickname");
             $line_arr2['publish_time'] = $resultp[$k]->getValue("publish_time");



            if($k < 10) {
                $nk = '0'.$k;
            }
            else {
                $nk = $k;
            }

            $lines['nodes:item'][$nk] = $line_arr2;
        }

        $block_arr['lines'] = $lines;
        $block_arr['numpages'] = $curpage;
        $block_arr['total'] = $countcat;

        return $block_arr;

    }



        public function postmobi($parent_element_id = false) {
            $bNeedFinalPanic = false;

            $parent_element_id = (int) $parent_element_id;
            if(!isset($parent_element_id) || !$parent_element_id) {
                $parent_element_id = (int) getRequest('param0');
            }

            $title = trim(getRequest('title'));
            $content = trim(getRequest('comment'));
            $nick = htmlspecialchars(getRequest('author_nick'));
            $email = htmlspecialchars(getRequest('author_email'));
            $usluga = htmlspecialchars(getRequest('usluga'));
            $imya_otpravitelya = htmlspecialchars(getRequest('imya_otpravitelya'));

            $referer_url = getServer('HTTP_REFERER');
            $posttime = time();
            $ip = getServer('REMOTE_ADDR');

            if(!$referer_url) {
                $referer_url = umiHierarchy::getInstance()->getPathById($parent_element_id);
            }
            $this->errorRegisterFailPage($referer_url);

            if (!(strlen($title) || strlen($content))) {
                $this->errorNewMessage('%comments_empty%', false);
                $this->errorPanic();
            }


            $user_id = cmsController::getInstance()->getModule('users')->user_id;

            if(!$nick) {
                $nick = getRequest('nick');
            }

            if(!$email) {
                $email = getRequest('email');
            }


            if($nick) {
                $nick = htmlspecialchars($nick);
            }

            if($email) {
                $email = htmlspecialchars($email);
            }

            $is_sv = false;
            if($users_inst = cmsController::getInstance()->getModule("users")) {
                if($users_inst->is_auth()) {
                    $author_id = $users_inst->createAuthorUser($user_id);
                    $is_sv = permissionsCollection::getInstance()->isSv($user_id);
                } else {
                    if(!(regedit::getInstance()->getVal("//modules/comments/allow_guest"))) {
                        $this->errorNewMessage('%comments_not_allowed_post%', true);
                    }

                    $author_id = $users_inst->createAuthorGuest($nick, $email, $ip);
                }
            }

            $is_active = ($this->moderated && !$is_sv) ? 0 : 1;

            if($is_active) {
                $is_active = antiSpamHelper::checkContent($content.$title.$nick.$email) ? 1 : 0;
            }

            if (!$is_active) {
                $this->errorNewMessage('%comments_posted_moderating%', false);
                $bNeedFinalPanic = true;
            }

            $object_type_id = umiObjectTypesCollection::getInstance()->getBaseType("comments", "comment");
            $hierarchy_type_id = umiHierarchyTypesCollection::getInstance()->getTypeByName("comments", "comment")->getId();

            $parentElement = umiHierarchy::getInstance()->getElement($parent_element_id);
            $tpl_id     = $parentElement->getTplId();
            $domain_id  = $parentElement->getDomainId();
            $lang_id    = $parentElement->getLangId();

            if (!strlen(trim($title)) && ($parentElement instanceof umiHierarchyElement)) {
                $title = "Re: ".$parentElement->getName();
            }

            $element_id = umiHierarchy::getInstance()->addElement($parent_element_id, $hierarchy_type_id, $title, $title, $object_type_id, $domain_id, $lang_id, $tpl_id);
            permissionsCollection::getInstance()->setDefaultPermissions($element_id);

            $element = umiHierarchy::getInstance()->getElement($element_id, true);

            $element->setIsActive($is_active);
            $element->setIsVisible(false);

            $element->setValue("message", $content);
            $element->setValue("publish_time", $posttime);
            $element->setValue("usluga", $usluga);
            $element->setValue("imya_otpravitelya", $imya_otpravitelya);

            $element->getObject()->setName($title);
            $element->setValue("h1", $title);

            $element->setValue("author_id", $author_id);

            $object_id = $element->getObject()->getId();
            $data_module = cmsController::getInstance()->getModule('data');
            $data_module->saveEditedObject($object_id, true);

            // moderate
            $element->commit();
            $parentElement->commit();

            $oEventPoint = new umiEventPoint("comments_message_post_do");
            $oEventPoint->setMode("after");
            $oEventPoint->setParam("topic_id", $parent_element_id);
            $oEventPoint->setParam("message_id", $element_id);
            $this->setEventPoint($oEventPoint);

            // redirect with or without error messages
            if ($bNeedFinalPanic) {
                $this->errorPanic();
            } else {
                // validate url
                $referer_url = preg_replace("/_err=\d+/is", '', $referer_url);
                while (strpos($referer_url, '&&') !== false || strpos($referer_url, '??') !== false || strpos($referer_url, '?&') !== false) {
                    $referer_url = str_replace('&&', '&', $referer_url);
                    $referer_url = str_replace('??', '?', $referer_url);
                    $referer_url = str_replace('?&', '?', $referer_url);
                }
                if (strlen($referer_url) && (substr($referer_url, -1) === '?' || substr($referer_url, -1) === '&')) $referer_url = substr($referer_url, 0, strlen($referer_url)-1);

                return 'Success';

            }
        }






    //Метод upage
    public function upage($elementId, $width = '600', $height = 'auto'){
        $hierarchy = umiHierarchy::getInstance();
        $element = $hierarchy->getElement($elementId);
        $line_arr = Array();
        $line_arr2 = Array();
        $line_arr['id'] = $elementId;
        $line_arr['name'] = $element->getName();
        $line_arr['content'] = $element->getValue("content");
        $line_arr['anons'] = $element->getValue("anons");
        $line_arr['vremya'] = $element->getValue("publish_time");

        $elh = $element->getValue("photo");
        if ($elh) {
            $imgsrc = $elh->getFilePath();
            $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
            $line_arr['thumbs'] = $sourcthmb;
        }
        else {
            $line_arr['thumbs'] = "/images/cms/thumbs/f8c15f3a9a4ad9fe97f7d162357d4696012859f3/nofoto_260_auto_5_80.jpg";
        }


//        $elh = $element->getValue("photo_2");
 //       if ($elh) {
 //           $imgsrc = $elh->getFilePath();
 //           $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
 //           $line_arr['nodes:thumbs'][] = $sourcthmb;
 //       }

 //       $elh = $element->getValue("photo_3");
 //       if ($elh) {
 //           $imgsrc = $elh->getFilePath();
 //           $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
 //           $line_arr['nodes:thumbs'][] = $sourcthmb;
 //       }

 //       $elh = $element->getValue("photo_4");
 //       if ($elh) {
 //           $imgsrc = $elh->getFilePath();
 //           $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
 //          $line_arr['nodes:thumbs'][] = $sourcthmb;
 //       }

        return $line_arr;

    }


    public function npage($elementId, $width = '500', $height = '300'){
        $hierarchy = umiHierarchy::getInstance();
        $element = $hierarchy->getElement($elementId);
        $line_arr = Array();
        $line_arr2 = Array();
        $line_arr['id'] = $elementId;
        $line_arr['name'] = $element->getName();
        $line_arr['content'] = $element->getValue('content');
        $elh = $element->getValue("publish_pic");
        if ($elh) {
            $imgsrc = $elh->getFilePath();
            $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
            $line_arr['thumbs'] = $sourcthmb;
        }



//        $elh = $element->getValue("photo_2");
 //       if ($elh) {
 //           $imgsrc = $elh->getFilePath();
 //           $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
 //           $line_arr['nodes:thumbs'][] = $sourcthmb;
 //       }

 //       $elh = $element->getValue("photo_3");
 //       if ($elh) {
 //           $imgsrc = $elh->getFilePath();
 //           $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
 //           $line_arr['nodes:thumbs'][] = $sourcthmb;
 //       }

 //       $elh = $element->getValue("photo_4");
 //       if ($elh) {
 //           $imgsrc = $elh->getFilePath();
 //           $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
 //          $line_arr['nodes:thumbs'][] = $sourcthmb;
 //       }

        return $line_arr;

    }    

    public function custupage($elementId){
        $hierarchy = umiHierarchy::getInstance();
        $element = $hierarchy->getElement($elementId);
        $line_arr = Array();
        $line_arr['id'] = $elementId;

        $kolvokup = $element->getValue('kolichestvo_kuponov');


        if($kolvokup > 0) {
            if ($kolvokup == 0 or $kolvokup == '-1') {
                $line_arr['kypon'] = 'end';
            }
            else {
                $line_arr['kypon'] = $element->getValue('kolichestvo_kuponov');
            }
        }
        else {
            $line_arr['kypon'] = null;
        }

        if($element->getValue('end_time')) {
            $datenow = date(U);
            $timeakcia = $element->getValue('end_time');
            $timeakciaUnix = strtotime($timeakcia);
            $timedokoncaUnix = ($timeakciaUnix - $datenow);
            $timedokonca = round($timedokoncaUnix / 86400);
            if ($timedokonca == '1')
            {$timeinfoo = $timedokonca.' день';}
            else if ($timedokonca == '2' or $timedokonca == '3' or $timedokonca == '4')
            {$timeinfoo = $timedokonca.' дня';}
            else if ($timedokonca < '0')
            {$timeinfoo = 'end';}
            else {$timeinfoo = $timedokonca.' дней';}
            $line_arr['time'] = $timeinfoo;
        }
        else {
            $line_arr['time'] = null;
        }

        return $line_arr;

    }

    public function contitem($parentElementId, $width = '600', $height = '400'){
        $hierarchyTypes = umiHierarchyTypesCollection::getInstance();
        $hierarchyType = $hierarchyTypes->getTypeByName("content");
        $hierarchyTypeId = $hierarchyType->getId();

        //Получаем базовый тип данных для новостей
        $objectTypes = umiObjectTypesCollection::getInstance();
        $objectTypeId = $objectTypes->getTypeByHierarchyTypeId($hierarchyTypeId);
        $objectType = $objectTypes->getType($objectTypeId);

        $hierarchy = umiHierarchy::getInstance();

        //Создаем и подготавливаем выборку
        $sel = new umiSelection;
        $sel->addElementType($hierarchyTypeId); //Добавляет поиск по иерархическому типу
        $sel->addHierarchyFilter($parentElementId); //Устанавливаем поиск по разделу
        $sel->addPermissions(); //Говорим, что обязательно нужно учитывать права доступа

        //Получаем результаты
        $result = umiSelectionsParser::runSelection($sel); //Массив id объектов
        $total = umiSelectionsParser::runSelectionCounts($sel); //Количество записей

        $datenow = date(U);

        //Выводим список  на экран
        if(($sz = sizeof($result)) > 0) {
            $block_arr = Array();
            $lines = Array();
            for($i = 0; $i < $sz; $i++) {
                $element_id = $result[$i];
                $element = umiHierarchy::getInstance()->getElement($element_id);
                if(!$element) continue;
                $line_arr = Array();
                $line_arr['attribute:id'] = $element_id;
                if($i < 10) {
                    $line_arr['i'] = '0'.$i;
                    $impr = '0'.$i;
                }
                else {
                    $line_arr['i'] = $i;
                    $impr = $i;
                }
                $line_arr['name'] = $element->getName();
                $line_arr['h1'] = $element->getValue('h1');
                $line_arr['kontent'] = $element->getValue('kontent');
                $line_arr['cena'] = $element->getValue('cena');
                $elh = $element->getValue("izobrazhenie");
                $sourcthmb = $this->makeThumbnailFullmy('.'.$elh,$width,$height);
                $line_arr['thumb'] = $sourcthmb;

                $lines['nodes:item'][$impr] = $line_arr;
                templater::pushEditable("catalog", "object", $element_id);
                umiHierarchy::getInstance()->unloadElement($element_id);
            }
            $block_arr['lines'] = $lines;
            $block_arr['numpages'] = umiPagenum::generateNumPage($total, $per_page);
            $block_arr['total'] = $total;
            $block_arr['per_page'] = $per_page;
            $block_arr['category_id'] = $category_id;

            if($type_id) {
                $block_arr['type_id'] = $type_id;
            }
            return $block_arr;
        } else {
            $block_arr['numpages'] = umiPagenum::generateNumPage(0, 0);
            $block_arr['lines'] = "";
            $block_arr['total'] = 0;
            $block_arr['per_page'] = 0;
            $block_arr['category_id'] = $category_id;
        }
        return $block_arr;

    }



    function makeThumbnailFullmy($path, $width, $height, $crop = true, $cropside = 5, $isLogo = false, $quality = 50) {

        $isSharpen=true;
        $thumbs_path="./images/cms/thumbs/";
        $image = new umiImageFile($path);
        $file_name = $image->getFileName();
        $file_ext = strtolower($image->getExt());
        $file_ext = ($file_ext=='bmp'?'jpg':$file_ext);

        $allowedExts = Array('gif', 'jpeg', 'jpg', 'png', 'bmp');
        if(!in_array($file_ext, $allowedExts)) return "";

        $file_name = substr($file_name, 0, (strlen($file_name) - (strlen($file_ext) + 1)) );

        $thumbPath = sha1($image->getDirName());

        if (!is_dir($thumbs_path . $thumbPath)) {
            mkdir($thumbs_path . $thumbPath, 0755, true);
        }

        $file_name_new = $file_name . '_' . $width . '_' . $height . '_' . $cropside . '_' . $quality . "." . $file_ext;
        $path_new = $thumbs_path . $thumbPath . '/' . $file_name_new;

        if(!file_exists($path_new) || filemtime($path_new) < filemtime($path)) {
            if(file_exists($path_new)) {
                unlink($path_new);
            }

            $width_src = $image->getWidth();
            $height_src = $image->getHeight();

            if(!($width_src && $height_src)) {
                throw new coreException(getLabel('error-image-corrupted', null, $path));
            }

            if($height == "auto") {
                $real_height = (int) round($height_src * ($width / $width_src));
                //change
                $height=$real_height;
                $real_width = (int) $width;
            } else {
                if($width == "auto") {
                    $real_width = (int) round($width_src * ($height / $height_src));
                    //change
                    $width=$real_width;
                } else {
                    $real_width = (int) $width;
                }

                $real_height = (int) $height;
            }

            $offset_h=0;
            $offset_w=0;

            // realloc: devision by zero fix
            if (!intval($width) || !intval($height)) {
                $crop = false;
            }

            if ($crop){
                $width_ratio = $width_src/$width;
                $height_ratio = $height_src/$height;

                if ($width_ratio > $height_ratio){
                    $offset_w = round(($width_src-$width*$height_ratio)/2);
                    $width_src = round($width*$height_ratio);
                } elseif ($width_ratio < $height_ratio){
                    $offset_h = round(($height_src-$height*$width_ratio)/2);
                    $height_src = round($height*$width_ratio);
                }


                if($cropside) {
                    //defore all it was cropside work like as - 5
                    //123
                    //456
                    //789
                    switch ($cropside):
                        case 1:
                            $offset_w = 0;
                            $offset_h = 0;
                            break;
                        case 2:
                            $offset_h = 0;
                            break;
                        case 3:
                            $offset_w += $offset_w;
                            $offset_h = 0;
                            break;
                        case 4:
                            $offset_w = 0;
                            break;
                        case 5:
                            break;
                        case 6:
                            $offset_w += $offset_w;
                            break;
                        case 7:
                            $offset_w = 0;
                            $offset_h += $offset_h;
                            break;
                        case 8:
                            $offset_h += $offset_h;
                            break;
                        case 9:
                            $offset_w += $offset_w;
                            $offset_h += $offset_h;
                            break;
                    endswitch;
                }
            }

            $thumb = imagecreatetruecolor($real_width, $real_height);

            $source_array = $image->createImage($path);
            $source = $source_array['im'];

            if ($width*4 < $width_src && $height*4 < $height_src) {
                $_TMP=array();
                $_TMP['width'] = round($width*4);
                $_TMP['height'] = round($height*4);

                $_TMP['image'] = imagecreatetruecolor($_TMP['width'], $_TMP['height']);

                if ($file_ext == 'gif') {
                    $_TMP['image_white'] = imagecolorallocate($_TMP['image'], 255, 255, 255);
                    imagefill($_TMP['image'], 0, 0, $_TMP['image_white']);
                    imagecolortransparent($_TMP['image'], $_TMP['image_white']);
                    imagealphablending($source, TRUE);
                    imagealphablending($_TMP['image'], TRUE);
                } else {
                    imagealphablending($_TMP['image'], false);
                    imagesavealpha($_TMP['image'], true);
                }
                imagecopyresampled($_TMP['image'], $source, 0, 0, $offset_w, $offset_h, $_TMP['width'], $_TMP['height'], $width_src, $height_src);

                imageDestroy($source);

                $source = $_TMP['image'];
                $width_src = $_TMP['width'];
                $height_src = $_TMP['height'];

                $offset_w = 0;
                $offset_h = 0;
                unset($_TMP);
            }

            if ($file_ext == 'gif') {
                $thumb_white_color = imagecolorallocate($thumb, 255, 255, 255);
                imagefill($thumb, 0, 0, $thumb_white_color);
                imagecolortransparent($thumb, $thumb_white_color);
                imagealphablending($source, TRUE);
                imagealphablending($thumb, TRUE);
            } else {
                imagealphablending($thumb, false);
                imagesavealpha($thumb, true);
            }

            imagecopyresampled($thumb, $source, 0, 0, $offset_w, $offset_h, $width, $height, $width_src, $height_src);
            if($isSharpen) $thumb = makeThumbnailFullUnsharpMask($thumb,80,.5,3);

            switch($file_ext) {
                case 'gif':
                    $res = imagegif($thumb, $path_new);
                    break;
                case 'png':
                    $res = imagepng($thumb, $path_new);
                    break;
                default:
                    $res = imagejpeg($thumb, $path_new, $quality);
            }
            if(!$res) {
                throw new coreException(getLabel('label-errors-16008'));
            }

            imageDestroy($source);
            imageDestroy($thumb);

            if($isLogo) {
                umiImageFile::addWatermark($path_new);
            }
        }

        $value = new umiImageFile($path_new);

        $arr = $value->getFilePath(true);
        return $arr;
    }






    public function userid(){
        $permissions = permissionsCollection::getInstance();
        $currentUserId = $permissions->getUserId();
        return $currentUserId;
    }

    public function ubalance(){
        $permissions = permissionsCollection::getInstance();
        $currentUserId = $permissions->getUserId();

        $objects = umiObjectsCollection::getInstance();
        $userObject = $objects->getObject($currentUserId);

        $ballance = $userObject->getValue("bonus");
        return $ballance;
    }


  /* ------------------   Начисление баллов -----------------*/

    public function userbonus() {
        $userId = getRequest('userid');
        $rate = getRequest('balli');

        $sRedirect = getRequest('ref_notsuccess');
        if (!$rate) {$this->redirect($sRedirect.'&errorbal=1');}

        //Получим объект пользователя
        $userObject = umiObjectsCollection::getInstance()->getObject($userId);
        // Получим текущий рейтинг, увеличим его на переданное значение и сохраним изменения
        $currentRateUser = $userObject->getValue("bonus");
        $currentvizity = $userObject->getValue("vizity"); //число визитов
        if ($currentvizity == 0) {
            $promo_code = $userObject->getValue("promo_kod");
            //Начисление баллов за рекомендацию
            if (strlen($promo_code) > 0) {
                $users_promo = new selector('objects');
                $users_promo->types('object-type')->name('users', 'user');
                $users_promo->where('id')->equals($promo_code);
                $totalusers = $users_promo->length;
                if ($totalusers > 0) {
                    $bbal = regedit::getInstance()->getVal("//modules/mobile/regbalpromoplus");
                    foreach ($users_promo as $user) {
                        $userbonus = $user->bonus;
                        $userbonus = intval($userbonus) + intval($bbal); //Начисляем 50 баллов за рекомендацию
                        $user->setValue("bonus", $userbonus);
                        $user->commit();
                    }
                    //Теперь Запишем данные о начислении в модуль
                    $pointTypes = umiObjectTypesCollection::getInstance();
                    $pointObjectTypeId = $pointTypes->getBaseType("points", "stat");

                    $pointsName = "Баллы за регистрацию";
                    $pontsData = date("U");
                    $pointsAdmin = 42;
                    $pointsUser = $promo_code;
                    $pointsUsluga = "Посещение по рекомендации";
                    $pointsSumm = $bbal;

                    $objectsp = umiObjectsCollection::getInstance();
                    $newPointsId = $objectsp->addObject($pointsName, $pointObjectTypeId);

                    //Получаем объект Поинта
                    $newPoint = $objectsp->getObject($newPointsId);
                    if ($newPoint instanceof umiObject) {
                        //Заполняем объект  новыми свойствами
                        $newPoint->setValue("administrator", $pointsAdmin);
                        $newPoint->setValue("polzovatel", $pointsUser);
                        $newPoint->setValue("data", $pontsData);
                        $newPoint->setValue("usluga", $pointsUsluga);
                        $newPoint->setValue("kolichestvo_ballov", $pointsSumm);
                        $newPoint->setValue("is_active", true);

                        //Подтверждаем внесенные изменения
                        $newPoint->commit(); //Хочу домой есть и спать
                    }
                    //Конец Запишем данные о начислении в модуль
                }
            }
        }
        //Конец начисления баллов за рекомендацию



        $currentvizity = $currentvizity + 1;
        $sumRateUser = $currentRateUser + round($rate/10, 0);
        $sumRateUser = round($sumRateUser, 0);
        $time = time();
        //Проверяем не было ли начислений на эту сумму 5 мин назад
        $check = new selector('objects');
        $check->types('object-type')->name('points', 'stat');
        $check->where('kolichestvo_ballov')->equals(round($rate/10, 0));
        $check->where('data')->between(time()-300, time()+300);
        $kolvocheck = $check->length;
        //КОнец проверяем начисления
        if ($kolvocheck >0) {
            $this->redirect('/mobile/success/?usr=' . $userId . '&errorbal=5&sumbal=' . round($rate/10, 0));
        }
        else {
            $userObject->setValue("vizity", $currentvizity);
            $userObject->setValue("bonus", $sumRateUser);
            $userObject->setValue("poslednij_vizit", $time);
            $userObject->commit();

            //Запишем данные о начислении в модуль
            $pointTypes = umiObjectTypesCollection::getInstance();
            $pointObjectTypeId = $pointTypes->getBaseType("points", "stat");

            $pemissios = permissionsCollection::getInstance();
            $cuetUseId = $pemissios->getUserId();

            $pointsName = "Начисление баллов";
            $pontsData = date("U");
            $pointsAdmin = $cuetUseId;
            $pointsUser = $userId;
            $pointsUsluga = "В салоне";
            $pointsSumm = $rate / 10;
            $pointsSumm = round($pointsSumm, 0);

            $objects = umiObjectsCollection::getInstance();
            $newPointsId = $objects->addObject($pointsName, $pointObjectTypeId);

            //Получаем объект Поинта
            $newPoint = $objects->getObject($newPointsId);
            if ($newPoint instanceof umiObject) {
                //Заполняем объект  новыми свойствами
                $newPoint->setValue("administrator", $pointsAdmin);
                $newPoint->setValue("polzovatel", $pointsUser);
                $newPoint->setValue("data", $pontsData);
                $newPoint->setValue("usluga", $pointsUsluga);
                $newPoint->setValue("kolichestvo_ballov", $pointsSumm);
                $newPoint->setValue("is_active", true);

                //Подтверждаем внесенные изменения
                $newPoint->commit();
            }


            $this->redirect('/mobile/success/?usr=' . $userId . '&sumbal=' . round($rate/10, 0));
        }

    }

    public function userminus() {
        $userId = getRequest('userid');
        $rate = getRequest('balli');
        $uslugaid = getRequest('usluga');

        $sRedirect = getRequest('ref_notsuccess');

        //Получим объект пользователя
        $userObject = umiObjectsCollection::getInstance()->getObject($userId);
        // Получим текущий рейтинг, увеличим его на переданное значение и сохраним изменения
        $currentRateUser = $userObject->getValue("bonus");

        if ($currentRateUser < $rate) {$this->redirect($sRedirect.'&errorbal=1');}

        $sumRateUser = $currentRateUser - (int) $rate;
        $time = time();
        $userObject->setValue("bonus", $sumRateUser);
        $userObject->setValue("poslednij_vizit", $time);
        $userObject->commit();

        //Запишем данные о Списании в модуль
        $pointTypes = umiObjectTypesCollection::getInstance();
        $pointObjectTypeId = $pointTypes->getBaseType("points", "stat");

        $pemissios = permissionsCollection::getInstance();
        $cuetUseId = $pemissios->getUserId();

        $hierarchy = umiHierarchy::getInstance();
        $elementId = $uslugaid;

        $element = $hierarchy->getElement($elementId);
        if($element instanceof umiHierarchyElement) {
            $pointsUsluga = $element->getName();
        }
        else {
            $pointsUsluga = "Подарок";
        }

        $pointsName = "Списание баллов";
        $pontsData = date("U");
        $pointsAdmin = $cuetUseId;
        $pointsUser = $userId;
        $pointsSumm = "-".$rate;

        $objects = umiObjectsCollection::getInstance();
        $newPointsId = $objects->addObject($pointsName, $pointObjectTypeId);

        //Получаем объект Поинта
        $newPoint = $objects->getObject($newPointsId);
        if($newPoint instanceof umiObject) {
            //Заполняем объект  новыми свойствами
            $newPoint->setValue("administrator", $pointsAdmin);
            $newPoint->setValue("polzovatel", $pointsUser);
            $newPoint->setValue("data", $pontsData);
            $newPoint->setValue("usluga", $pointsUsluga);
            $newPoint->setValue("kolichestvo_ballov", $pointsSumm);
            $newPoint->setValue("is_active", true);

            //Подтверждаем внесенные изменения
            $newPoint->commit();
        }


        $this->redirect('/mobile/success/?usr='.$userId);;
    }

    /*----------------- Конец начисления баллов -------------------*/




    public function show() {
        require_once 'Mobile_Detect.php';
        $detect = new Mobile_Detect;
        if($detect->isiOS() ){
            $redirect_url  = regedit::getInstance()->getVal("//modules/mobile/ioslink");
        }
        else if($detect->isAndroidOS() ){
            $redirect_url  = regedit::getInstance()->getVal("//modules/mobile/androidlink");
        }

        else {
            $redirect_url  = regedit::getInstance()->getVal("//modules/mobile/ioslink");
        }
        $this->redirect($redirect_url);
    }

        public function znachsprv($typeId){
            $objectsCollection = umiObjectsCollection::getInstance();
            $guidepr = $objectsCollection->getGuidedItems($typeId);
            
            $items = Array();
            $firstb = Array();
            foreach($guidepr as $itid => $item) {
                $item_arr = Array();
                $item_arr['attribute:value'] = $item;
                $item_arr['attribute:id'] = $itid;
                $items[] = $item_arr;
            }
            $itogarray = Array("items" => Array('nodes:item' => $items));
            return $itogarray;
            
        }

        public function payments(){
            static $paymentsList = null;

            if (!is_null($paymentsList)) {
                return $paymentsList;
            }

            $cacheFrontend = cacheFrontend::getInstance();
            $key = 'payments_list';
            $cachedPaymentsList = $cacheFrontend->loadData($key);

            if (is_array($cachedPaymentsList)) {
                return $cachedPaymentsList;
            }

            $sel = new selector('objects');
            $sel->types('hierarchy-type')->name('emarket', 'payment');
            $sel->option('load-all-props')->value(true);
            $paymentsList = $sel->result;

            $itogarray = Array("items" => Array('nodes:item' => $paymentsList));
            return $itogarray;
            
        }

 
public function config() {
return __mobile::config();
}
 
public function getEditLink($element_id, $element_type) {
$element = umiHierarchy::getInstance()->getElement($element_id);
$parent_id = $element->getParentId();
 
switch($element_type) {
    case "groupelements": {
    $link_add = $this->pre_lang . "/admin/mobile/add/{$element_id}/item_element/";
    $link_edit = $this->pre_lang . "/admin/mobile/edit/{$element_id}/";

    return Array($link_add, $link_edit);
    break;
    }

    case "group_feedback": {
        $link_add = $this->pre_lang . "/admin/mobile/add/{$element_id}/item_feedback/";
        $link_edit = $this->pre_lang . "/admin/mobile/edit/{$element_id}/";

        return Array($link_add, $link_edit);
        break;
    }
 
    case "item_element": {
    $link_edit = $this->pre_lang . "/admin/mobile/edit/{$element_id}/";

    return Array(false, $link_edit);
    break;
    }

    case "item_feedback": {
        $link_edit = $this->pre_lang . "/admin/mobile/edit/{$element_id}/";

        return Array(false, $link_edit);
        break;
    }
 
default: {
return false;
}
}
}
 
};
?>